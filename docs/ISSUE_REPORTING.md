# Issue reporting

The **Scenic App** depends on a complex software stack. As such, bugs can occur in any of the stack's components. Bugs will typically emerge from one of the following components:

+ The Scenic App itself
+ The **Scenic Core** websocket server
+ The **Switcher** engine

Relevant debugging information about each of these components can be produced by following the steps below. Please include as much information as possible when submitting your bug report.

The following debugging steps *may* require administrator privileges.

## Getting the Scenic App Log

The **Scenic App** log is accessible from the Chrome/Chromium browser's DevTools. Consequently, you should :

+ Open the [Chrome DevTools](https://developers.google.com/web/tools/chrome-devtools/)
+ Open the `Console` tab
+ Right-click in the `Console` panel
+ Select the "Save as..." option in the contextual menu
+ Send the saved file to the maintenance team

## Getting Scenic Core and Switcher's stack trace

In order to debug this stack you should :

+ Install the [GDB software](https://www.gnu.org/software/gdb/)
+ Recompile [Switcher](https://gitlab.com/sat-mtl/tools/switcher) in `DEBUG` mode (optional)
+ Rebuild [Scenic Core](https://gitlab.com/sat-mtl/tools/scenic/scenic-core) (optional)
+ Run `make gdb-debug` in order to start **Scenic Core** with **GDB**
+ Input `run` in the **GDB** prompt

When **Scenic Core** is launched, you should be able to connect **Scenic App** to it. Now is the time to reproduce your bug.

When a bug is thrown, the **GDB** prompt will freeze. You should then :

+ Type `bt full` in the **GDB** prompt to display the full stack trace
+ Type `quit` in order to exit **GDB**
+ Get the log file saved in `~/.scenic/log/gdb-<CURRENT_TIMESTAMP>.log`
+ Send this file to the maintenance team

## Adding information about your hardware

Information about your hardware may be useful for the maintenance team and can potentially accelerate bug resolution requests. All relevant information about your hardware can be produced with the following command :

```bash
sudo lshw > system-specs.txt
```

Please include this file with your bug report.
