# Automated cloud dev environment

This guide goes over how to use Gitpod automated cloud development environments to help develop Scenic.

Gitpod is a cloud IDE that allows a developer to code directly in their browser using a VS Code compatible environment. It automatically starts application back-end in a container every time user open a session. It's like having a fresh deployment of the application everytime you open the IDE.

Scenic Gitpod configuration automatically build front-end web app, serve it using npm webpack server. It also starts Scenic-core back-end from Docker image. It waits for web app and backend to be ready then automatically opens Scenic session using Gitpod https reverse proxy. Scenic web app is started using `?endpoint` URL parameter set to `8000-satmtltelepresen-scenic-<gitpod-workspace-id>.<gitpod-cluster>.gitpod.io:443`.

This configuration also leverage `gitpod/workspace-full-vnc` upstream Docker image. It allows to test Scenic-core back-end graphical output to a virtual X11 session running in developer's browser displayed via noVNC.

Workspace includes a dummy Jack2 server to test audio connections. It also includes Avahi service required to test NDI destinations and sources. OBS Studio along with its obs-ndi plugin are installed in the VNC X11 session to allow testing NDI easily.

Video4Linux sources, hardware encoders are not available in this environment.

## Open project in Gitpod

From Scenic Gitlab project page, choose your current working branch, then click on down arrow next to Web IDE button.

From drop down menu, choose `Gitpod` option.

Click `Gitpod` button.

Gitpod will ask to authenticate using Gitlab authentication. Click on `Continue with GitLab` button.

## Wait for front-end web app compilation

When Gitpod starts Scenic workspace, two terminals opens. Right terminal is downloading and starting Scenic-core Docker container image, left terminal is displaying npm build process. Wait couple seconds, then Scenic should automatically open in a new browser window.

You might need to disable pop-up window protection in your browser to be able to see new windows open by workspace.

## Manually open Scenic web app

In Gitpod workspace, navigate to `Remote Explorer` tab, select `Scenic Web app: 8080` port, then click on `Open Browser` icon (last icon on port item line).

A new tab should open webpage `https://8080-satmtltelepresen-scenic-<gitpod-workspace-id>.<gitpod-cluster>.gitpod.io/`.

In Switcher connection modal window, enter following details :

 - Host address: `8000-satmtltelepresen-scenic-<gitpod-workspace-id>.<gitpod-cluster>.gitpod.io`
 - Host port: 443

Click Connect button.

## Closing session

When done with Gitpod session, you can tell Gitpod servers to end your session and terminate all your running processes.

Click on burger menu button (upper left corner).

Choose `Gitpod: Stop workspace` option.

## Testing RTMP using Owncast running in workspace

When workspace is launched, a local instance of Owncast is started on port 8081.

To test RTMP destination in Scenic in Gitpod follow those steps.

1. In Scenic, click on `Sources` menu, select `Video`, then choose `Test Pattern`.
2. In matrix view, start `Test Pattern 0` source.
3. Click on `Compression` menu, choose `H264 Encoder`.
4. Assign `Test Pattern 0` source to `H264 Encoder 0` destination.
5. Click on `Sources` menu, select `Audio`, then choose `Test Signal`.
6. Select `Test Signal 0` source, set Number of channels to `2`.
7. Start `Test Signal 0` source.
8. In Scenic, click on `Destinations` menu, select `Network`, then choose `RTMP Streaming`.
9. In Create an RTMP Destination modal window, enter following details.
 - RTMP URL: `rtmp://localhost:1935/live`
 - Stream key: `abc123`
10. Click `Create` button.
11. Assign `H264 Encoder 0` source to `RTMP 0` destination.
12. Assign `Test Signal 0` source to `RTMP 0` destination.
13. Select `RTMP 0` destination, click on `on/off` toggle switch to start streaming.
14. In Gitpod workspace, navigate to `Remote Explorer` tab, select `Owncast Web app: 8081` port, then click on `Open Browser` icon (last icon on port item line).
15. Click on `play` button, your stream should play.
> Owncast is configured by default to transcode received stream into 1.2 Mbps bitrate and resample at 24 fps. Source resolution is kept as-is but can be configured to be resized.
17. You can access Owncast admin panel by visiting `https://8081-satmtltelepresen-scenic-<gitpod-workspace-id>.<gitpod-cluster>.gitpod.io/admin` page. Login using `admin` user and `abc123` password.