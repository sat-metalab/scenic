# Installation Guide

*Scenic is now available online at <http://scenic-app.com>. No other installation step is necessary to use this online version.*

*However, keep in mind that Scenic still requires that both [Switcher](https://gitlab.com/sat-mtl/tools/switcher) and [Scenic Core](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/) be installed and running on the system in order for it to do anything.*

*If you wish to build and run a local version of Scenic instead, follow the instructions below.*


## Installation from Source

Keep in mind that Scenic is tested and developed for the **Chromium** browser. On Ubuntu, you can install it using `sudo apt install chromium`.

### Prerequisites

| Name                                        | Version | Command                                                                     | Description                                       |
|---------------------------------------------|---------|-----------------------------------------------------------------------------|---------------------------------------------------|
| [NodeJS](https://nodejs.org/)               | LTS     | [See the Ubuntu distributions](https://github.com/nodesource/distributions) | Required to build and bundle the source code      |
| [H2O HTTP Server](https://h2o.examp1e.net/) | latest  | `sudo apt install h2o`                                                      | Recommended server to run and serve Scenic locally |

### Building steps

```bash
git clone https://gitlab.com/sat-mtl/tools/scenic/scenic.git
cd scenic
make bundle # Build and bundle the app
```

### Local deployment

Once Scenic is built, you need to deploy it to a local HTTP server. If using H2O, you can do so with the following one-liner:

```bash
sudo make install
```

Once deployed, Scenic will be served on <http://localhost:8080> and will be accessible from any modern browser.

All additional commands are documented in the [development section](./DEVELOPMENT.md).

### Deployment from a Container

You can build and run the Docker container from source:

```bash
docker build -f dockerfiles/h2o.Dockerfile -t scenic:h2o .  # Building
docker run -p 8080:8080 localhost/scenic:h2o  # Running
```

## Usage

First, launch the [Scenic Core websocket server](https://gitlab.com/sat-mtl/tools/scenic/scenic-core) with the `scenic` command.

Second, open Scenic in your browser (Chromium is recommended).

* If you are running Scenic locally, open <http://localhost:8080> in your browser. (`chromium-browser http://localhost:8080`)
* If you wish to run the online version of Scenic (no installation required), open <http://scenic-app.com> in your browser.
* For optimal support of touchscreens, consider launching Chromium with _touch-event_ option. (`chromium-browser --touch-events http://localhost:8080`).
