#!/bin/bash
# Run this script on ScenicOS to configure Scenic to use Scenic Station hardware
# Setup Scenic GUI in french, setup Scenic menus for 1 x HMDI and 3 x SDI inputs,
# and enable nvenc encoder and nvdec decoder.

mkdir -p ~/.config/scenic
cat << EOF > ~/.config/scenic/scenic.json
{
  "defaultLang": "fr",
  "hiddenCapabilities": [
    "layout",
    "channel-mask",
    "multiview-mode",
    "multiview-flags",
    "pixel-aspect-ratio",
    "chroma-site",
    "colorimetry",
    "stream-format",
    "alignment",
    "switcher-name",
    "quiddity-id"
  ]
}
EOF
cat << EOF > ~/.config/scenic/menus.json
{
  "customMenus": [
    {
      "name": "Sources",
      "subMenus": [
        {
          "name": "Video",
          "items": [
            {
              "name": "SDI 1",
              "exclusive": true,
              "quiddity": "sdiInput1",
              "categories": [
                "Video"
              ]
            },
            {
              "name": "SDI 2",
              "exclusive": true,
              "quiddity": "sdiInput2",
              "categories": [
                "Video"
              ]
            },
            {
              "name": "SDI 3",
              "exclusive": true,
              "quiddity": "sdiInput3",
              "categories": [
                "Video"
              ]
            },
            {
              "name": "HDMI",
              "exclusive": true,
              "quiddity": "hdmiInput",
              "categories": [
                "Video"
              ]
            },
            {
              "name": "Webcam",
              "exclusive": true,
              "quiddity": "webcamInput",
              "categories": [
                "Video"
              ]
            },
            {
              "name": "Test Pattern",
              "quiddity": "videoTestInput",
              "categories": [
                "Video"
              ]
            }
          ]
        },
        {
          "name": "Audio",
          "items": [
            {
              "name": "XR18 Input",
              "quiddity": "xr18Input",
              "categories": [
                "Audio"
              ]
            },
            {
              "name": "Test Signal",
              "quiddity": "audioTestInput",
              "categories": [
                "Audio"
              ]
            }
          ]
        },
        {
          "name": "Control",
          "items": [
            {
              "name": "MIDI Input",
              "quiddity": "midiInput",
              "categories": [
                "Control"
              ]
            },
            {
              "name": "OSC Input",
              "quiddity": "oscInput",
              "categories": [
                "Control"
              ]
            }
          ]
        },
        {
          "name": "SIP",
          "hidden": true,
          "items": [
            {
              "name": "SIP",
              "quiddity": "extshmsrc",
              "categories": [
                "SIP/Other"
              ]
            }
          ]
        },
        {
          "name": "Network",
          "items": [
            {
              "name": "NDI® Input",
              "quiddity": "scenicNdiSniffer",
              "categories": [
                "Network"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Destinations",
      "subMenus": [
        {
          "name": "Video",
          "items": [
            {
              "name": "Video Monitor",
              "quiddity": "videoOutput",
              "categories": [
                "Video"
              ]
            }
          ]
        },
        {
          "name": "Audio",
          "items": [
            {
              "name": "XR18 Output",
              "quiddity": "xr18Output",
              "categories": [
                "Audio"
              ]
            }
          ]
        },
        {
          "name": "Control",
          "items": [
            {
              "name": "MIDI Output",
              "quiddity": "midiOutput",
              "categories": [
                "Control"
              ]
            },
            {
              "name": "OSC Output",
              "quiddity": "oscOutput",
              "categories": [
                "Control"
              ]
            }
          ]
        },
        {
          "name": "Network",
          "items": [
            {
              "name": "RTMP Streaming",
              "quiddity": "rtmpOutput",
              "categories": [
                "Network"
              ]
            },
            {
              "name": "NDI® Output",
              "quiddity": "ndiOutput",
              "categories": [
                "Network"
              ]
            }
          ]
        }
      ]
    },
    {
      "name": "Compression",
      "items": [
        {
          "name": "Video Encoder",
          "quiddity": "videoEncoder",
          "categories": [
            "Compression"
          ]
        }
      ]
    }
  ]
}
EOF
cat << EOF > ~/.config/scenic/bundles.json
{
  "bundle": {
    "hdmiInput": {
      "pipeline": "v4l2src name=Capture <shmw _force_framerate=false _device=0 _pixel_format=4 _tv_standard _custom_framerate _framerate _deinterlace_mode _save_mode <top_level <add_to_start ! videnc name=Encoder <no_prop _codec_params _bitrate _codec <shmw",
      "doc": {
        "long_name": "HDMI",
        "category": "video",
        "tags": "writer",
        "description": "Video from HDMI input"
      }
    },
    "sdiInput1": {
      "pipeline": "v4l2src name=Capture <shmw _force_framerate=false _device=1 _pixel_format=4 _tv_standard _custom_framerate _framerate _deinterlace_mode _save_mode <top_level <add_to_start ! videnc name=Encoder <no_prop _codec_params _bitrate _codec <shmw",
      "doc": {
        "long_name": "SDI1",
        "category": "video",
        "tags": "writer",
        "description": "Video from SDI 1 input"
      }
    },
    "sdiInput2": {
      "pipeline": "v4l2src name=Capture <shmw _force_framerate=false _device=2 _pixel_format=4 _tv_standard _custom_framerate _framerate _deinterlace_mode _save_mode <top_level <add_to_start ! videnc name=Encoder <no_prop _codec_params _bitrate _codec <shmw",
      "doc": {
        "long_name": "SDI2",
        "category": "video",
        "tags": "writer",
        "description": "Video from SDI 2 input"
      }
    },
    "sdiInput3": {
      "pipeline": "v4l2src name=Capture <shmw _force_framerate=false _device=3 _pixel_format=4 _tv_standard _custom_framerate _framerate _deinterlace_mode _save_mode <top_level <add_to_start ! videnc name=Encoder <no_prop _codec_params _bitrate _codec <shmw",
      "doc": {
        "long_name": "SDI3",
        "category": "video",
        "tags": "writer",
        "description": "Video from SDI 3 input"
      }
    },
    "webcamInput": {
      "pipeline": "v4l2src name=WEBCAM <shmw _force_framerate=false _device=4 _pixel_format=0 _tv_standard _custom_framerate _framerate _deinterlace_mode _save_mode <top_level <add_to_start ! videnc name=Encoder <no_prop _codec_params _bitrate _codec <shmw",
      "doc": {
        "long_name": "Webcam",
        "category": "video",
        "tags": "writer",
        "description": "Video from USB webcam"
      }
    },
    "xr18Input": {
      "pipeline": "jacksrc name=XR18 <add_to_start <no_prop <top_level _channels _index <shmw",
      "doc": {
        "long_name": "XR18 input",
        "category": "audio",
        "tags": "writer",
        "description": "Audio from XR18 mixer"
      }
    },
    "videoOutput": {
      "pipeline": "glfwin name=Window title=Monitor <top_level <shmr width=400 height=225 position_x=0 position_y=0 _xevents=false",
      "doc": {
        "long_name": "Video output",
        "category": "video",
        "tags": "reader/device/occasional-writer",
        "description": "Video send to resizable window"
      }
    },
    "xr18Output": {
      "pipeline": "shmdelay name=Delay <shmr <no_prop _time_delay=0 restrict_caps=1 ! jacksink name=Output _connect_to=system:playback_%d _auto_connect=true _do_format_conversion=true _do_rate_conversion=true _server_name",
      "doc": {
        "long_name": "XR18 output",
        "category": "audio",
        "tags": "reader",
        "description": "Audio send to XR18 mixer"
      }
    },
    "videoEncoder": {
      "pipeline": "videnc name=Encoder <no_prop _codec_params _bitrate _codec <shmw <top_level <shmr",
      "doc": {
        "long_name": "Video encoder",
        "category": "video",
        "tags": "reader/writer",
        "description": "Video compressor"
      }
    }
  },
  "gstreamer": {
    "primary_priority": {
      "nvdec": 10
    }
  }
}
EOF
# Configure Switcher extra config paths
mkdir -p ~/.config/switcher
cat << EOF > ~/.config/switcher/global.json
{
  "jacksrc": {
    "max_number_of_channels": 18
  },
  "jacksink": {
    "max_number_of_channels": 18
  },
  "extraConfig": [
    "~/.config/scenic/scenic.json",
    "~/.config/scenic/menus.json",
    "~/.config/scenic/contacts.json",
    "~/.config/scenic/bundles.json"
  ]
}
EOF
