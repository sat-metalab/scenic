#!/usr/bin/env python3

import argparse
import json


def parse_args() -> argparse.Namespace:
    argp = argparse.ArgumentParser()
    argp.add_argument("seed",
                      help="Seed file from which to generate contacts",
                      type=str)
    argp.add_argument("-o",
                      "--output",
                      help="Output file path (Default is ./contacts.json)",
                      type=str)

    return argp.parse_args()


def generate_contacts(seed: dict, domain: str) -> dict:
    contacts = {}
    for (contact_id, values) in seed['contacts'].items():
        if domain not in values["exclude_from"]:
            contacts[contact_id + "@" + domain] = {
                "authorized": bool("true"),
                "name": values["name"]
            }
    return contacts


def main() -> None:
    args = parse_args()

    # Open seed file
    try:
        with open(args.seed) as f:
            contacts_seed = json.load(f)
    except IOError as e:
        print("Error while trying to open file '" + args.seed + "': " + str(e))
        return
    except json.JSONDecodeError as e:
        print("Error while loading JSON seed: " + str(e))
        return

    # Generate contacts from seed
    full_contacts = {}
    for domain in contacts_seed['domains']:
        for (contact_id, values) in contacts_seed['contacts'].items():
            if domain not in values["exclude_from"]:
                contact_list = generate_contacts(contacts_seed, domain)
                full_contacts.update({
                    contact_id + "@" + domain: contact_list
                })

    # Write contacts to file
    if not args.output:
        output_file = './contacts.json'
    else:
        output_file = args.output

    try:
        with open(output_file, 'w') as file:
            json.dump(full_contacts,
                      file,
                      indent=4,
                      sort_keys=True,
                      ensure_ascii=False)
    except IOError as e:
        print("Error while trying to open file '" +
              output_file + "': " + str(e))
    except (TypeError, OverflowError, ValueError) as e:
        print("Error while writing contacts to '" +
              output_file + "': " + str(e))


if __name__ == "__main__":
    main()
