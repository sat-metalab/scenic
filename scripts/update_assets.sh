#!/bin/bash

# Updates all required assets of all kinds
#
# $1 - The locale JSON file
#
# Usage:
#  ./update_assets.sh
#
# Dependencies:
# * The script uses the [`curl command`](https://curl.se/) to automatically fetch the assets.
update_image_asset () {
  local source_url="$1"
  local target_file="$2"

  echo "Download ${source_url} to ${target_file}"
  curl "${source_url}" > "${target_file}"
}

GITLAB_STATIC_URL='https://assets.gitlab-static.net/uploads/-/system/project/avatar'
GITLAB_RAW_URL='https://gitlab.com/sat-metalab/metalabcomm/-/raw'

SAT_LOGO_URL="${GITLAB_RAW_URL}/master/logos/LogoSATwhite.png"
SAT_LOGO_FILE="./assets/img/sat.png"

update_image_asset "$SAT_LOGO_URL" "$SAT_LOGO_FILE"

SHMDATA_LOGO_URL="${GITLAB_STATIC_URL}/2101218/Shmdata-color.png?width=64"
SHMDATA_LOGO_FILE="./assets/img/shmdata.png"

update_image_asset "$SHMDATA_LOGO_URL" "$SHMDATA_LOGO_FILE"

SWITCHER_LOGO_URL="${GITLAB_STATIC_URL}/2101216/Switcher-color.png?width=64"
SWITCHER_LOGO_FILE="./assets/img/switcher.png"

update_image_asset "$SWITCHER_LOGO_URL" "$SWITCHER_LOGO_FILE"

SPLASH_LOGO_URL="${GITLAB_RAW_URL}/master/logos/Splash/WEB/Splash_WEB_RGB-.png?width=64"
SPLASH_LOGO_FILE="./assets/img/splash.png"

update_image_asset "$SPLASH_LOGO_URL" "$SPLASH_LOGO_FILE"

SATIE_LOGO_URL="${GITLAB_RAW_URL}/master/logos/SATIE/WEB/Satie_RGB.png?width=64"
SATIE_LOGO_FILE="./assets/img/satie.png"

update_image_asset "$SATIE_LOGO_URL" "$SATIE_LOGO_FILE"

VARAYS_LOGO_URL="${GITLAB_STATIC_URL}/6967884/vaRays_C.png?width=64"
VARAYS_LOGO_FILE="./assets/img/varays.png"

update_image_asset "$VARAYS_LOGO_URL" "$VARAYS_LOGO_FILE"

EIS_LOGO_URL="${GITLAB_STATIC_URL}/3466704/Edition_in_situ_C.png?width=64"
EIS_LOGO_FILE="./assets/img/eis.png"

update_image_asset "$EIS_LOGO_URL" "$EIS_LOGO_FILE"

CALIMIRO_LOGO_URL="${GITLAB_RAW_URL}/master/logos/Calimiro/PNG/Calimiro_B.png?width=64"
CALIMIRO_LOGO_FILE="./assets/img/calimiro.png"

update_image_asset "$CALIMIRO_LOGO_URL" "$CALIMIRO_LOGO_FILE"
