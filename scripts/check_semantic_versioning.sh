#!/bin/bash

if [[ -z "${GITLAB_API_TOKEN}" || -z "${GITLAB_MERGE_REQUEST_API}" || -z "${CI_COMMIT_REF_NAME}" ]]; then
  echo "Gitlab variables missing !"
  exit 0
fi

echo "Install semver tooling"

apt update -qq
apt install -y -qq jq git curl
npm i -g semver

echo "Fetch all version values from git repositories"

CURRENT_VERSION=$(cat package.json | jq .version | xargs semver -c)
TARGET_BRANCH=$(curl -LsS -H "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" ${GITLAB_MERGE_REQUEST_API} | jq --raw-output ".[0].target_branch")

if [ ${TARGET_BRANCH} = null ]; then
  echo "Branch ${CI_COMMIT_REF_NAME} is not part of a merge request !"
  exit 0
fi

TARGET_VERSION=$(git show origin/${TARGET_BRANCH}:package.json | jq .version | xargs semver -c)

echo "Compare all version values"

if [ -z $(echo ${TARGET_BRANCH} | grep -E '(develop|master)') ]; then
  echo "Checking package version is useless"
  exit 0
elif [ -z $(semver -r ">${TARGET_VERSION}" "${CURRENT_VERSION}") ]; then
  (>&2 echo "ERROR: Package version needs to be upper then ${TARGET_VERSION} !")
  exit 1
fi
