#!/bin/bash

# check arguments
if [ "$#" -ne 1 ]; then
  echo "Service name is required"
  exit 0
fi

# check if server's service exists
SERVICE=$(service --status-all | grep -om 1 ${1})

if [ -n "${SERVICE}" ]; then
  echo "Restart ${SERVICE} service"
  $(service ${SERVICE} restart)
else
  echo "${1} is not found as a service"
  exit 0
fi
