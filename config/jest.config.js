// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

const path = require('path')
const { execSync } = require('child_process')

const MINIMAL_COVERAGE = {
  branches: 80,
  functions: 80,
  lines: 80,
  statements: 80
}

const JEST_CONFIG = {
  rootDir: path.resolve(__dirname, '..'),

  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // The directory where Jest should output its coverage files
  coverageDirectory: './coverage/',

  collectCoverageFrom: [
    'src/**/*.js'
  ],

  /** @todo FIX THE COVERAGE TO 80 FOR EVER */
  coverageThreshold: {
    'src/components/': {
      branches: 50,
      functions: 50,
      lines: 50,
      statements: 50
    },
    'src/stores/': {
      ...MINIMAL_COVERAGE,
      branches: 75
    }
  },

  // A map from regular expressions to module names that allow to stub out resources with a single module
  moduleNameMapper: {
    '\\.(css|scss|jpg|png|html|ejs)$': '<rootDir>/test/emptyModule.js',
    '\\.(md)$': '<rootDir>/test/emptyComponent.js',
    '~/lib/spin': '<rootDir>/test/emptyModule.js',
    '^~(.*)': '<rootDir>/$1',
    '^@components(.*)': '<rootDir>/src/components$1',
    '^@stores(.*)': '<rootDir>/src/stores$1',
    '^@api(.*)': '<rootDir>/src/api$1',
    '^@model(.*)': '<rootDir>/src/model$1',
    '^@models(.*)': '<rootDir>/src/models$1',
    '^@utils(.*)': '<rootDir>/src/utils$1',
    '^@test(.*)': '<rootDir>/test/$1',
    '^@fixture(.*)': '<rootDir>/test/fixture$1',
    '^@assets(.*)': '<rootDir>/assets$1',
    logger: '<rootDir>/src/logger',

    // special mapper for esm libraries
    // see https://stackoverflow.com/questions/73203367/jest-syntaxerror-unexpected-token-export-with-uuid-library
    uuid: require.resolve('uuid')
  },

  // An array of regexp pattern strings that are matched against all source file paths before transformation. If the file path matches any of the patterns, it will not be transformed.
  // see https://jestjs.io/docs/configuration/#transformignorepatterns-arraystring
  transformIgnorePatterns: [
    '/node_modules/(?!(uuid)/)'
  ],

  // An array of regexp pattern strings, matched against all module paths before considered 'visible' to the module loader
  modulePathIgnorePatterns: [
    '<rootDir>/dist'
  ],

  // The paths to modules that run some code to configure or set up the testing environment before each test
  setupFiles: [
    '<rootDir>/config/jest.setup.js',
    '<rootDir>/config/enzyme.setup.js'
  ],

  // The test environment that will be used for testing
  testEnvironment: 'jsdom',

  testTimeout: 15000
}

// A list of reporter names that Jest uses when writing coverage reports
JEST_CONFIG.coverageReporters = [
  'text-summary',
  'html'
]

// A list of reporter names that Jest uses when writing coverage reports
JEST_CONFIG.reporters = [
  'default'
]

module.exports = JEST_CONFIG

/**
 * Adds additional env that matches the webpack environment plugin
 * @see config/webpack.config.js
 **/
process.env = Object.assign(process.env, {
  GIT_SHORT_HASH: execSync('git rev-parse --short HEAD', { encoding: 'utf8' }).trim()
})
