/**
 * Pino logger provides a minimal JSON logger
 * @external pino/logger
 * @see [Pino `logger` instance]{@link https://getpino.io/#/docs/api?id=export}
 */

/**
 * Define each level of the Pino logger
 * @external pino/level
 * @see [Pino `level` attribute]{@link https://getpino.io/#/docs/api?id=logger-level}
 */

/**
 * StatusEnum defines a shared status for all UI components
 * @external pino/mergingObject
 * @see [Pino `mergingObject` option]{@link https://getpino.io/#/docs/api?id=mergingobject-object}
 */

/**
 * Structure provided by the `transmit` capability of the logger
 * @external pino/logEvent
 * @see [Pino `transmit` function]{@link https://getpino.io/#/docs/browser?id=transmit-object}
 */

/**
 * Array of bindings that is created by each child logger
 * @external pino/logBindings
 * @see [Pino `bindings` API]{@link https://getpino.io/#/docs/api?id=bindings}
 */

/**
 * Tuple that contains a callback and a value. The callback is triggering the value update.
 * @external react/customHook
 * @see [Documentation to build Custom hooks]{@link https://reactjs.org/docs/hooks-custom.html}
 */

/**
 * @module utils/logger
 * @desc Define all logging capabilities of the webapp
 */
