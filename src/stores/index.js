/**
 * @module stores/common
 * @desc Define all stores for common UI behaviors
 */

/**
 * @module stores/quiddity
 * @desc Define all stores for UI behaviors about quiddities
 */

/**
 * @module stores/matrix
 * @desc Define all stores for UI behaviors about the matrix
 */
