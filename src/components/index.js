/**
 * An Mobx observer component that reacts to each of its properties notifying an update
 * @external mobx-react/ObserverComponent
 * @see [Mobx's React guide]{@link https://mobx.js.org/react-integration.html}
 */

/**
 * A React component written with JSX
 * @external react/Component
 * @see [React component documentation]{@link https://reactjs.org/docs/components-and-props.html}
 */

/**
 * A React context that dispatches properties into nested children
 * @external react/Context
 * @see [React context documentation]{@link https://reactjs.org/docs/context.html}
 */

/**
 * The UI-Components Field
 * @external ui-components/Inputs/Field
 * @see [Field specification]{@link https://sat-mtl.gitlab.io/telepresence/ui-components/#/Components?id=field}
 */

/**
 * The UI-Components Menu item
 * @external ui-components/Menu.Item
 * @see [MenuItem specification]{@link https://sat-mtl.gitlab.io/telepresence/ui-components/#/Components?id=item}
 */

/**
 * The UI-Components Menu group
 * @external ui-components/Menu.Group
 * @see [MenuGroup specification]{@link https://sat-mtl.gitlab.io/telepresence/ui-components/#/Components?id=group}
 */

/**
 * The UI-Components Menu
 * @external ui-components/Menu
 * @see [MenuGroup specification]{@link https://sat-mtl.gitlab.io/telepresence/ui-components/#/Components?id=menu}
 */

/**
 * A UI-Components Cell
 * @external ui-components/Layout.Cell
 * @see [Cell specification]{@link https://sat-mtl.gitlab.io/telepresence/ui-components/#/Components?id=cell}
 */

/**
 * A UI-Components Tooltip
 * @external ui-components/Feedback.Tooltip
 * @see [Cell specification]{@link https://sat-mtl.gitlab.io/telepresence/ui-components/#/Components?id=tooltip}
 */

/**
 * React color components
 * @external react-color
 * @see [React Color homepage]{@link https://casesandberg.github.io/react-color/}
 */

/**
 * @module components/Common
 * @desc Define all common components
 */

/**
 * The UI Components Button
 * @external ui-components/Inputs.Button
 * @see [Button specification]{@link https://sat-mtl.gitlab.io/telepresence/ui-components/#/Components/Common/Others?id=button}
 */

/**
 * The UI Components Checkbox
 * @external ui-components/Inputs.Checkbox
 * @see [Checkbox specification]{@link https://sat-mtl.gitlab.io/telepresence/ui-components/#/Components/Inputs?id=checkbox}
 */

/**
 * @module components/fields
 * @desc Define all field components
 */

/**
 * @module components/pages
 * @desc Define all page components
 */

/**
 * @module components/bars
 * @desc Define all bar components
 */

/**
 * @module components/wrappers
 * @desc Define all wrapper components
 */

/**
 * @module components/modals
 * @desc Define all modal components
 */
