import React, { useState, useContext, useEffect } from 'react'

import { observer } from 'mobx-react'
import { useTranslation } from 'react-i18next'

import { AppStoresContext } from '@components/App'

import '@styles/panels/Menu.scss'
import '@styles/panels/FilterPanel.scss'

import { Inputs, Common, Layout, Context } from '@sat-mtl/ui-components'

const { Tag, Icon, Menu: { Menu, Item } } = Common
const { FlexRow } = Layout
const { Checkbox } = Inputs
const { ThemeContext } = Context

/**
 * Displays a filter menu item
 * @memberof module:components/panels.FilterPanel
 * @param {string} category - A category of a quiddity
 * @returns {external:react/Component} - The menu item
*/
const MenuItem = ({ category }) => {
  const { filterStore } = useContext(AppStoresContext)
  const { t } = useTranslation()

  const handleMenuItemClick = (category) => {
    if (filterStore.categoryFilters.has(category)) {
      filterStore.removeCategoryFilter(category)
    } else {
      filterStore.addCategoryFilter(category)
    }
  }
  return (
    <Item key={category} onClick={() => handleMenuItemClick(category)}>
      <FlexRow columnGap={8}>
        <Checkbox checked={filterStore.categoryFilters.has(category)} shape='square'>
          &#x2713;
        </Checkbox>
        <span className='CategoryFilterLabel'>
          {t(category)}
        </span>
      </FlexRow>
    </Item>
  )
}

/**
 * Displays a filter tag
 * @memberof module:components/panels.FilterPanel
 * @param {string} category - A category of a quiddity
 * @returns {external:react/Component} - The filter tag
*/
const FilterTag = ({ category }) => {
  const { filterStore } = useContext(AppStoresContext)
  const { t } = useTranslation()

  return (
    <Tag key={category} onClick={() => filterStore.removeCategoryFilter(category)}>
      {t(category)}
      <Icon type='close' />
    </Tag>
  )
}

/**
 * Displays all the applied filter tags
 * @memberof module:components/panels.FilterPanel
 * @returns {external:react/Component} - The filter tags container
*/
const Tags = () => {
  const { filterStore: { categoryFilters } } = useContext(AppStoresContext)

  const $tags = Array.from(categoryFilters)
    .map(category => <FilterTag key={category} category={category} />)

  return (
    <FlexRow id='FilterTags' columnGap={6} alignItems='center'>
      {$tags}
    </FlexRow>
  )
}

/**
 * Displays a link to clear all applied filters
 * @memberof module:components/panels.FilterPanel
 * @param {string} theme - The theme that is applied
 * @returns {external:react/Component} - The clear link
*/
const ClearLink = ({ theme }) => {
  const { filterStore } = useContext(AppStoresContext)
  const { t } = useTranslation()

  const linkProps = {
    key: 'ClearFilterLink',
    className: `link-${theme}`,
    onClick: () => filterStore.clearCategoryFilters()
  }

  return (
    <a {...linkProps}>
      {t('Clear filters')}
    </a>
  )
}

/**
 * Displays a filter menu with all available filters
 * @memberof module:components/panels.FilterPanel
 * @param {object} filterMenuRef - A reference object to the filter menu
 * @returns {external:react/Component} - The filter menu
*/
const FilterMenu = ({ filterMenuRef }) => {
  const [isMenuExpanded, setIsMEnuExpanded] = useState(false)
  const { filterStore: { userCategories } } = useContext(AppStoresContext)
  const { t } = useTranslation()

  const $filters = []

  for (const category of userCategories) {
    $filters.push(
      <MenuItem key={category} category={category} />
    )
  }

  return (
    <Menu
      key='FilterMenu'
      expanded={isMenuExpanded}
      title={t('Filters')}
      menuRef={filterMenuRef}
      onClick={() => setIsMEnuExpanded(!isMenuExpanded)}
      onBlur={() => isMenuExpanded ? setIsMEnuExpanded(false) : ''}
      width={125}
      addonAfter={<Icon type='down' />}
    >
      {$filters}
    </Menu>
  )
}

/** Displays a filter panel for quiddity categories
 * @Selector `#FilterPanel`
 * @returns {external:mobx-react/ObserverComponent} - The filter panel for sources and destinations
*/
const FilterPanel = observer(() => {
  const theme = useContext(ThemeContext)
  const { filterStore } = useContext(AppStoresContext)
  const filterMenuRef = React.createRef()

  /** Updates the focus on the menu when it is opened */
  useEffect(() => {
    if (filterMenuRef && filterMenuRef.current) {
      filterMenuRef.current.focus()
    }
  }, [])

  return (
    <div id='FilterPanel' className={`panel-${theme}`}>
      <FlexRow columnGap={20} alignItems='center'>
        <FilterMenu filterMenuRef={filterMenuRef} />
        <Tags />
        {filterStore.categoryFilters.size > 1 && <ClearLink theme={theme} />}
      </FlexRow>
    </div>
  )
})

export default FilterPanel
