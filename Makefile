ifndef $(shell command -v node 2> /dev/null)
	VERSION := $(shell cat package.json | grep version | sed -r 's/^.*"(.*)".*$$/\1/')
else
	VERSION := $(shell node -e "console.log(require('./package.json').version)")
endif

PREFIX := /usr/local
SCENICDIR := $(PREFIX)/share/scenic-client
ARCHIVE := scenic-client_$(VERSION)
BUNDLE_NAME := scenic-${VERSION}.tar.gz
LOCALES := public/locales
DIST_DIR := dist
TARGET_DIR := /var/www/scenic
NODE_MODULES_BIN := node_modules/.bin
I18NEXT := $(NODE_MODULES_BIN)/i18next

.PHONY: build
build: clean
	$(info Install all dependencies)
	npm ci
	$(info Building Scenic Client $(VERSION))
	NODE_OPTIONS="--max_old_space_size=4096" npm run build

.PHONY: bundle
bundle: build
	@echo Bundle current build
	tar -zcvf "${BUNDLE_NAME}.tgz" -C $(DIST_DIR) .

.PHONY: configure-h2o
configure-h2o:
	@echo Configure h2o Service
	@rm -f /etc/h2o/h2o.conf
	@mkdir -p /var/log/scenic
	@cp config/h2o.yml /etc/h2o/h2o.conf
	./scripts/restart_service.sh h2o

.PHONY: configure-apache2
configure-apache2:
	@echo Configure apache2 Service
	@rm -f /etc/apache2/sites-available/000-default.conf
	@cp config/apache.conf /etc/apache2/sites-enabled/scenic.conf
	./scripts/restart_service.sh apache2

.PHONY: copy-webapp
copy-webapp:
	$(info Copy webapp to $(TARGET_DIR))
ifneq ($(wildcard $(TARGET_DIR)),)
	rm -rd $(TARGET_DIR)
endif
	mkdir -p $(TARGET_DIR)
	cp -r $(DIST_DIR)/* $(TARGET_DIR)

.PHONY: install install-h2o install-apache2
install install-h2o: copy-webapp configure-h2o
install-apache2: copy-webapp configure-apache2

.PHONY: install-mkdocs serve-doc build-doc
install-mkdocs:
	$(info Install mkdocs python binary)
	pip install \
		-r docs/requirements.txt

.PHONY: sort-locale-keys
sort-locale-keys:
	$(info Sort all locale keys)
	fd . "assets/locales" \
		-e json \
		-x ./scripts/sort_locale_keys.sh {}

.PHONY: uninstall
uninstall:
	@echo "Uninstalling $(ARCHIVE)"
	@rm -rf /var/www/scenic
	@rm -f /etc/h2o/h2o.conf
	@rm -f /etc/apache2/sites-enabled/scenic.conf

.PHONY: clean clean-env
clean:
ifneq ($(wildcard $(DIST_DIR)),)
	$(info Cleaning DIST directory)
	rm -rf "$(DIST_DIR)"
	$(info Clean the NPM cache)
	npm cache clean -f
endif

CI_REGISTRY_IMAGE := "registry.gitlab.com/sat-mtl/tools/scenic/scenic"

build-switcher:
	docker build \
		--rm \
		--no-cache=true \
		-f dockerfiles/switcher.dockerfile \
		-t $(CI_REGISTRY_IMAGE):switcher .

run-switcher:
	docker run -i -t --rm \
		-p 8000:8000 \
		--name switcher \
		$(CI_REGISTRY_IMAGE):switcher

build-swIO:
	docker build \
		--rm \
		--no-cache=true \
		-f dockerfiles/swIO.dockerfile \
		-t $(CI_REGISTRY_IMAGE):swIO .

run-swIO:
	docker run -i -t --rm \
		-p 8000:8000 \
		--name swIO \
		$(CI_REGISTRY_IMAGE):swIO
