import Quiddity from '@models/quiddity/Quiddity'

import {
  NDI_INPUT_KIND_ID,
  NDI_OUTPUT_KIND_ID,
  NDI_SNIFFER_KIND_ID,
  NDI_SNIFFER_NICKNAME,
  SIP_ID,
  SIP_KIND_ID,
  RTMP_KIND_ID
} from '@models/quiddity/specialQuiddities'

import SDI_INPUT_QUIDDITY from '@fixture/models/quiddity.sdiInput1.json'
import VIDEO_OUTPUT_QUIDDITY from '@fixture/models/quiddity.videoOutput.json'
import SYSTEM_USAGE_QUIDDITY from '@fixture/models/quiddity.systemUsage.json'
import RTMP_QUIDDITY from '@fixture/models/quiddity.rtmp0.json'
import SIP_MEDIA_QUIDDITY from '@fixture/models/quiddity.sipMedia0.json'
import NDI_OUTPUT_QUIDDITY from '@fixture/models/quiddity.ndiOutput0.json'
import H264_ENCODER_QUIDDITY from '@fixture/models/quiddity.h264Encoder0.json'
import XR18_OUTPUT_QUIDDITY from '@fixture/models/quiddity.xr18Output.json'
import VIDEO_TEST_INPUT_QUIDDITY from '@fixture/models/quiddity.videotestsrcWithWriter.json'
import JACK_INPUT_QUIDDITY from '@fixture/models/quiddity.jackinput.json'

const test = new Quiddity('test', 'test0', 'test')

const sdiInput = Quiddity.fromJSON(SDI_INPUT_QUIDDITY)
const videoOutput = Quiddity.fromJSON(VIDEO_OUTPUT_QUIDDITY)
const systemUsage = Quiddity.fromJSON(SYSTEM_USAGE_QUIDDITY)
const sipMedia = Quiddity.fromJSON(SIP_MEDIA_QUIDDITY)
const encoderH264 = Quiddity.fromJSON(H264_ENCODER_QUIDDITY)
const videotestsrc = Quiddity.fromJSON(VIDEO_TEST_INPUT_QUIDDITY)
const jacksrc = Quiddity.fromJSON(JACK_INPUT_QUIDDITY)

const sip = new Quiddity(SIP_ID, 'sip0', SIP_KIND_ID)

const ndiSniffer = new Quiddity(NDI_SNIFFER_NICKNAME, 'ndiSniffer0', NDI_SNIFFER_KIND_ID)
const ndiInput = new Quiddity(NDI_INPUT_KIND_ID, 'ndiInput0', NDI_INPUT_KIND_ID)
const ndiOutput = Quiddity.fromJSON({
  ...NDI_OUTPUT_QUIDDITY,
  id: NDI_OUTPUT_KIND_ID,
  kindId: NDI_OUTPUT_KIND_ID
})

const rtmpOutput = Quiddity.fromJSON({
  ...RTMP_QUIDDITY,
  id: RTMP_KIND_ID,
  kindId: RTMP_KIND_ID
})

const xr18Output = Quiddity.fromJSON({
  ...XR18_OUTPUT_QUIDDITY,
  id: 'xr18Output0',
  kindId: 'xr18Output'
})

function copyQuiddity (quiddity, newId) {
  return Quiddity.fromJSON({ ...quiddity.toJSON(), id: newId })
}

export {
  copyQuiddity,
  test,
  sdiInput,
  videoOutput,
  systemUsage,
  sip,
  ndiSniffer,
  ndiInput,
  ndiOutput,
  rtmpOutput,
  sipMedia,
  encoderH264,
  xr18Output,
  videotestsrc,
  jacksrc
}
