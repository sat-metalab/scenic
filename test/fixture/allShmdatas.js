import Shmdata from '@models/shmdata/Shmdata'
import sdiInputShmdatas from '@fixture/models/shmdatas.sdiInput1.json'
import ndiInputShmdatas from '@fixture/models/shmdatas.ndiInput0.json'

const video = new Shmdata(
  '/tmp/switcher_default_1_video',
  [
    'video/x-raw',
    'width=(int)800',
    'height=(int)600',
    'framerate=(fraction)25/1',
    'pixel-aspect-ratio=(fraction)1/1',
    'format=(string)I420'
  ],
  'video'
)

const ndiVideo = new Shmdata(
  '/tmp/scenic/ndi_7387998559783/ndi7387998559783_video',
  [
    'video/x-raw',
    'width=(int)800',
    'height=(int)600',
    'framerate=(fraction)25/1',
    'pixel-aspect-ratio=(fraction)1/1',
    'format=(string)I420'
  ],
  'video'
)

const ndiAudio = new Shmdata(
  '/tmp/scenic/ndi_7387998559783/ndi7387998559783_audio',
  [
    'audio/x-raw'
  ],
  'audio'
)

const sdiInputShmdataPath = '/tmp/switcher_sdi10_1_1'
const encodedSdiInputShmdataPath = '/tmp/switcher_sdi10_2_1'

const ndiVideoInputShmdataPath = '/tmp/scenic/ndi_7387998559783/ndi7387998559783_video'
const ndiAudioInputShmdataPath = '/tmp/scenic/ndi_7387998559783/ndi7387998559783_audio'

const sdiInputShmdata = Shmdata.fromJSON(
  sdiInputShmdataPath,
  sdiInputShmdatas.writer[sdiInputShmdataPath]
)

const encodedSdiInputShmdata = Shmdata.fromJSON(
  encodedSdiInputShmdataPath,
  sdiInputShmdatas.writer[encodedSdiInputShmdataPath]
)

const ndiVideoInputShmdata = Shmdata.fromJSON(
  ndiVideoInputShmdataPath,
  ndiInputShmdatas.writer[ndiVideoInputShmdataPath]
)

const ndiAudioInputShmdata = Shmdata.fromJSON(
  ndiAudioInputShmdataPath,
  ndiInputShmdatas.writer[ndiAudioInputShmdataPath]
)

const addShmdataToInfoTree = (json, shmdata) => {
  const connectedQuid = json
  connectedQuid.infoTree.shmdata = shmdata
  return connectedQuid
}

export {
  video,
  sdiInputShmdataPath,
  encodedSdiInputShmdataPath,
  sdiInputShmdata,
  encodedSdiInputShmdata,
  ndiVideo,
  ndiAudio,
  ndiVideoInputShmdataPath,
  ndiAudioInputShmdataPath,
  ndiVideoInputShmdata,
  ndiAudioInputShmdata,
  addShmdataToInfoTree
}
