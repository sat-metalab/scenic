/* global describe it expect beforeAll afterAll beforeEach */

import { createSocket } from '@test/testTools'

import QuiddityAPI from '@api/quiddity/QuiddityAPI'
import NicknameAPI from '@api/quiddity/NicknameAPI'
import SessionAPI from '@api/switcher/SessionAPI'

import switcherKinds from '@fixture/switcher.kinds.json'

describe('NicknameAPI', () => {
  const OSCsinkKind = switcherKinds.kinds[0].kind
  const socket = createSocket('ws://localhost', 8000)

  let quiddityAPI, sessionAPI, nicknameAPI
  let quid

  beforeAll((done) => {
    socket.on('connect', () => {
      quiddityAPI = new QuiddityAPI(socket)
      sessionAPI = new SessionAPI(socket)
      nicknameAPI = new NicknameAPI(socket)
      done()
    })

    socket.open()
  })

  beforeEach(async () => {
    await sessionAPI.reset()
    quid = await quiddityAPI.create(OSCsinkKind, 'test')
  })

  afterAll(() => socket.close())

  describe('get', () => {
    it('should get the current nickname of a quiddity', async () => {
      const nickname = await nicknameAPI.get(quid.id)
      expect(nickname).toEqual('test')
    })
  })

  describe('set', () => {
    it('should set a new nickname for the quiddity', async () => {
      expect(nicknameAPI.set(quid.id, 'test2')).resolves.toEqual(true)
      expect(nicknameAPI.get(quid.id)).resolves.toEqual('test2')
    })
  })

  describe('onUpdated', () => {
    it('should receive an update when a new nickname is set', (done) => {
      nicknameAPI.onUpdated(
        (quidId, updatedNickname) => {
          expect(updatedNickname).toEqual('test2')
          expect(quidId).toEqual(quid.id)
          done()
        },
        quidId => quidId === quid.id
      )

      nicknameAPI.set(quid.id, 'test2')
    })
  })
})
