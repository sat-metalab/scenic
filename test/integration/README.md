# Test suite for SwitcherIO

These test cases are designed to battle test the Scenic API model and the current statement of SwitcherIO. It sould be released once this test suite is succeded.

We assume all the following commands are executed from the `root` folder of this project.

## Build the image for tests

A docker image with `switcherIO` should be used to execute this test suite. An image is provided in the `dockerfiles` folder, it is built on top of the `apt` packages of `switcher` and `shmdata`.

```bash
CI_REGISTRY_IMAGE="registry.gitlab.com/sat-mtl/tools/scenic/scenic"

docker build --rm \
  -f dockerfiles/tests.Dockerfile \
  -t "${CI_REGISTRY_IMAGE}/tests" .
```

## Run the image for tests

```bash
docker run -it --rm -p 8000:8000 "${CI_REGISTRY_IMAGE}/tests"
```

## Publish new SwitcherIO image in the Scenic registry

```bash
docker login registry.gitlab.com
docker push "${CI_REGISTRY_IMAGE}/tests"
```
