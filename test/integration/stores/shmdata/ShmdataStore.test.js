/* global describe it expect beforeAll beforeEach afterAll */

import { createSocket, delay } from '@test/testTools'

import populateStores from '@stores/populateStores'

describe('ShmdataStore', () => {
  let socketStore, quiddityStore, propertyStore, shmdataStore, configStore, kindStore
  let stores
  const socket = createSocket('ws://localhost', 8000)

  beforeAll((done) => {
    stores = populateStores()

    ;({
      socketStore,
      kindStore,
      quiddityStore,
      propertyStore,
      shmdataStore,
      configStore
    } = stores)

    socket.on('connect', async () => {
      socketStore.setActiveSocket(socket)
      await configStore.initialize()
      await kindStore.initialize()
      await delay(3000)
      done()
    })

    socket.open()
  })

  beforeEach(async () => {
    await socketStore.APIs.sessionAPI.clear()
  })

  afterAll(() => {
    socket.close()
  })

  it('should detect writer shmdata when a source is started', async () => {
    const quid = await quiddityStore.applyQuiddityCreation('videotestsrc', 'src')
    await delay(2000)
    await propertyStore.applyNewPropertyValue(quid.id, 'started', true)
    await delay(2000)
    expect(shmdataStore.writerPaths.get(quid.id).size).toEqual(1)
  })

  it('should detect follower shmdata when a destination is connected', async () => {
    const oscSrc = await quiddityStore.applyQuiddityCreation('oscInput', 'oscSrc')
    await propertyStore.applyNewPropertyValue(oscSrc.id, 'started', true)
    const oscDst = await quiddityStore.applyQuiddityCreation('oscOutput', 'oscDst')

    await delay(2000)
    await quiddityStore.applyQuiddityConnection(oscSrc.id, oscDst.id)
    await delay(2000)

    expect(shmdataStore.followerPaths.get(oscDst.id).size).toEqual(1)
  })
})
