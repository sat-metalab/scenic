/* global describe it jest expect beforeAll afterAll */

import { createSocket } from '@test/testTools'
import { when } from 'mobx'

import populateStores from '~/src/stores/populateStores'
import { initializeStores } from '~/src/stores/initializeStores'
import InitStateEnum from '@models/common/InitStateEnum'

import Connection from '@models/userTree/Connection'

import { USER_TREE_CONFIG } from '@models/quiddity/specialQuiddities'

describe('ConnectionStore', () => {
  let socketStore, sceneStore, connectionStore, quiddityStore, configStore
  let stores
  const socket = createSocket('ws://localhost', 8000)

  let initSpy

  beforeAll((done) => {
    stores = populateStores()

    ;({
      socketStore,
      connectionStore,
      quiddityStore,
      sceneStore,
      configStore
    } = stores)

    socket.on('connect', async () => {
      socketStore.setActiveSocket(socket)

      await socketStore.APIs.sessionAPI.clear()
      await initializeStores(stores)

      done()
    })

    configStore.defaultScenic.initQuiddities = [USER_TREE_CONFIG]
    initSpy = jest.spyOn(connectionStore, 'initialize')

    socket.open()
  })

  afterAll(() => {
    socket.close()
  })

  describe('initialization', () => {
    it('should be initialized after the SceneStore is initialized', async () => {
      await when(
        () => sceneStore.initState === InitStateEnum.INITIALIZED,
        () => expect(initSpy).toHaveBeenCalled()
      )
    })
  })

  describe('quiddity connection/disconnection', () => {
    it('should connect and disconnect two compatible quiddities', async () => {
      const oscSrc = await quiddityStore.applyQuiddityCreation('oscInput', 'oscSrc')
      const oscDst = await quiddityStore.applyQuiddityCreation('oscOutput', 'oscDst')

      const connection = new Connection(oscSrc.id, oscDst.id)

      await when(() => connectionStore.initState === InitStateEnum.INITIALIZED)

      // check if the selected scene is active
      expect(sceneStore.activeScene).toEqual(sceneStore.selectedScene)

      await expect(connectionStore.applyUserConnection(connection))
        .resolves.toEqual(true)

      await expect(connectionStore.applyUserDisconnection(connection))
        .resolves.toEqual(true)
    })

    it('should disconnect two compatible quiddities', () => {

    })

    it.todo('should connect a quiddity according the connection specs')
    it.todo('should switch connections when the number of connections is overflow')
  })

  describe('connection armement/disarmement', () => {
    it.todo('should connect a quiddity according the connection specs')
    it.todo('should switch connections when the number of connections is overflow')
  })
})
