/* global describe it jest expect beforeAll beforeEach afterAll */

import { createSocket, delay } from '@test/testTools'
import populateStores from '~/src/stores/populateStores'
import { initializeStores } from '~/src/stores/initializeStores'

import {
  USER_TREE_NICKNAME,
  USER_TREE_KIND_ID,
  USER_TREE_CONFIG
} from '@models/quiddity/specialQuiddities'

describe('UserTreeStore', () => {
  let socketStore, quiddityStore, configStore, kindStore, userTreeStore
  let stores
  const socket = createSocket('ws://localhost', 8000)

  let fallbackSpy

  beforeAll((done) => {
    stores = populateStores()

    ;({
      socketStore,
      configStore,
      kindStore,
      quiddityStore,
      userTreeStore
    } = stores)

    socket.on('connect', () => {
      socketStore.setActiveSocket(socket)
      done()
    })

    socket.open()

    fallbackSpy = jest.spyOn(userTreeStore, 'fallbackUserTreeQuiddity')

    configStore.initialize = jest.fn().mockResolvedValue(true)
    configStore.defaultScenic.initQuiddities = [USER_TREE_CONFIG]
  })

  afterAll(() => {
    socket.close()
  })

  describe('initialization', () => {
    beforeEach(async () => {
      await socketStore.APIs.sessionAPI.clear()
      await initializeStores(stores)
    })

    it('should be grafted when the initialization is finished', async () => {
      expect(configStore.initQuiddities.length).toEqual(1)
      expect(fallbackSpy).toHaveBeenCalled()
      await delay(2000)
      expect(quiddityStore.usedKindIds.has(USER_TREE_KIND_ID)).toEqual(true)
      const tree = await userTreeStore.fetchUserTree()
      expect(tree.scenes).toEqual(USER_TREE_CONFIG.userTree.scenes)
      expect(tree).toHaveProperty('connections')
    })
  })

  describe('fallback', () => {
    beforeEach(async () => {
      await socketStore.APIs.sessionAPI.clear()
      await configStore.initialize()
      await kindStore.initialize(true)
    })

    it('should not be fallbacked when it is already created', async () => {
      await quiddityStore.applyQuiddityCreation(USER_TREE_KIND_ID, USER_TREE_NICKNAME)
      await delay(1000)
      await initializeStores(stores)
      await delay(1000)
      expect(fallbackSpy).not.toHaveBeenCalled()
    })
  })
})
