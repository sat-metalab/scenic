/* global describe it expect beforeAll beforeEach afterAll */

import { createSocket, delay } from '@test/testTools'
import { when } from 'mobx'

import populateStores from '~/src/stores/populateStores'
import Quiddity from '@models/quiddity/Quiddity'

describe('QuiddityStore', () => {
  let socketStore, quiddityStore, kindStore, configStore
  const socket = createSocket('ws://localhost', 8000)

  beforeAll((done) => {
    ({
      socketStore,
      quiddityStore,
      kindStore,
      configStore
    } = populateStores())

    socket.on('connect', () => {
      socketStore.setActiveSocket(socket)
      done()
    })

    socket.open()
  })

  beforeEach(async () => {
    await socketStore.APIs.sessionAPI.clear()
    await configStore.initialize()
    await delay(4000)
    await kindStore.initialize()
    expect(kindStore.kinds.has('oscOutput')).toEqual(true)
    await quiddityStore.initialize()
  })

  afterAll(() => {
    socket.close()
  })

  describe('creation', () => {
    function createAndDestroyQuiddity (quidKind, quidName, done) {
      quiddityStore.applyQuiddityCreation(quidKind, quidName).then(() => {
        when(
          () => quiddityStore.quiddities.size >= 1,
          async () => {
            const [quid] = quiddityStore.quiddities.values()
            expect(quid).toBeInstanceOf(Quiddity)
            const numOfQuiddities = quiddityStore.quiddities.size
            await quiddityStore.applyQuiddityRemoval(quid.id)

            when(
              () => quiddityStore.quiddities.size === numOfQuiddities - 1,
              () => done()
            )
          }
        )
      })
    }

    it('should create and destroy a basic quiddity', (done) => {
      createAndDestroyQuiddity('OSCsink', 'sinkTest', done)
    })

    it('should create and destroy a bundled quiddity', (done) => {
      createAndDestroyQuiddity('oscOutput', 'oscTest', done)
    })

    it('should create configured quiddities with default index', async () => {
      const port = 1000

      await quiddityStore.applyQuiddityCreation('OSCsink', 'sinkTest', { port })
      await delay(1000)
      const quid = Array.from(quiddityStore.quiddities.values()).filter(
        quid => quid.nickname === 'sinkTest'
      )[0]
      await expect(socketStore.APIs.propertyAPI.get(quid.id, 'port')).resolves.toEqual(port)
      await expect(socketStore.APIs.userTreeAPI.get(quid.id)).resolves.toHaveProperty('index', 0)
    })
  })

  describe('sources and destinations', () => {
    it('should create a source quiddity as oscInput', (done) => {
      quiddityStore.applyQuiddityCreation('oscInput', 'source').then(() => {
        when(
          () => quiddityStore.sources.length >= 1,
          () => {
            const [quid] = quiddityStore.sources
            expect(quid.kindId).toEqual('oscInput')
            done()
          }
        )
      })
    })

    it('should create a destination quiddity as oscOutput', (done) => {
      quiddityStore.applyQuiddityCreation('oscOutput', 'destination').then(() => {
        when(
          () => quiddityStore.destinations.length >= 1,
          () => {
            const [quid] = quiddityStore.destinations
            expect(quid.kindId).toEqual('oscOutput')
            done()
          }
        )
      })
    })
  })

  describe('connection and disconnection', () => {
    it('should connect and disconnect two compatible quiddities', async () => {
      const oscSrc = await quiddityStore.applyQuiddityCreation('oscInput', 'oscSrc')
      const oscDst = await quiddityStore.applyQuiddityCreation('oscOutput', 'oscDst')

      const sfId = await quiddityStore.applyQuiddityConnection(oscSrc.id, oscDst.id)
      expect(sfId).toBeDefined()

      await expect(
        quiddityStore.applyQuiddityDisconnection(oscDst.id, sfId))
        .resolves.toBeTruthy()
    })

    it('should not connect incompatible quiddities', async () => {
      const vidSrc = await quiddityStore.applyQuiddityCreation('videotestsrc', 'vidSrc')
      const oscDst = await quiddityStore.applyQuiddityCreation('oscOutput', 'oscDst')

      await expect(
        quiddityStore.applyQuiddityConnection(vidSrc.id, oscDst.id))
        .resolves.toBeNull()
    })
  })
})
