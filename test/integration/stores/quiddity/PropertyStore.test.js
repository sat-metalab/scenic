/* global jest describe it expect beforeAll beforeEach afterAll */

import { createSocket, delay } from '@test/testTools'

import populateStores from '~/src/stores/populateStores'

describe('PropertyStore', () => {
  let stores
  let socketStore, quiddityStore, propertyStore, kindStore, configStore
  const socket = createSocket('ws://localhost', 8000)
  let initSpy, updatedSpy

  beforeAll((done) => {
    stores = populateStores()

    ;({
      socketStore,
      kindStore,
      quiddityStore,
      propertyStore,
      configStore
    } = stores)

    socket.on('connect', async () => {
      socketStore.setActiveSocket(socket)
      await configStore.initialize()
      await delay(2000)
      await kindStore.initialize()
      done()
    })

    socket.open()

    initSpy = jest.spyOn(propertyStore, 'initializeProperties')
  })

  beforeEach(async () => {
    await socketStore.APIs.sessionAPI.clear()
  })

  afterAll(() => {
    socket.close()
  })

  describe('initialization', () => {
    it('should be initialized when a quiddity is created', async () => {
      const quid = await quiddityStore.applyQuiddityCreation('oscInput', 'source')
      await delay(3000)
      expect(initSpy).toHaveBeenCalledWith(quid.id)
    })
  })

  describe('start', () => {
    /**
     * @todo debug `property.updated` signal that is not systematically sent for `started` property
     * it seems working with `videotestsrc` but `oscInput` is still sending false negative
     */
    it('should start a startable quiddity', async () => {
      const quid = await quiddityStore.applyQuiddityCreation('videotestsrc', 'source')
      await delay(2000)
      expect(propertyStore.isStartable(quid.id)).toEqual(true)
      expect(propertyStore.isStarted(quid.id)).toEqual(false)
      const prop = propertyStore.getStartableProperty(quid.id)
      await propertyStore.applyNewPropertyValue(quid.id, prop.id, true)
      await delay(2000)
      expect(propertyStore.isStarted(quid.id)).toEqual(true)
    })
  })

  describe('update', () => {
    it('should be updated when a property is set', async () => {
      updatedSpy = jest.spyOn(propertyStore, 'handleUpdatedProperty')
      const quid = await quiddityStore.applyQuiddityCreation('oscInput', 'source')
      await delay(2000)
      expect(propertyStore.getPropertyValue(quid.id, 'OSC/port')).toEqual(1056)
      await propertyStore.applyNewPropertyValue(quid.id, 'OSC/port', 2222)
      await delay(2000)
      expect(updatedSpy).toHaveBeenCalledWith(quid.id, 'OSC/port', 2222)
    })
  })
})
