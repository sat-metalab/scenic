from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

tools_url = "https://gitlab.com/sat-mtl/tools"

def test_help_page(scenic_instance_1) -> None:
    try:

        # Find and click the Help Page Tab
        # TODO Left Sidebar menu items do not contain custom selectors; when they do replace XPath Selectors with CSS Selectors
        scenic_instance_1.driver.find_element(By.XPATH, '//div[contains(text(), "help")]').click()

        # Search for the Help Page ID and assert that it is present
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#HelpPage'), 'The Help Page ID did not appear'

        # Find the Assistance Section and assert its presence
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '#AssistanceSection'), 'The Assistance Section did not appear'

        # Find the Info Section
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#InfoSection'), 'The Info Section did not appear'

        # Find the Contributing Section and assert its presence
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '#ContributingSection'), 'The Contributing Section did not appear'

        # Find the About Section and assert its presence
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, "#AboutSection"), 'The About Section did not appear'

        # Find the Metalab section and assert its presence
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '#MetalabSection'), 'The Metalab Section did not appear'

        # Find and assert the presence of the various gitlab links/icons
        assert scenic_instance_1.driver.find_element(
            By.XPATH, f'//a[@href="{tools_url}/scenic/scenic"]'), 'The Scenic gitlab link does not appear'
        assert scenic_instance_1.driver.find_element(
            By.XPATH, f'//a[@href="{tools_url}/switcher"]'), 'The Switcher gitlab link does not appear'
        assert scenic_instance_1.driver.find_element(
            By.XPATH, f'//a[@href="{tools_url}/shmdata"]'), 'The Shmdata gitlab link does not appear'
        assert scenic_instance_1.driver.find_element(
            By.XPATH, f'//a[@href="{tools_url}/splash/splash"]'), 'The Splash gitlab link does not appear'
        assert scenic_instance_1.driver.find_element(
            By.XPATH, f'//a[@href="{tools_url}/satie/SATIE"]'), 'The SATIE gitlab link does not appear'
        assert scenic_instance_1.driver.find_element(
            By.XPATH, f'//a[@href="{tools_url}/vaRays"]'), 'The vaRays gitlab link does not appear'
        assert scenic_instance_1.driver.find_element(
            By.XPATH, f'//a[@href="{tools_url}/EditionInSitu"]'), 'The EditionInSitu gitlab link does not appear'
        assert scenic_instance_1.driver.find_element(
            By.XPATH, f'//a[@href="{tools_url}/splash/calimiro"]'), 'The Calimiro gitlab link does not appear'
        scenic_instance_1.return_to_main_page()

    except NoSuchElementException as e:
        scenic_instance_1.return_to_main_page()
        assert False, f'UI element could not be found. Error: {e.msg}'
