from selenium.common.exceptions import NoSuchElementException

def test_basic_page_elements(scenic_instance_1):
    # This tests for the presence of the major page elements that we expect when Scenic loads
    try:

        # Test for the presence of the main page elements
        scenic_instance_1.assert_element_presence('.ScenicSignature')
        scenic_instance_1.assert_element_presence('#PageMenu')
        scenic_instance_1.assert_element_presence('#MatrixPage')
        scenic_instance_1.assert_element_presence('#MatrixMenuPanel')
        scenic_instance_1.assert_element_presence('#FileControlPanel')
        scenic_instance_1.assert_element_presence('#StatusBar')
        scenic_instance_1.assert_element_presence('.SideBar')

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
