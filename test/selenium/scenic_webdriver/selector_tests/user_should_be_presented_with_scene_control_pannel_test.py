import time
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

def test_scene_control_panel_elements(scenic_instance_1):

    try:
        time.sleep(5)
        scenic_instance_1.assert_element_presence('#SceneControlPanel')
        scenic_instance_1.assert_element_presence('.TabBar')
        scenic_instance_1.assert_element_presence('#SceneTabBar')
        scenic_instance_1.assert_element_presence('.InputTextField[value="Default scene"]')
        scenic_instance_1.assert_element_presence('.AddSceneButton')
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.AddSceneButton').click()
        scenic_instance_1.assert_element_presence('.DeleteSceneButton')

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
