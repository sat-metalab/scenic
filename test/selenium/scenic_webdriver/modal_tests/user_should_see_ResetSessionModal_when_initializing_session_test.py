from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import time

def test_ResetSessionModal(scenic_instance_1) -> None:

    try:
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.NewSessionMenu .icon-new-file').click()

        # Assert that the ResetSessionModal appears
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#ResetSessionModal')

        # Assert the presence of the cancel button
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.CancelButton')

        # Click the confirmation button in the modal - will also assert its presence
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton').click()


    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
