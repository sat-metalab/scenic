from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import time

def test_RenameNicknameModal(scenic_instance_1) -> None:
    TEST_QUIDDITY_NAME = 'Test!'
    try:
        # Create a source
        scenic_instance_1.create_quiddity_from_menu('Sources', 'Video', 'SDI 1')
        scenic_instance_1.driver.implicitly_wait(5)

        # Open its property panel
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SourceHead[data-nickname="sdiInput11"]').click()

        # Find the NicknameInput and click it, clear it, and enter a new name
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.NicknameInput .InputTextField').click()
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.NicknameInput .InputTextField').clear()
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.NicknameInput .InputTextField').send_keys(TEST_QUIDDITY_NAME)

        # Change focus from input text field
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#Matrix').click()

        # Assert the presence of the RenameNicknameModal and the confirm and cancel buttons
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#RenameNicknameModal'), 'The RenameNicknameModal did not appear'
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.CancelButton'), 'The RenameNicknameModal appeared but there is no Cancel button'
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.RenameButton'), 'The RenameNicknameModal appeared, but there is not Confirm button'

        # Might as well test that the nickname changing function works, while we're here.
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.RenameButton').click()
        quiddity_name = scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SourceHead .EntryTitle').text

        assert quiddity_name == TEST_QUIDDITY_NAME, 'The quiddity renaming did not work'


    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
