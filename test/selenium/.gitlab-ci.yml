variables:
  RUNNER_HOSTNAME:
    description: "The specific GitLab runner hostname. Each runner needs to have a tag manually defined with its hostname."

stages:
  - selenium

.selenium-template:
  variables:
    # enable build container to talk to service containers (scenic web server
    # and scenic-core instances).
    # build container connects to service container (scenic) via http to fetch Scenic
    # build container connects to service containers (scenic-core instances) via websocket
    # service containers (scenic-core) connects to X server running on build via TCP
    FF_NETWORK_PER_BUILD: "true"
    # hide scenic-core services logs
    CI_DEBUG_SERVICES: "false"
    SCENIC_SELENIUM_IMAGE: registry.gitlab.com/sat-mtl/tools/scenic/scenic/selenium
    SCENIC_SELENIUM_TAG: "develop"
    SCENIC_IMAGE: registry.gitlab.com/sat-mtl/tools/scenic/scenic
    SCENIC_CORE_IMAGE: registry.gitlab.com/sat-mtl/tools/scenic/scenic-core
    # SIP server hostname and SIP password are configured in project CI/CD variables
    SIP_HOST: $CI_SIP_HOST
    SIP_PASSWORD: $CI_SIP_PASSWORD
    # SIP users are configured by internal policy (see: https://confluence.sat.qc.ca/x/EoHIEQ)
    SIP_USER1: scenic-ci1
    SIP_USER2: scenic-ci2
    SIP_USER3: scenic-ci3
    # selenium paths
    SELENIUM_DIR: test/selenium
    SELENIUM_SCRIPT: scripts/ci/test-and-record.sh
  tags:
    - ${RUNNER_HOSTNAME}
  timeout: 30 minute
  image: $SCENIC_SELENIUM_IMAGE:$SCENIC_SELENIUM_TAG
  stage: selenium
  services:
    - name: $SCENIC_IMAGE:$SCENIC_TAG
      alias: scenic
    - name: $SCENIC_CORE_IMAGE:$SCENIC_CORE_TAG
      alias: scenic-core-1
    - name: $SCENIC_CORE_IMAGE:$SCENIC_CORE_TAG
      alias: scenic-core-2
    - name: $SCENIC_CORE_IMAGE:$SCENIC_CORE_TAG
      alias: scenic-core-3
  before_script:
    - cd $SELENIUM_DIR
    # Install scenic-selenium Python dependencies from pypy into a venv
    - pipenv install --dev
    # Start Xvfb X server
    - export DISPLAY=:0
    - Xvfb :0 -screen 0 1920x1080x24 -ac -listen tcp &
    # Wait for X server to be ready before starting recording
    - sleep 3
    # Configure scenic-selenium variables
    - |
      cat << EOF > .env
      SERVER_ADDRESS=$SIP_HOST
      SCENIC1_URL="http://scenic:8080"
      SWITCHER1_HOST="scenic-core-1"
      SWITCHER1_PORT="8000"
      USER1_SIP_SOURCE_PORT="5060"
      USER1_NAME=$SIP_USER1
      USER1_PASSWORD=$SIP_PASSWORD
      SCENIC2_URL="http://scenic:8080"
      SWITCHER2_HOST="scenic-core-2"
      SWITCHER2_PORT="8000"
      USER2_SIP_SOURCE_PORT="5060"
      USER2_NAME=$SIP_USER2
      USER2_PASSWORD=$SIP_PASSWORD
      SCENIC3_URL="http://scenic:8080"
      SWITCHER3_HOST="scenic-core-3"
      SWITCHER3_PORT="8000"
      USER3_SIP_SOURCE_PORT="5060"
      USER3_NAME=$SIP_USER3
      USER3_PASSWORD=$SIP_PASSWORD
      PATH_TO_DOWNLOADS_FOLDER="/home/$USER/Downloads"
      EOF
  script:
    ## Basic tests ##
    # Start boot test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/scenic_boot_test.py false
    # Start source test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/source_tests/user_should_be_able_to_instantiate_and_power_on_a_source_test.py false
    # Start SIP login test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/sip_tests/user_should_be_able_to_login_to_sip_test.py false
    # Start unilateral SIP call test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/sip_tests/user_should_be_able_to_make_a_unilateral_sip_call_test.py false
    ## Essential tests ##
    # Start basic UI test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/selector_tests/user_should_be_presented_with_basic_ui_test.py false
    # Start compression menu test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/selector_tests/user_should_be_presented_with_compression_menu_test.py false
    # Start destination menu test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/selector_tests/user_should_be_presented_with_destinations_menu_test.py false
    # Start side bar test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/selector_tests/user_should_be_presented_with_side_bar_test.py false
    # Start sources menu test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/selector_tests/user_should_be_presented_with_sources_menu_test.py false
    # Start status bar test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/selector_tests/user_should_be_presented_with_status_bar_test.py false
    # Start settings page test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/page_tests/user_should_be_able_to_open_the_settings_page_test.py false
    # Start page menu test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/selector_tests/user_should_be_presented_with_page_menu_test.py false
    # Start help page test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/page_tests/user_should_be_able_to_open_the_help_page_test.py false
    # Start file drawer test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/selector_tests/user_should_be_presented_file_drawer_test.py false
    # Start sip drawer test, do not allow it to fail
    - $SELENIUM_SCRIPT scenic_webdriver/selector_tests/user_should_be_presented_with_sip_drawer_test.py false
  after_script:
    # Wait for ffmpeg to cleanly exit, stop recording before Xvfb exits, ffmpeg x11grab crashes if X server quits before ffmpeg.
    - sleep 3
    - pkill Xvfb
  artifacts:
    when: always
    paths:
      - $SELENIUM_DIR/log/*
      - $SELENIUM_DIR/recording/*

selenium:
  variables:
    SCENIC_TAG: $CI_COMMIT_REF_NAME
    SCENIC_CORE_TAG: $CI_COMMIT_REF_NAME
  extends:
    - .selenium-template
  needs:
    - release-docker-image
  stage: selenium
  only: ['tags', 'develop', 'master']

manual-selenium-from-branch:
  variables:
    SCENIC_TAG: $CI_COMMIT_REF_SLUG
    SCENIC_CORE_TAG: "develop"
  extends:
    - .selenium-template
  needs: []
  stage: selenium
  allow_failure: true
  when: manual