/* global describe it expect beforeEach */

import { Socket, mockSocketCallback } from '@fixture/api/socket.mock'

import ConnectionSpecsAPI from '@api/tree/ConnectionSpecsAPI'

describe('ConnectionSpecsAPI', () => {
  const QUIDDITY_TEST = 'test'
  const CONNECTION_DATA_TEST = 'test'
  const CONNECTION_DATA_ERROR = new Error()

  let api, socket, mockEmitSocketCallback

  beforeEach(() => {
    socket = new Socket()
    api = new ConnectionSpecsAPI(socket)
    mockEmitSocketCallback = mockSocketCallback.bind(null, api.socket, 'emit')
  })

  describe('constructor', () => {
    it('should expect ConnectionSpecsAPI to be defined', () => {
      expect(api).toBeDefined()
      expect(api.socket).toStrictEqual(socket)
    })
  })

  describe('get', () => {
    const execGetConnectionData = async (quiddityID, path) => {
      return api.get(quiddityID, path)
    }

    it('should emit `connection_specs_get`', async () => {
      mockEmitSocketCallback(null, null)
      await execGetConnectionData(QUIDDITY_TEST)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'connection_specs_get',
        QUIDDITY_TEST,
        expect.any(Function)
      )
    })

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitSocketCallback(null, CONNECTION_DATA_TEST)
      expect(await execGetConnectionData()).toEqual(CONNECTION_DATA_TEST)
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitSocketCallback(CONNECTION_DATA_ERROR)
      await expect(execGetConnectionData()).rejects.toEqual(CONNECTION_DATA_ERROR)
    })

    it('should resolve the request when the callback contains nothing', async () => {
      mockEmitSocketCallback(null, null)
      await expect(execGetConnectionData()).resolves.toStrictEqual({})
    })
  })
})
