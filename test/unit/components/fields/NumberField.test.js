/* global jest describe beforeEach it expect */

import React from 'react'
import { shallow } from 'enzyme'

import Property from '@models/quiddity/Property'
import NumberField from '@components/fields/NumberField'

import UnitEnum, { toNumber, fromNumber } from '@models/common/UnitEnum'
import { Inputs } from '@sat-mtl/ui-components'

const { Slider, InputNumber, Field } = Inputs

describe('<NumberField />', () => {
  const jsonProperty = {
    id: 'Encoder/bitrate',
    type: 'unsigned int',
    value: 5000000,
    label: 'Desired bitrate',
    writable: true,
    min: 1000000,
    max: 20000000,
    order: 40,
    parent: '',
    enabled: true,
    unit: UnitEnum.KILOBIT_PER_SECOND
  }

  const property = Property.fromJSON(jsonProperty)

  const bitProperty = Property.fromJSON({
    ...jsonProperty,
    unit: UnitEnum.BIT_PER_SECOND
  })

  const describedProperty = Property.fromJSON({
    ...jsonProperty,
    description: 'Value of the desired average bitrate (Kbps)'
  })

  let onChangeMock

  beforeEach(() => {
    onChangeMock = jest.fn()
  })

  function shallowField (property) {
    return shallow(
      <NumberField
        {...property.toFieldProps()}
        onChange={onChangeMock}
      />
    )
  }

  describe('<Silder />', () => {
    it('should render a slider', () => {
      const $field = shallowField(bitProperty)
      expect($field.find(Slider).length).toEqual(1)
    })

    it('should render a slider with converted max attribute', () => {
      const $slider = shallowField(bitProperty).find(Slider)
      const convertedMax = toNumber(UnitEnum.BIT_PER_SECOND, jsonProperty.max)
      expect($slider.prop('max')).toEqual(convertedMax)
    })

    it('should render a slider with converted min attribute', () => {
      const $slider = shallowField(bitProperty).find(Slider)
      const convertedMin = toNumber(UnitEnum.BIT_PER_SECOND, jsonProperty.min)
      expect($slider.prop('min')).toEqual(convertedMin)
    })

    it('should handle and convert the user changes on the slider', () => {
      const $slider = shallowField(bitProperty).find(Slider)
      $slider.prop('onChange')(1)
      expect(onChangeMock).toHaveBeenCalledWith(fromNumber(UnitEnum.BIT_PER_SECOND, 1))
    })
  })

  describe('<InputNumber />', () => {
    it('should render a number input', () => {
      const $field = shallowField(bitProperty)
      expect($field.find(InputNumber).length).toEqual(1)
    })

    it('should render a slider with converted max attribute', () => {
      const $input = shallowField(bitProperty).find(InputNumber)
      const convertedMax = toNumber(UnitEnum.BIT_PER_SECOND, jsonProperty.max)
      expect($input.prop('max')).toEqual(convertedMax)
    })

    it('should render a slider with converted min attribute', () => {
      const $input = shallowField(bitProperty).find(InputNumber)
      const convertedMin = toNumber(UnitEnum.BIT_PER_SECOND, jsonProperty.min)
      expect($input.prop('min')).toEqual(convertedMin)
    })

    it('should handle and convert the user changes on the input number', () => {
      const $input = shallowField(bitProperty).find(InputNumber)
      $input.prop('onChange')(1)
      expect(onChangeMock).toHaveBeenCalledWith(fromNumber(UnitEnum.BIT_PER_SECOND, 1))
    })
  })

  describe('<Field />', () => {
    it('should render the Field with the given description', () => {
      const $field = shallowField(describedProperty).find(Field)
      expect($field.prop('description')).toEqual(describedProperty.description)
    })

    it('should render a default description if it is not given', () => {
      const $field = shallowField(property).find(Field)
      expect($field.prop('description')).not.toBeFalsy()
    })
  })
})
