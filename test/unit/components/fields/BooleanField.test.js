/* global jest describe beforeEach it expect */

import React from 'react'
import { shallow } from 'enzyme'

import Property from '@models/quiddity/Property'
import BooleanField from '@components/fields/BooleanField'

import { Inputs } from '@sat-mtl/ui-components'
import videoOutput from '@fixture/models/quiddity.videoOutput.json'

const { Field, Switch } = Inputs

describe('<BooleanField />', () => {
  const jsonProperty = videoOutput.infoTree.property.find(json => json.id === 'Window/fullscreen')
  const property = Property.fromJSON(jsonProperty)

  const undescribedProperty = Property.fromJSON({
    ...jsonProperty,
    description: undefined
  })

  let onChangeMock

  beforeEach(() => {
    onChangeMock = jest.fn()
  })

  function shallowField (property) {
    return shallow(
      <BooleanField
        {...property.toFieldProps()}
        onChange={onChangeMock}
      />
    )
  }

  describe('<Switch />', () => {
    it('should be checked with the field value', () => {
      const $switch = shallowField(property).find(Switch)
      expect($switch.prop('checked')).toEqual(jsonProperty.value)
    })

    it('should handle each user changes on the switch', () => {
      const $switch = shallowField(property).find(Switch)
      $switch.prop('onChange')(!jsonProperty.value)
      expect(onChangeMock).toHaveBeenCalledWith(!jsonProperty.value)
    })
  })

  describe('<Field />', () => {
    it('should render the Field with the given description', () => {
      const $field = shallowField(property).find(Field)
      expect($field.prop('description')).toEqual(property.description)
    })

    it('should render a default description if it is not given', () => {
      const $field = shallowField(undescribedProperty).find(Field)
      expect($field.prop('description')).not.toBeFalsy()
    })
  })
})
