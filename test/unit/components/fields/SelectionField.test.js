/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'

import { AppStoresContext } from '@components/App'
import populateStores from '@stores/populateStores.js'
import SelectionField from '@components/fields/SelectionField'
import { Inputs } from '@sat-mtl/ui-components'
import { act } from 'react-dom/test-utils'

import '@utils/i18n'

const { Select } = Inputs

const options = [{
  label: 'Item 1',
  value: 'item1'
}, {
  label: 'Item 2',
  value: 'item2'
}, {
  label: 'Item 3',
  value: 'item3'
}, {
  label: 'Item 4',
  value: 'item4'
}]

describe('<SelectionField />', () => {
  let helpMessageStore

  beforeEach(() => {
    ({ helpMessageStore } = populateStores())
  })

  function renderSelectionField (props) {
    return mount(
      <AppStoresContext.Provider value={{ helpMessageStore }}>
        <SelectionField
          {...props}
        />
      </AppStoresContext.Provider>
    )
  }
  it('should render the SelectionField component', () => {
    const $field = renderSelectionField({ options: options, option: options[0] })
    expect($field).not.toBeNull()
  })
  it('should display the default selected element on initialization', () => {
    const $field = renderSelectionField({ options: options, option: options[0] })
    expect($field).not.toBeNull()
    expect($field.find(Select).prop('selected')).toEqual({ label: 'Item 1', value: 'item1' })
  })
  it('should display the first element if it is selected', () => {
    const onSelectionMock = jest.fn()
    const onChangeMock = jest.fn()
    const $field = renderSelectionField({ options: options, onSelection: onSelectionMock, onChange: onChangeMock, option: options[0] })
    expect($field.find(Select).length).toEqual(1)
    act(() => $field.find(Select).prop('onSelection')('Item 1'))
    expect($field.update().find(Select).prop('selected')).toEqual({ label: 'Item 1', value: 'item1' })
  })
})
