/* global describe beforeEach it expect */
import React from 'react'
import { mount } from 'enzyme'
import { AppStoresContext } from '@components/App'
import { DisplayThumbnailsField, DisplayEncodersField } from '@components/fields/SettingFields'
import populateStores from '@stores/populateStores.js'

import { Inputs } from '@sat-mtl/ui-components'

const { Switch } = Inputs

describe('SettingFields', () => {
  let settingStore
  let stores

  beforeEach(() => {
    stores = populateStores()

    ;({
      settingStore
    } = stores)
  })

  function mountDisplayThumbnailsField () {
    return mount(
      <AppStoresContext.Provider value={stores}>
        <DisplayThumbnailsField />
      </AppStoresContext.Provider>
    )
  }

  function mountDisplayEncodersField () {
    return mount(
      <AppStoresContext.Provider value={stores}>
        <DisplayEncodersField />
      </AppStoresContext.Provider>
    )
  }

  describe('<DisplayThumbnailsField />', () => {
    it('should set the thumbnailsDisplay property to true if the switch is on', () => {
      const $switch = mountDisplayThumbnailsField().find(Switch)
      $switch.prop('onChange')(true)
      expect(settingStore.displayThumbnails).toEqual(true)
    })

    it('should set the thumbnailsDisplay property to false if the switch is off', () => {
      const $switch = mountDisplayThumbnailsField().find(Switch)
      $switch.prop('onChange')(false)
      expect(settingStore.displayThumbnails).toEqual(false)
    })
  })

  describe('<DisplayEncodersField/>', () => {
    it('should set the displayEncoders property to true if the switch in on', () => {
      const $switch = mountDisplayEncodersField().find(Switch)
      $switch.prop('onChange')(true)
      expect(settingStore.displayEncoders).toEqual(true)
    })

    it('should set the displayEncoders property to false if the switch in off', () => {
      const $switch = mountDisplayEncodersField().find(Switch)
      $switch.prop('onChange')(false)
      expect(settingStore.displayEncoders).toEqual(false)
    })
  })
})
