/* global jest describe beforeEach it expect */

import React, { useState as useStateMock } from 'react'
import { shallow } from 'enzyme'
import { HuePicker, AlphaPicker } from 'react-color'

import Property from '@models/quiddity/Property'
import ColorField, { ColorHuePicker, ColorAlphaPicker, ColorTextInput } from '@components/fields/ColorField'

import { Inputs } from '@sat-mtl/ui-components'
import videoOutput from '@fixture/models/quiddity.videoOutput.json'

const { Field, InputText } = Inputs

jest.mock('react', () => ({
  ...jest.requireActual('react'),
  useState: jest.fn()
}))

describe('<ColorField />', () => {
  const jsonProperty = videoOutput.infoTree.property.find(json => json.id === 'Window/overlay_color')
  const property = Property.fromJSON(jsonProperty)

  const undescribedProperty = Property.fromJSON({
    ...jsonProperty,
    description: undefined
  })

  let onChangeMock, onEditMock

  beforeEach(() => {
    onEditMock = jest.fn()
    onChangeMock = jest.fn()
    useStateMock.mockImplementation(init => [init, onEditMock])
  })

  function shallowField (property) {
    return shallow(
      <ColorField
        {...property.toFieldProps()}
        onChange={onChangeMock}
      />
    )
  }

  describe('<ColorTextInput />', () => {
    function shallowColorTextInput (property) {
      return shallow(
        <ColorTextInput
          {...property.toFieldProps()}
          onChange={onChangeMock}
        />
      )
    }

    it('should be checked with the field value', () => {
      const $input = shallowColorTextInput(property).find(InputText)
      expect($input.prop('value')).toEqual(jsonProperty.value)
    })

    it('should handle each user changes on the switch in the component hook', () => {
      const $input = shallowColorTextInput(property).find(InputText)
      $input.prop('onChange')({ target: { value: 'test' } })
      expect(onEditMock).toHaveBeenCalledWith('test')
    })

    it('should send the edited value on blur', () => {
      useStateMock.mockImplementation(() => ['test', onEditMock])
      const $input = shallowColorTextInput(property).find(InputText)
      $input.simulate('blur')
      expect(onChangeMock).toHaveBeenCalledWith('test')
    })

    it('should send the edited value when the user press the key "Enter"', () => {
      useStateMock.mockImplementation(() => ['test', onEditMock])
      const $input = shallowColorTextInput(property).find(InputText)
      $input.prop('onPressEnter')()
      expect(onChangeMock).toHaveBeenCalledWith('test')
    })

    it('shouldn\'t send the edited value on blur if the value wasn\'t changed', () => {
      useStateMock.mockImplementation(() => [jsonProperty.value, onEditMock])
      const $input = shallowColorTextInput(property).find(InputText)
      $input.simulate('blur')
      expect(onChangeMock).not.toHaveBeenCalled()
    })

    it('shouldn\'t send the edited value when the user press "Enter" if the value wasn\'t changed', () => {
      useStateMock.mockImplementation(() => [jsonProperty.value, onEditMock])
      const $input = shallowColorTextInput(property).find(InputText)
      $input.prop('onPressEnter')()
      expect(onChangeMock).not.toHaveBeenCalled()
    })
  })

  describe('<ColorHuePicker />', () => {
    function shallowColorHuePicker (property) {
      return shallow(
        <ColorHuePicker
          {...property.toFieldProps()}
          onChange={onChangeMock}
        />
      )
    }

    it('should return the hexadecimal code from the HuePicker', () => {
      const $picker = shallowColorHuePicker(property).find(HuePicker)
      $picker.prop('onChange')({ rgb: { r: 255, g: 0, b: 0 } })
      expect(onChangeMock).toHaveBeenCalledWith('ff0000ff')
    })

    it('should fallback the alpha value if it is equal to 1', () => {
      const $picker = shallowColorHuePicker(property).find(HuePicker)
      $picker.prop('onChange')({ rgb: { r: 255, g: 255, b: 255, a: 1 } })
      expect(onChangeMock).toHaveBeenCalledWith(property.value.toLowerCase())
    })
  })

  describe('<ColorAlphaPicker />', () => {
    function shallowColorAlphaPicker (property) {
      return shallow(
        <ColorAlphaPicker
          {...property.toFieldProps()}
          onChange={onChangeMock}
        />
      )
    }

    it('should return the hexadecimal code from the HuePicker', () => {
      const $picker = shallowColorAlphaPicker(property).find(AlphaPicker)
      $picker.prop('onChange')({ rgb: { r: 255, g: 0, b: 0 } })
      expect(onChangeMock).toHaveBeenCalledWith('ff0000ff')
    })
  })

  describe('<Field />', () => {
    it('should contain all sub components', () => {
      const $field = shallowField(property)
      expect($field.find(ColorTextInput).length).toEqual(1)
      expect($field.find(ColorHuePicker).length).toEqual(1)
      expect($field.find(ColorAlphaPicker).length).toEqual(1)
    })

    it('should render the Field with the given description', () => {
      const $field = shallowField(property).find(Field)
      expect($field.prop('description')).toEqual(property.description)
    })

    it('should render a default description if it is not given', () => {
      const $field = shallowField(undescribedProperty).find(Field)
      expect($field.prop('description')).not.toBeFalsy()
    })
  })
})
