/* global describe it expect beforeEach */

import React from 'react'
import { mount } from 'enzyme'

import { AppStoresContext } from '@components/App'
import populateStores from '@stores/populateStores.js'

import { Shared, Common } from '@sat-mtl/ui-components'
import ShmdataBox from '@components/matrix/ShmdataBox'

import Shmdata from '@models/shmdata/Shmdata'
import MediaTypeEnum from '@models/shmdata/MediaTypeEnum'

const { Icon } = Common
const { ShmdataCell } = Shared

const SHMDATA_NAME = 'shmdata_test'
const THUMBNAIL_URL = 'thumbnail.com'

describe('<ShmdataBox />', () => {
  let socketStore, configStore, quiddityStore, shmdataStore, thumbnailStore, previewStore, capsStore, statStore

  beforeEach(() => {
    ({
      socketStore,
      configStore,
      quiddityStore,
      shmdataStore,
      thumbnailStore,
      previewStore,
      capsStore,
      statStore
    } = populateStores())
  })

  const mountShmdata = ({ shmdata } = {}) => {
    return mount(
      <AppStoresContext.Provider value={{
        previewStore,
        thumbnailStore,
        socketStore,
        configStore,
        quiddityStore,
        shmdataStore,
        capsStore,
        statStore
      }}
      >
        <ShmdataBox
          shmdata={shmdata}
        />
      </AppStoresContext.Provider>
    )
  }

  function shmdataFromMediaType (mediaType = '') {
    return new Shmdata(`${SHMDATA_NAME}`, mediaType, 'test')
  }

  describe('render', () => {
    it('should render a ShmdataCell with disabled state', () => {
      const $source = mountShmdata()
      const $cell = $source.find(ShmdataCell)
      expect($cell.length).toEqual(1)
      expect($cell.prop('disabled')).toEqual(true)
      expect($cell.prop('src')).toEqual(null)
    })

    it('should render a ShmdataCell with an Icon if a shmdata is given', () => {
      const shmdata = shmdataFromMediaType(MediaTypeEnum.VIDEO_RAW)
      const $source = mountShmdata({ shmdata })
      const $cell = $source.find(ShmdataCell)
      expect($cell.find(Icon).length).toEqual(1)
    })

    it('should render a ShmdataCell with an URL if a thumbnail is listening', () => {
      const shmdata = shmdataFromMediaType(MediaTypeEnum.VIDEO_RAW)
      thumbnailStore.thumbnailUrls.set(shmdata.id, THUMBNAIL_URL)

      const $cell = mountShmdata({ shmdata }).find(ShmdataCell)
      expect($cell.length).toEqual(1)
      expect($cell.prop('disabled')).toEqual(false)
      expect($cell.prop('src')).toEqual(THUMBNAIL_URL)
    })
  })
})
