/* global describe it expect */
import React from 'react'
import { mount } from 'enzyme'

import HelpPage from '@components/pages/HelpPage'
import NpmPackage from '~/package.json'

import '@utils/i18n'

describe('<HelpPage />', () => {
  function mountHelpPage () {
    return mount(
      <HelpPage />
    )
  }

  describe('<VersionSubSection />', () => {
    it('should render the current Scenic version', () => {
      expect(mountHelpPage().find('#ScenicVersion').text()).toMatch(new RegExp(NpmPackage.version))
    })

    it('should render the commit hash of the build', () => {
      expect(mountHelpPage().find('#ScenicVersion').text()).toMatch(new RegExp(process.env.GIT_SHORT_HASH))
    })
  })
})
