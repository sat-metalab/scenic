/* global describe it expect */

import React from 'react'
import { mount } from 'enzyme'

import { BuddyStatusEnum, SendStatusEnum, RecvStatusEnum } from '@models/sip/SipEnums'
import SipContactInformationTooltip, { ReceivingStatus, SendingStatus } from '@components/tooltips/SipContactInformationTooltip'

describe('<SipContactInformationTooltip />', () => {
  function mountSipContactInformationTooltip (buddyStatus, sendStatus, recvStatus) {
    return mount(
      <SipContactInformationTooltip
        buddyStatus={buddyStatus}
        sendStatus={sendStatus}
        recvStatus={recvStatus}
      />
    )
  }

  describe('render', () => {
    it('should not render the sending and the receiving statuses by default', () => {
      const $wrapper = mountSipContactInformationTooltip(BuddyStatusEnum.OFFLINE)

      expect($wrapper.find(ReceivingStatus).length).toEqual(0)
      expect($wrapper.find(SendingStatus).length).toEqual(0)
    })

    it('should render the sending status if it is given', () => {
      const $wrapper = mountSipContactInformationTooltip(BuddyStatusEnum.OFFLINE, SendStatusEnum.CONNECTING)
      expect($wrapper.find(ReceivingStatus).length).toEqual(0)
      expect($wrapper.find(SendingStatus).length).toEqual(1)
    })

    it('should render the receiving status if it is given', () => {
      const $wrapper = mountSipContactInformationTooltip(BuddyStatusEnum.OFFLINE, undefined, RecvStatusEnum.CONNECTING)
      expect($wrapper.find(ReceivingStatus).length).toEqual(1)
      expect($wrapper.find(SendingStatus).length).toEqual(0)
    })
  })
})
