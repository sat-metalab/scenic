/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'

import { AppStoresContext } from '@components/App'
import populateStores from '@stores/populateStores.js'

import { SwitcherConnectionButton, PortInput, UrlInput } from '@components/modals/SwitcherConnectionModal'
import { Common, Inputs } from '@sat-mtl/ui-components'

const { Button } = Common
const { InputText } = Inputs

describe('<SwitcherConnectionModal />', () => {
  let socketStore, modalStore

  beforeEach(() => {
    ({
      socketStore,
      modalStore
    } = populateStores())

    modalStore.setActiveModal = jest.fn()
    modalStore.cleanActiveModal = jest.fn()
  })

  const mountSwitcherConnectionButton = () => {
    return mount(
      <AppStoresContext.Provider value={{ socketStore }}>
        <SwitcherConnectionButton />
      </AppStoresContext.Provider>
    )
  }

  const mountPortInput = () => {
    return mount(
      <AppStoresContext.Provider value={{ socketStore }}>
        <PortInput />
      </AppStoresContext.Provider>
    )
  }

  const mountUrlInput = () => {
    return mount(
      <AppStoresContext.Provider value={{ socketStore }}>
        <UrlInput />
      </AppStoresContext.Provider>
    )
  }

  describe('<SwitcherConnectionButton />', () => {
    beforeEach(() => {
      socketStore.applyConnection = jest.fn()
    })

    it('should have the socketStore\'s url and key values', () => {
      const $wrapper = mountSwitcherConnectionButton()
      const $button = $wrapper.find(Button)
      $button.simulate('click')
      expect(socketStore.applyConnection).toHaveBeenCalled()
    })

    it('should be disabled when one or both input fields are empty', () => {
      socketStore.setHost('')
      socketStore.setPort('port')
      const $button = mountSwitcherConnectionButton().find(Button)
      expect($button.prop('disabled')).toEqual(true)
    })

    it('should not be disabled when both input fields are not empty', () => {
      socketStore.setHost('host')
      socketStore.setPort('port')
      const $button = mountSwitcherConnectionButton().find(Button)
      expect($button.prop('disabled')).toEqual(false)
    })
  })

  describe('<PortInput />', () => {
    beforeEach(() => {
      socketStore.applyConnection = jest.fn()
      socketStore.setRemote('localhost', '8080')
    })

    it('should register the entered value on change and set the port to that changed value', () => {
      const $wrapper = mountPortInput()
      const $input = $wrapper.find(InputText)
      $input.simulate('change')
      expect($input.prop('value')).not.toEqual('')
    })

    it('should try to apply a switcher connection when clicked', () => {
      const $wrapper = mountPortInput()
      const $input = $wrapper.find(InputText)
      $input.prop('onPressEnter')()
      expect(socketStore.applyConnection).toHaveBeenCalled()
    })
  })

  describe('<UrlInput />', () => {
    beforeEach(() => {
      socketStore.applyConnection = jest.fn()
      socketStore.setRemote('localhost', '8080')
    })

    it('should register the entered value on change and set the url to that changed value', () => {
      const $wrapper = mountUrlInput()
      const $input = $wrapper.find(InputText)
      expect($input.prop('value')).not.toEqual('')
    })

    it('should try to apply a switcher connection when clicked', () => {
      const $wrapper = mountUrlInput()
      const $input = $wrapper.find(InputText)
      $input.prop('onPressEnter')()
      expect(socketStore.applyConnection).toHaveBeenCalled()
    })
  })
})
