/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'

import { AppStoresContext } from '@components/App'
import populateStores from '@stores/populateStores.js'

import { CancelButton } from '@components/common/Buttons'
import { Common, Feedback } from '@sat-mtl/ui-components'

import '@utils/i18n'
import { RenameSceneModal, RENAME_SCENE_MODAL_ID, RenameSceneCancelButton, RenameSceneButton, DeleteSceneCancelButton, DeleteSceneButton, DELETE_SCENE_MODAL_ID, DeleteSceneModal } from '@components/modals/SceneModals'

const { Button } = Common
const { Modal } = Feedback

describe('<SceneModals />', () => {
  let modalStore
  let confirmMock, cancelMock

  beforeEach(() => {
    ({ modalStore } = populateStores())

    modalStore.setActiveModal = jest.fn()
    modalStore.cleanActiveModal = jest.fn()
    confirmMock = jest.fn()
    cancelMock = jest.fn()
  })

  function mountRenameSceneModal () {
    return mount(
      <AppStoresContext.Provider value={{
        modalStore
      }}
      >
        <RenameSceneModal
          visible
          onSceneRename={confirmMock}
          onCancel={cancelMock}
        />
      </AppStoresContext.Provider>
    )
  }

  function mountRenameSceneCancelButton () {
    return mount(
      <RenameSceneCancelButton
        onCancel={cancelMock}
      />
    )
  }

  function mountRenameSceneButton () {
    return mount(
      <RenameSceneButton
        onSceneRename={confirmMock}
      />
    )
  }

  function mountDeleteSceneModal () {
    return mount(
      <AppStoresContext.Provider value={{
        modalStore
      }}
      >
        <DeleteSceneModal
          visible
          onSceneDelete={confirmMock}
          onCancel={cancelMock}
        />
      </AppStoresContext.Provider>
    )
  }

  function mountDeleteSceneCancelButton () {
    return mount(
      <AppStoresContext.Provider value={{
        modalStore
      }}
      >
        <DeleteSceneCancelButton
          onCancel={cancelMock}
        />
      </AppStoresContext.Provider>
    )
  }

  function mountDeleteSceneButton () {
    return mount(
      <DeleteSceneButton
        onSceneDelete={confirmMock}
      />
    )
  }

  describe('<RenameSceneModal />', () => {
    it('should be registered to modalStore', () => {
      mountRenameSceneModal()
      modalStore.modals.has(RENAME_SCENE_MODAL_ID)
    })

    it('should not be visible if rename is not requested', () => {
      expect(mountRenameSceneModal().html()).toEqual('')
    })

    it('should set the visible prop to true when an open rename scene modal is requested', () => {
      const $modal = mountRenameSceneModal()
      modalStore.setActiveModal(RENAME_SCENE_MODAL_ID)
      expect($modal.exists()).toBe(true)
      expect($modal.prop('visible')).toEqual(true)
    })

    it('should not be visible when it is not active', () => {
      const $wrapper = mountRenameSceneModal()
      modalStore.cleanActiveModal(RENAME_SCENE_MODAL_ID)
      const $modal = $wrapper.find(Modal)
      expect($modal.prop('visible')).toEqual(false)
    })
  })

  describe('<RenameSceneCancelButton />', () => {
    it('should cancel the modal when the cancel button is clicked', () => {
      const $button = mountRenameSceneCancelButton().find(CancelButton)
      $button.simulate('click')
      expect(cancelMock).toHaveBeenCalled()
    })
  })

  describe('<RenameSceneButton />', () => {
    it('should trigger the onSceneRename function when clicked is clicked', () => {
      const $button = mountRenameSceneButton().find(Button)
      $button.simulate('click')
      expect(confirmMock).toHaveBeenCalled()
    })
  })

  describe('<DeleteSceneModal />', () => {
    it('should set the visible prop to true when an open session modal is requested', () => {
      const $modal = mountDeleteSceneModal()
      modalStore.setActiveModal(DELETE_SCENE_MODAL_ID)
      expect($modal.exists()).toBe(true)
      expect($modal.prop('visible')).toEqual(true)
    })
  })

  describe('<DeleteSceneCancelButton />', () => {
    it('should cancel the modal when the cancel button is clicked', () => {
      const $button = mountDeleteSceneCancelButton().find(CancelButton)
      $button.simulate('click')
      expect(modalStore.cleanActiveModal).toHaveBeenCalled()
    })
  })

  describe('<DeleteSceneButton />', () => {
    it('should trigger the onSceneDelete function when clicked', () => {
      const $button = mountDeleteSceneButton().find(Button)
      $button.simulate('click')
      expect(confirmMock).toHaveBeenCalled()
    })
  })
})
