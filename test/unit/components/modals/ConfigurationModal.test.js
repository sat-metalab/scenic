/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'

import { AppStoresContext } from '@components/App'
import populateStores from '@stores/populateStores.js'

import ConfigurationModal, { CONFIGURATION_MODAL_ID, ReloadButton } from '@components/modals/ConfigurationModal'
import { Feedback } from '@sat-mtl/ui-components'

const { Modal } = Feedback

describe('<ConfigurationModal />', () => {
  let socketStore, modalStore

  beforeEach(() => {
    ({
      socketStore,
      modalStore
    } = populateStores())
  })

  const mountConfigurationModal = () => {
    return mount(
      <AppStoresContext.Provider value={{
        modalStore,
        socketStore
      }}
      >
        <ConfigurationModal visible />
      </AppStoresContext.Provider>
    )
  }

  const mountReloadButton = () => {
    return mount(
      <AppStoresContext.Provider value={{
        socketStore
      }}
      >
        <ReloadButton />
      </AppStoresContext.Provider>
    )
  }

  describe('ConfigurationModal', () => {
    it('should register the modal when it is mounting', () => {
      mountConfigurationModal()
      expect(modalStore.modals.has(CONFIGURATION_MODAL_ID)).toEqual(true)
    })

    it('should apply the socket  connection when the reload button is clicked', () => {
      socketStore.applyConnection = jest.fn()
      const $reload = mountReloadButton()
      const $button = $reload.find('ReloadButton')
      $button.simulate('click')
      expect(socketStore.applyConnection).toHaveBeenCalled()
    })

    it('should be visible when it is active', () => {
      const $wrapper = mountConfigurationModal()
      modalStore.setActiveModal(CONFIGURATION_MODAL_ID)
      expect($wrapper.exists()).toBe(true)
      expect($wrapper.prop('visible')).toEqual(true)
    })

    it('should not be visible when it is not active', () => {
      const $wrapper = mountConfigurationModal()
      modalStore.cleanActiveModal(CONFIGURATION_MODAL_ID)
      const $modal = $wrapper.find(Modal)
      expect($modal.prop('visible')).toEqual(false)
    })
  })
})
