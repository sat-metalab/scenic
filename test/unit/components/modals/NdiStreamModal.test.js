/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'

import populateStores from '@stores/populateStores.js'
import { AppStoresContext } from '@components/App'
import NdiStreamModal, { NDI_STREAM_MODAL_ID, NdiCancelButton, NdiConfirmButton, NdiStreamSelect } from '@components/modals/NdiStreamModal'
import NdiStream from '@models/quiddity/NdiStream'

import { CancelButton, ConfirmButton } from '@components/common/Buttons'
import { Inputs, Common, Feedback } from '@sat-mtl/ui-components'

const { Modal } = Feedback

const { Spinner } = Common
const { Select } = Inputs

describe('<NdiStreamModal />', () => {
  const NDI_OUTPUT_TEST = 'TEST (test1)'
  const NDI_OUTPUT_ALT = 'TEST (test2)'

  const NDI2SHMDATA_SIGNAL = {
    stdout: `
      ${NDI_OUTPUT_TEST}
      ${NDI_OUTPUT_ALT}
    `
  }

  const NDI_STREAMS = [
    NdiStream.fromNdiEntry(NDI_OUTPUT_TEST),
    NdiStream.fromNdiEntry(NDI_OUTPUT_ALT)
  ]

  let ndiStore, modalStore

  beforeEach(() => {
    ({ ndiStore, modalStore } = populateStores())
  })

  const mountNdiStreamModal = () => {
    return mount(
      <AppStoresContext.Provider value={{
        modalStore,
        ndiStore
      }}
      >
        <NdiStreamModal visible />
      </AppStoresContext.Provider>
    )
  }

  const mountNdiStreamSelect = () => {
    return mount(
      <AppStoresContext.Provider value={{
        ndiStore,
        modalStore
      }}
      >
        <NdiStreamSelect />
      </AppStoresContext.Provider>
    )
  }

  const mountNdiCancelButton = () => {
    return mount(
      <AppStoresContext.Provider value={{
        ndiStore
      }}
      >
        <NdiCancelButton />
      </AppStoresContext.Provider>
    )
  }

  const mountNdiConfirmButton = () => {
    return mount(
      <AppStoresContext.Provider value={{
        ndiStore
      }}
      >
        <NdiConfirmButton />
      </AppStoresContext.Provider>
    )
  }

  it('should be registered to modalStore', () => {
    mountNdiStreamModal()
    modalStore.modals.has(NDI_STREAM_MODAL_ID)
  })

  it('should not be visible if streams are not requested', () => {
    expect(mountNdiStreamModal().html()).toEqual('')
  })

  it('should set the visible prop to true when the ndi stream modal is requested', () => {
    const $modal = mountNdiStreamModal()
    modalStore.setActiveModal(NDI_STREAM_MODAL_ID)
    expect($modal.exists()).toBe(true)
    expect($modal.prop('visible')).toEqual(true)
  })

  it('should not be visible when it is not active', () => {
    const $wrapper = mountNdiStreamModal()
    modalStore.cleanActiveModal(NDI_STREAM_MODAL_ID)
    const $modal = $wrapper.find(Modal)
    expect($modal.prop('visible')).toEqual(false)
  })

  it('should apply the NDIInput quiddity creation when the confirmation button is clicked', () => {
    ndiStore.applyNdiInputQuiddityCreation = jest.fn()
    ndiStore.handleSnifferOutputUpdate(NDI2SHMDATA_SIGNAL.stdout)
    ndiStore.setNdiInputRequestFlag(true)
    ndiStore.setNdiStreamSelection(NDI_STREAMS[0].toOption())
    const $confirm = mountNdiConfirmButton()
    const $button = $confirm.find(ConfirmButton)
    $button.simulate('click')
    expect(ndiStore.applyNdiInputQuiddityCreation).toHaveBeenCalled()
  })

  it('should set the ndi input request to false when the user clicks on the cancel Button', () => {
    ndiStore.handleSnifferOutputUpdate('')
    ndiStore.setNdiInputRequestFlag = jest.fn()
    const $cancel = mountNdiCancelButton()
    const $button = $cancel.find(CancelButton)
    $button.simulate('click')
    expect(ndiStore.setNdiInputRequestFlag).toHaveBeenCalledWith(false)
  })

  describe('NdiStreamSelect', () => {
    it('should return false if the selected NdiStream exists', () => {
      ndiStore.setNdiStreams(NDI_STREAMS)
      ndiStore.setNdiStreamSelection(NDI_STREAMS[0].toOption())
      const $modal = mountNdiStreamSelect().find(Select)
      expect($modal.prop('hasError')).toEqual(false)
    })

    it('should return true if the selected NdiStream doesn\'t exist', () => {
      ndiStore.setNdiStreamSelection(NDI_STREAMS[0].toOption())
      const $modal = mountNdiStreamSelect().find(Select)
      expect($modal.prop('hasError')).toEqual(true)
    })
  })

  describe('renderModalContent', () => {
    beforeEach(() => {
      ndiStore.handleSnifferOutputUpdate(NDI2SHMDATA_SIGNAL.stdout)
      ndiStore.setNdiInputRequestFlag(true)
    })

    it('should render a Spinner if no stream is provided', () => {
      ndiStore.isNetworkScanned = false
      const $modal = mountNdiStreamModal()
      expect($modal.find(Spinner)).toHaveLength(1)
      expect($modal.find(Select)).toHaveLength(0)
    })

    it('should render a specific message when no stream is available', () => {
      ndiStore.setNdiStreams([])
      const $modal = mountNdiStreamModal()
      expect($modal.find(Spinner)).toHaveLength(0)
      expect($modal.find(Select)).toHaveLength(0)
    })

    it('should render a Select component when streams are available', () => {
      const $modal = mountNdiStreamModal()
      expect($modal.find(Spinner)).toHaveLength(0)
      expect($modal.find(Select)).toHaveLength(1)
    })
  })
})
