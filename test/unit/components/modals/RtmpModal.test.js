/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'

import { AppStoresContext } from '@components/App'
import populateStores from '@stores/populateStores'

import RtmpModal, { UrlInput, RtmpConfirmButton, RtmpCancelButton, RtmpFooter, StreamKeyInput, RTMP_MODAL_ID } from '@components/modals/RtmpModal'

import { CancelButton, ConfirmButton } from '@components/common/Buttons'
import { Inputs, Feedback } from '@sat-mtl/ui-components'

const { Modal } = Feedback
const { InputText, InputPassword } = Inputs

describe('<RtmpModal />', () => {
  let rtmpStore, modalStore

  beforeEach(() => {
    ({
      modalStore,
      rtmpStore
    } = populateStores())
  })

  const mountRtmpModal = () => {
    return mount(
      <AppStoresContext.Provider value={{ rtmpStore, modalStore }}>
        <RtmpModal visible />
      </AppStoresContext.Provider>
    )
  }

  const mountUrlInput = () => {
    return mount(
      <AppStoresContext.Provider value={{ rtmpStore }}>
        <UrlInput />
      </AppStoresContext.Provider>
    )
  }

  const mountStreamKeyInput = () => {
    return mount(
      <AppStoresContext.Provider value={{ rtmpStore }}>
        <StreamKeyInput />
      </AppStoresContext.Provider>
    )
  }

  const mountRtmpCancelButton = () => {
    return mount(
      <AppStoresContext.Provider value={{ rtmpStore }}>
        <RtmpCancelButton />
      </AppStoresContext.Provider>
    )
  }

  const mountRtmpConfirmButton = () => {
    return mount(
      <AppStoresContext.Provider value={{ rtmpStore }}>
        <RtmpConfirmButton />
      </AppStoresContext.Provider>
    )
  }

  const mountRtmpFooter = () => {
    return mount(
      <AppStoresContext.Provider value={{ rtmpStore }}>
        <RtmpFooter />
      </AppStoresContext.Provider>
    )
  }

  it('should be visible when it is active', () => {
    const $wrapper = mountRtmpModal()
    modalStore.setActiveModal(RTMP_MODAL_ID)
    expect($wrapper.exists()).toBe(true)
    expect($wrapper.prop('visible')).toEqual(true)
  })

  it('should not be visible when it is not active', () => {
    const $wrapper = mountRtmpModal()
    modalStore.cleanActiveModal(RTMP_MODAL_ID)
    const $modal = $wrapper.find(Modal)
    expect($modal.prop('visible')).toEqual(false)
  })

  describe('UrlInput', () => {
    it('should have empty url in the text input', () => {
      const $wrapper = mountUrlInput()
      const $input = $wrapper.find(InputText)
      expect($input.prop('value')).toEqual('')
    })

    it('should edit the url on change', () => {
      const $wrapper = mountUrlInput()
      const $input = $wrapper.find(InputText)
      $input.simulate('change')
      expect($input.prop('value')).not.toEqual(' ')
    })
  })

  describe('RtmpCancelButton', () => {
    it('should clean the modal when the cancel button is clicked', () => {
      rtmpStore.cleanRtmpCredentials = jest.fn()
      rtmpStore.setRtmpRequestFlag = jest.fn()
      const $button = mountRtmpCancelButton().find(CancelButton)
      $button.simulate('click')
      expect(rtmpStore.cleanRtmpCredentials).toHaveBeenCalled()
      expect(rtmpStore.setRtmpRequestFlag).toHaveBeenCalledWith(false)
    })
  })

  describe('RtmpConfirmButton', () => {
    it('should create the RTMP quiddity with the provided RTMP settings', () => {
      rtmpStore.setUrl('url')
      rtmpStore.setKey('key')
      rtmpStore.applyRtmpQuiddityCreation = jest.fn().mockResolvedValue(true)
      rtmpStore.cleanRtmpCredentials = jest.fn()
      rtmpStore.setRtmpRequestFlag = jest.fn()
      const $button = mountRtmpConfirmButton().find(ConfirmButton)
      $button.simulate('click')
      expect(rtmpStore.cleanRtmpCredentials).toHaveBeenCalled()
      expect(rtmpStore.setRtmpRequestFlag).toHaveBeenCalledWith(false)
      expect(rtmpStore.applyRtmpQuiddityCreation).toHaveBeenCalled()
    })

    it('should be disabled when one or both input fields are empty', () => {
      const $button = mountRtmpConfirmButton().find(ConfirmButton)
      rtmpStore.setUrl('')
      rtmpStore.setKey('key')
      expect($button.prop('disabled')).toEqual(true)
    })

    it('should not be disabled when both input fields are not empty', () => {
      rtmpStore.setUrl('url')
      rtmpStore.setKey('key')
      const $button = mountRtmpConfirmButton().find(ConfirmButton)
      expect($button.prop('disabled')).toEqual(false)
    })
  })

  describe('UrlField', () => {
    it('should render an InputText', () => {
      const $modal = mountUrlInput()
      expect($modal.find(InputText)).toHaveLength(1)
    })
  })

  describe('StreamKeyField', () => {
    it('should render an InputPassword', () => {
      const $modal = mountStreamKeyInput()
      expect($modal.find(InputPassword)).toHaveLength(1)
    })
  })

  describe('renderModalFooter', () => {
    it('should render a create and cancel Button', () => {
      const $footer = mountRtmpFooter()
      expect($footer.find(ConfirmButton)).toHaveLength(1)
      expect($footer.find(CancelButton)).toHaveLength(1)
    })
  })
})
