/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'
import { act } from 'react-dom/test-utils'
import populateStores from '@stores/populateStores.js'

import { AppStoresContext } from '@components/App'

import FileBridge from '@models/common/FileBridge'
import FileEntry from '@components/entries/FileEntry'

import FileDeletionDrawer, { FILE_DELETION_DRAWER_ID } from '@components/drawers/FileDeletionDrawer'
import { TrashCurrentSessionModal, TRASH_CURRENT_SESSION_MODAL_ID } from '@components/modals/SessionModals'
import { Layout } from '@sat-mtl/ui-components'

import '@utils/i18n'

const { Backdrop } = Layout

describe('<FileDeletionDrawer />', () => {
  let drawerStore, sessionStore, modalStore, helpMessageStore

  beforeEach(() => {
    ({
      drawerStore,
      sessionStore,
      modalStore,
      helpMessageStore
    } = populateStores())

    sessionStore.updateSessionList = jest.fn()
  })

  function mountDrawer () {
    return mount(
      <AppStoresContext.Provider value={{ sessionStore, modalStore, drawerStore, helpMessageStore }}>
        <FileDeletionDrawer />
      </AppStoresContext.Provider>
    )
  }

  describe('useEffect', () => {
    let addDrawerSpy, removeDrawerSpy
    let addModalSpy, removeModalSpy
    let clearSessionTrashSpy

    beforeEach(() => {
      addDrawerSpy = jest.spyOn(drawerStore, 'addDrawer')
      removeDrawerSpy = jest.spyOn(drawerStore, 'removeDrawer')

      addModalSpy = jest.spyOn(modalStore, 'addModal')
      removeModalSpy = jest.spyOn(modalStore, 'removeModal')

      clearSessionTrashSpy = jest.spyOn(sessionStore, 'clearSessionTrash')
    })

    it('should register the FileDeletionDrawer and the TrashCurrentSessionModal when it is mounted', () => {
      mountDrawer()
      expect(addDrawerSpy).toHaveBeenCalledWith(FILE_DELETION_DRAWER_ID)
      expect(addModalSpy).toHaveBeenCalledWith(TRASH_CURRENT_SESSION_MODAL_ID)
    })

    it('should unregister the FileDeletionDrawer and the TrashCurrentSessionModal when it is unmounted', () => {
      mountDrawer().unmount()
      expect(removeDrawerSpy).toHaveBeenCalledWith(FILE_DELETION_DRAWER_ID)
      expect(removeModalSpy).toHaveBeenCalledWith(TRASH_CURRENT_SESSION_MODAL_ID)
    })

    it('should reset the file trash when it is rendered', () => {
      mountDrawer()
      expect(clearSessionTrashSpy).toHaveBeenCalled()
    })
  })

  describe('<Backdrop/>', () => {
    let clearActiveDrawerSpy

    beforeEach(() => {
      clearActiveDrawerSpy = jest.spyOn(drawerStore, 'clearActiveDrawer')
    })

    it('should clean the drawers when the backdrop is clicked', () => {
      mountDrawer().find(Backdrop).simulate('click')
      expect(clearActiveDrawerSpy).toHaveBeenCalled()
    })
  })

  describe('<FileDeletionList/>', () => {
    const FILE_ID = 'test.json'
    let addSessionToTrashSpy, removeSessionFromTrashSpy, setActiveModalSpy

    beforeEach(() => {
      addSessionToTrashSpy = jest.spyOn(sessionStore, 'addSessionToTrash')
      removeSessionFromTrashSpy = jest.spyOn(sessionStore, 'removeSessionFromTrash')
      setActiveModalSpy = jest.spyOn(modalStore, 'setActiveModal')

      sessionStore.addSession(new FileBridge(FILE_ID))
      expect(sessionStore.sessions.size).toEqual(1)
    })

    it('should add a file on the trash when selected if it isn\'t the current session file', () => {
      mountDrawer().find(FileEntry).simulate('click')
      expect(addSessionToTrashSpy).toHaveBeenCalledWith(FILE_ID)
    })

    it('should delete a file from the trash when selected if was already in the trash', () => {
      const $entry = mountDrawer().find(FileEntry)
      act(() => sessionStore.addSessionToTrash(FILE_ID))
      $entry.simulate('click')
      expect(removeSessionFromTrashSpy).toHaveBeenCalledWith(FILE_ID)
    })

    it('should open the TrashCurrentSessionModal if the file represents the current session', () => {
      const $entry = mountDrawer().find(FileEntry)
      act(() => sessionStore.setCurrentSession(FILE_ID))
      $entry.simulate('click')
      expect(setActiveModalSpy).toHaveBeenCalledWith(TRASH_CURRENT_SESSION_MODAL_ID)
    })
  })

  describe('<TrashCurrentSessionModal />', () => {
    const FILE_ID = 'test.json'
    let addSessionToTrashSpy, cleanActiveModalSpy

    beforeEach(() => {
      addSessionToTrashSpy = jest.spyOn(sessionStore, 'addSessionToTrash')
      cleanActiveModalSpy = jest.spyOn(modalStore, 'cleanActiveModal')

      sessionStore.addSession(new FileBridge(FILE_ID))
      sessionStore.setCurrentSession(FILE_ID)
    })

    it('should trash the current session\'s file if the modal is confirmed', () => {
      const $drawer = mountDrawer()
      act(() => $drawer.find(TrashCurrentSessionModal).prop('onConfirm')())
      expect(addSessionToTrashSpy).toHaveBeenCalledWith(FILE_ID)
    })

    it('should close the modal if it is canceled', () => {
      const $drawer = mountDrawer()
      act(() => $drawer.find(TrashCurrentSessionModal).prop('onCancel')())
      expect(cleanActiveModalSpy).toHaveBeenCalledWith(TRASH_CURRENT_SESSION_MODAL_ID)
    })
  })
})
