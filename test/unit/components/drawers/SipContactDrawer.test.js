/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'

import { AppStoresContext } from '@components/App'
import populateStores from '@stores/populateStores.js'
import SipCredentials from '@models/sip/SipCredentials'
import Contact from '@models/sip/Contact'

import { Common } from '@sat-mtl/ui-components'

import SipContactDrawer, {
  SipContactHeader,
  SipContact,
  SessionPartner,
  SessionContact,
  CallPartnerButton,
  HangupPartnerButton,
  RemovePartnerButton,
  AddPartnerButton,
  SessionPartnerSection,
  SessionContactSection,
  HangupAllButton,
  SectionRefContext
} from '@components/drawers/SipContactDrawer'

import {
  SendStatusEnum,
  RecvStatusEnum,
  BuddyStatusEnum
} from '@models/sip/SipEnums'

import RowEntry from '@components/entries/RowEntry'

const { Button } = Common

const sipUser = 'test'
const sipServer = 'sip.dev'
const sipCredentials = new SipCredentials(sipUser, sipServer)

const buddyUser = 'buddy'
const buddyUri = `${buddyUser}@${sipServer}`

const contact = Contact.fromJSON({
  id: '0',
  uri: buddyUri
})

const calledContact = Contact.fromJSON({
  id: '1',
  uri: buddyUri,
  sendStatus: SendStatusEnum.CALLING
})

const connectedContact = Contact.fromJSON({
  id: '1',
  uri: buddyUri,
  connections: ['test']
})

const contacts = [contact, calledContact]

describe('<SipContactDrawer />', () => {
  let drawerStore
  let sipStore
  let contactStore
  let contactStatusStore
  let helpMessageStore
  let modalStore

  beforeEach(() => {
    ({
      sipStore,
      contactStore,
      contactStatusStore,
      drawerStore,
      helpMessageStore,
      modalStore
    } = populateStores())
  })

  function mountSipContactDrawer () {
    return mount(
      <AppStoresContext.Provider value={{
        sipStore,
        contactStore,
        contactStatusStore,
        drawerStore,
        helpMessageStore,
        modalStore
      }}
      >
        <SipContactDrawer />
      </AppStoresContext.Provider>

    )
  }

  describe('<SipContact />', () => {
    function mountSipContact (contact) {
      return mount(
        <AppStoresContext.Provider value={populateStores()}>
          <SipContact contact={contact} />
        </AppStoresContext.Provider>
      )
    }

    it('should render a RowHead with the buddy data', () => {
      const $contact = mountSipContact(contact)
      expect($contact.find(RowEntry).length).toEqual(1)
      expect($contact.find('.EntryTitle').text()).toEqual(contact.name)
      expect($contact.find('.EntrySubtitle').text()).toEqual(contact.uri)
    })
  })

  describe('<SipContactHeader />', () => {
    function mountSipContactHeader (credentials) {
      return mount(
        <SipContactHeader
          credentials={credentials}
        />
      )
    }

    it('should render a default SIP user name if it is not defined', () => {
      const $header = mountSipContactHeader()
      expect($header.find(RowEntry).length).toEqual(1)
      expect($header.find('.EntryTitle').text()).toEqual('Unknown')
    })

    it('should render the SIP user name', () => {
      const $header = mountSipContactHeader(sipCredentials)
      expect($header.find('.EntryTitle').text()).toEqual(sipCredentials.sipUser)
    })
  })

  describe('<HangupAllButton>', () => {
    function renderButton () {
      return mount(
        <AppStoresContext.Provider value={{ contactStore }}>
          <HangupAllButton />
        </AppStoresContext.Provider>
      )
    }

    beforeEach(() => {
      contactStore.applyHangupAllCalls = jest.fn()
    })

    it('should hangup all contacts when it is clicked', () => {
      renderButton().simulate('click')
      expect(contactStore.applyHangupAllCalls).toHaveBeenCalled()
    })
  })

  describe('<CallPartnerButton />', () => {
    beforeEach(() => {
      contactStore.applyCall = jest.fn()
    })

    function mountCallPartnerButton (contact) {
      return mount(
        <AppStoresContext.Provider value={{ contactStore, helpMessageStore }}>
          <CallPartnerButton contact={contact} />
        </AppStoresContext.Provider>
      )
    }

    it('should call the partner when the button is clicked', () => {
      const $button = mountCallPartnerButton(contact)
      $button.find(Button).simulate('click')
      expect(contactStore.applyCall).toHaveBeenCalledWith(contact.uri)
    })
  })

  describe('<HangupPartnerButton />', () => {
    beforeEach(() => {
      contactStore.applyHangup = jest.fn()
    })

    function mountHangupPartnerButton (contact) {
      return mount(
        <AppStoresContext.Provider value={{ contactStore, helpMessageStore }}>
          <HangupPartnerButton contact={contact} />
        </AppStoresContext.Provider>
      )
    }

    it('should hangup the partner when the contact is in call', () => {
      const $button = mountHangupPartnerButton(contact)
      $button.find(Button).simulate('click')
      expect(contactStore.applyHangup).toHaveBeenCalledWith(contact.uri)
    })
  })

  describe('<RemovePartnerButton />', () => {
    beforeEach(() => {
      contactStore.removeContactFromSession = jest.fn()
    })

    function mountRemovePartnerButton (contact) {
      return mount(
        <AppStoresContext.Provider value={{ contactStore, helpMessageStore }}>
          <RemovePartnerButton contact={contact} />
        </AppStoresContext.Provider>
      )
    }

    it('should remove the partner when the button is clicked', () => {
      const $button = mountRemovePartnerButton(contact)
      $button.find(Button).simulate('click')
      expect(contactStore.removeContactFromSession).toHaveBeenCalledWith(contact.id)
    })
  })

  describe('<AddPartnerButton />', () => {
    let partnerRefFake
    beforeEach(() => {
      contactStore.addContactToSession = jest.fn()
      partnerRefFake = {
        current: {
          scrollIntoView: jest.fn()
        }
      }
    })

    function mountAddPartnerButton (contact) {
      return mount(
        <AppStoresContext.Provider value={{ contactStore, helpMessageStore }}>
          <SectionRefContext.Provider value={{ partnerRef: partnerRefFake }}>
            <AddPartnerButton contact={contact} />
          </SectionRefContext.Provider>
        </AppStoresContext.Provider>
      )
    }

    it('should add the partner when the button is clicked', () => {
      const $button = mountAddPartnerButton(contact)
      $button.find(Button).simulate('click')
      expect(contactStore.addContactToSession).toHaveBeenCalledWith(contact)
    })

    it('should call the scrollIntoView function when a contact is added', () => {
      const $button = mountAddPartnerButton(contact)
      $button.find(Button).simulate('click')
      expect(partnerRefFake.current.scrollIntoView).toHaveBeenCalled()
    })
  })

  describe('<SessionPartner />', () => {
    beforeEach(() => {
      contactStore.addContact(contact)
      contactStatusStore.setSendingStatus(contact.id, SendStatusEnum.CONNECTING)
      contactStatusStore.setReceivingStatus(contact.id, RecvStatusEnum.CONNECTING)
      contactStatusStore.setBuddyStatus(contact.id, BuddyStatusEnum.ONLINE)

      contactStore.addContact(connectedContact)
      contactStatusStore.setSendingStatus(connectedContact.id, SendStatusEnum.CONNECTING)
      contactStatusStore.setReceivingStatus(connectedContact.id, RecvStatusEnum.CONNECTING)
      contactStatusStore.setBuddyStatus(connectedContact.id, BuddyStatusEnum.ONLINE)
    })

    function mountAddPartnerButton (contact) {
      return mount(
        <AppStoresContext.Provider value={{ contactStatusStore, helpMessageStore }}>
          <SessionPartner contact={contact} />
        </AppStoresContext.Provider>
      )
    }

    it('should render a partner with a remove button when it has no connection', () => {
      const $partner = mountAddPartnerButton(contact)
      expect($partner.find(CallPartnerButton).length).toEqual(0)
      expect($partner.find(HangupPartnerButton).length).toEqual(0)
      expect($partner.find(RemovePartnerButton).length).toEqual(1)
    })

    it('should render a partner with the call button when it is not called', () => {
      const $partner = mountAddPartnerButton(connectedContact)
      expect($partner.find(CallPartnerButton).length).toEqual(1)
      expect($partner.find(HangupPartnerButton).length).toEqual(0)
      expect($partner.find(RemovePartnerButton).length).toEqual(0)
    })

    it('should render a partner with the hangup button when it is called', () => {
      contactStatusStore.setSendingStatus(contact.id, SendStatusEnum.CALLING)
      const $partner = mountAddPartnerButton(contact)
      expect($partner.find(CallPartnerButton).length).toEqual(0)
      expect($partner.find(HangupPartnerButton).length).toEqual(1)
      expect($partner.find(RemovePartnerButton).length).toEqual(0)
    })
  })

  describe('<SessionPartnerSection />', () => {
    it('should render a default message when there is no partner', () => {
      const $section = mountSipContactDrawer().find(SessionPartnerSection)
      expect($section.find('aside').text())
        .toEqual('Click on a \'+\' button to add a contact to the session')
    })

    it('should render all partners', () => {
      contacts.forEach(c => {
        contactStore.addContact(c)
        contactStore.addContactToSession(c)
      })

      const $section = mountSipContactDrawer().find(SessionPartnerSection)
      expect($section.find(SessionPartner).length).toEqual(contacts.length)
    })
  })

  describe('<SessionContact />', () => {
    beforeEach(() => {
      contactStore.addContact(calledContact)
      contactStore.addContactToSession(calledContact)

      contactStatusStore.setSendingStatus(contact.id, SendStatusEnum.CONNECTING)
      contactStatusStore.setReceivingStatus(contact.id, RecvStatusEnum.CONNECTING)
      contactStatusStore.setBuddyStatus(contact.id, BuddyStatusEnum.ONLINE)

      contactStatusStore.setSendingStatus(calledContact.id, SendStatusEnum.CONNECTING)
      contactStatusStore.setReceivingStatus(calledContact.id, RecvStatusEnum.CONNECTING)
      contactStatusStore.setBuddyStatus(calledContact.id, BuddyStatusEnum.ONLINE)
    })

    function mountSessionContact (contact) {
      return mount(
        <AppStoresContext.Provider value={{ contactStore, contactStatusStore, helpMessageStore }}>
          <SessionContact contact={contact} />
        </AppStoresContext.Provider>
      )
    }

    it('should render a contact with the add button when it is not a partner', () => {
      const $contact = mountSessionContact(contact)
      expect($contact.find(RemovePartnerButton).length).toEqual(0)
      expect($contact.find(AddPartnerButton).length).toEqual(1)
    })

    it('should render a contact with the remove button when it is a partner', () => {
      const $contact = mountSessionContact(calledContact)
      expect($contact.find(RemovePartnerButton).length).toEqual(1)
      expect($contact.find(AddPartnerButton).length).toEqual(0)
    })
  })

  describe('<SessionContactSection />', () => {
    it('should render a default message when there is no partner', () => {
      const $section = mountSipContactDrawer().find(SessionContactSection)
      expect($section.find('aside').text()).toEqual('Contact list is empty')
    })

    it('should render all contacts', () => {
      contacts.forEach(c => {
        contactStore.addContact(c)
      })

      const $section = mountSipContactDrawer().find(SessionContactSection)
      expect($section.find(SessionContact).length).toEqual(2)
    })

    it('should not render partners', () => {
      contacts.forEach(c => {
        contactStore.addContact(c)
        contactStore.addContactToSession(c)
      })

      const $section = mountSipContactDrawer().find(SessionContactSection)
      expect($section.find(SessionContact).length).toEqual(0)
    })
  })
})
