/* global describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'

import { AppStoresContext } from '@components/App'
import populateStores from '@stores/populateStores'
import { Socket } from '@fixture/socket'

import Scene from '@models/userTree/Scene'
import SceneTabBar from '@components/bars/SceneTabBar'

import { Common, Inputs } from '@sat-mtl/ui-components'
const { Checkbox } = Inputs
const { NavTab } = Common
const { Tab } = NavTab

describe('<SceneTabBar />', () => {
  let socketStore, sceneStore, modalStore
  const SCENE_ACTIVE = new Scene('test', undefined, true)
  const TEST_SCENE = new Scene('test', undefined, false)

  beforeEach(() => {
    ({ socketStore, sceneStore, modalStore } = populateStores())
    socketStore.setActiveSocket(new Socket())
  })

  const mountSceneTabBar = () => {
    return mount(
      <AppStoresContext.Provider value={{ socketStore, sceneStore, modalStore }}>
        <SceneTabBar />
      </AppStoresContext.Provider>
    )
  }

  it('should always render a default scene Tab', () => {
    const $bar = mountSceneTabBar()
    expect($bar.find(Tab).length).toEqual(1)
  })

  it('should render a Tab for every added scene', () => {
    sceneStore.addUserScene(SCENE_ACTIVE)
    sceneStore.addUserScene(TEST_SCENE)
    const $bar = mountSceneTabBar()
    expect($bar.find(Tab).length).toEqual(2)
  })

  it('should not render an active checkbox when there is no active scene', () => {
    sceneStore.removeUserScene(SCENE_ACTIVE)
    const $bar = mountSceneTabBar()
    expect($bar.find(Checkbox).length).toEqual(0)
  })

  it('should not render an active checkbox when there is no active scene', () => {
    sceneStore.addUserScene(SCENE_ACTIVE)
    const $bar = mountSceneTabBar()
    expect($bar.find(Checkbox).length).toEqual(1)
  })
})
