/* global describe it expect */

import { isObject, arrayToObject, stripEmptyArrays, getFromSwitcherTreePath, getMutatedFromSwitcherTreePath } from '@utils/objectTools'

describe('objectTools', () => {
  describe('isObject', () => {
    it('should return true if the tested value is an Object', () => {
      expect(isObject({})).toEqual(true)
      expect(isObject({ test1: 1 })).toEqual(true)
      expect(isObject({ test2: 'a string' })).toEqual(true)
      expect(isObject({ test3: { test: 'nested', array: [1, 2, 3] } })).toEqual(true)
    })

    it('should return false if the tested value is not an Object', () => {
      expect(isObject(null)).toEqual(false)
      expect(isObject(undefined)).toEqual(false)
      expect(isObject(1234)).toEqual(false)
      expect(isObject('a string')).toEqual(false)
      expect(isObject(false)).toEqual(false)
      expect(isObject(['an array', 1])).toEqual(false)
    })
  })

  describe('arrayToObject', () => {
    it('should return an array when an object is passed', () => {
      expect(arrayToObject([{ id: 'test1', firstName: 'test2', lastName: 'test3' }], 'id')).toEqual({ test1: { id: 'test1', firstName: 'test2', lastName: 'test3' } })
    })
  })

  describe('stripEmptyArrays', () => {
    it('should remove all key/value pairs that have an empty array for value', () => {
      expect(stripEmptyArrays({ id: 'test1', firstName: 'test2', jobs: ['teacher', 'nurse'] })).toEqual({ id: 'test1', firstName: 'test2', jobs: ['teacher', 'nurse'] })
      expect(stripEmptyArrays({ id: 'test1', firstName: 'test2', jobs: [] })).toEqual({ id: 'test1', firstName: 'test2' })
      expect(stripEmptyArrays({ id: [], firstName: [], jobs: [] })).toEqual({})
    })
  })

  describe('getFromSwitcherTreePath', () => {
    it('should get the value at the specified path', () => {
      const testObject = {
        artists: [
          {
            name: 'Harmonium',
            albums: ['Harmonium', 'Si on avait besoin d\'une cinquième saison', 'L\'Heptade']
          },
          {
            name: 'Sleeping People',
            albums: ['Sleeping People', 'Growing', 'Notruf']
          }
        ]
      }
      expect(getFromSwitcherTreePath(testObject, 'artists.0.albums.2')).toEqual('L\'Heptade')
      expect(getFromSwitcherTreePath(testObject, 'artists.1')).toEqual(
        {
          name: 'Sleeping People',
          albums: ['Sleeping People', 'Growing', 'Notruf']
        }
      )
    })
  })

  describe('getMutatedFromSwitcherTreePath', () => {
    it('should set the value at the specified path creating missing objects and arrays as it goes', () => {
      let testObject = {}
      const upsilonAcrux = {
        name: 'Upsilon Acrux',
        albums: [
          'In the Acrux of the Upsilon King',
          'The Last Pirates of Upsilon',
          'Last Train Out',
          'Volucris Avis Dirae-Arum',
          'Galapagos Momentum',
          'Radian Futura'
        ]
      }
      testObject = getMutatedFromSwitcherTreePath(testObject, 'artists.0', upsilonAcrux)
      expect(testObject).toEqual({
        artists: [
          {
            name: 'Upsilon Acrux',
            albums: [
              'In the Acrux of the Upsilon King',
              'The Last Pirates of Upsilon',
              'Last Train Out',
              'Volucris Avis Dirae-Arum',
              'Galapagos Momentum',
              'Radian Futura'
            ]
          }
        ]
      })
      testObject = getMutatedFromSwitcherTreePath(testObject, 'artists.0.albums.6', 'Sun Square Dialect')
      expect(testObject.artists[0].albums[6]).toEqual('Sun Square Dialect')
    })
    it('should not mutate the original object', () => {
      const testObject = {}
      getMutatedFromSwitcherTreePath(testObject, 'test.test.test', '4')
      expect(testObject).toEqual({})
    })
  })
})
