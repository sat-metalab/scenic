/* global describe it expect */

import Property from '@models/quiddity/Property'
import rotation from '@fixture/property.monitor.rotation.json'

describe('Property', () => {
  const PROP_ID = 'test'
  const PROP_TYPE = 'test'
  const PROP_VALUE = 'test'

  describe('constructor', () => {
    /* eslint-disable no-new */
    it('should fail if required parameters are not well-typed', () => {
      expect(() => { new Property() }).toThrow()
      expect(() => { new Property(PROP_ID) }).toThrow()
      expect(() => { new Property(PROP_ID, PROP_TYPE, PROP_VALUE, 0) }).toThrow()
      expect(() => { new Property(PROP_ID, PROP_TYPE, PROP_VALUE, undefined, 0) }).toThrow()
      expect(() => { new Property(PROP_ID, PROP_TYPE, PROP_VALUE, undefined, undefined, 0) }).toThrow()
      expect(() => { new Property(PROP_ID, PROP_TYPE, PROP_VALUE, undefined, undefined, undefined, 0) }).toThrow()
      expect(() => { new Property(PROP_ID, PROP_TYPE, PROP_VALUE, undefined, undefined, undefined, undefined, 'fail') }).toThrow()
    })
    /* eslint-enable no-new */
  })

  describe('fromJSON', () => {
    it('should fail if JSON input is not well-formed', () => {
      expect(() => { Property.fromJSON() }).toThrow()
      expect(() => { Property.fromJSON({}) }).toThrow()
      expect(() => { Property.fromJSON({ 1: 1 }) }).toThrow()

      expect(() => {
        Property.fromJSON({ type: 'failure' })
      }).toThrow()
    })

    it('should create property from property definition', () => {
      expect(Property.fromJSON(rotation)).toBeDefined()
    })
  })

  describe('hasDoublePrecision', () => {
    it('should have a double precision if it is a bitrate', () => {
      expect(Property.fromJSON({ id: 'Generator_bitrate', type: 'int' }).hasDoublePrecision()).toEqual(true)
    })

    it('should have a double precision if it is a float', () => {
      expect(Property.fromJSON({ id: 'test', type: 'double' }).hasDoublePrecision()).toEqual(true)
    })

    it('should have a double precision if it is a double', () => {
      expect(Property.fromJSON({ id: 'test', type: 'float' }).hasDoublePrecision()).toEqual(true)
    })

    it('should have a step of 0.01 if it has a double precision', () => {
      expect(Property.fromJSON({ id: 'test', type: 'float' }).step).toEqual(0.01)
    })
  })

  describe('isFrequency', () => {
    it('should detect if a property is a frequency', () => {
      expect(Property.fromJSON({ id: 'Generator_bitrate', type: 'double' }).isFrequency()).toEqual(false)
      expect(Property.fromJSON({ id: 'frequency', type: 'double' }).isFrequency()).toEqual(false)
      expect(Property.fromJSON({ id: 'Generator_frequency', type: 'double' }).isFrequency()).toEqual(true)
    })

    it('should set a frequency step to 1', () => {
      expect(Property.fromJSON({ id: 'Generator_frequency', type: 'double' }).step).toEqual(1)
    })
  })

  describe('isMsDelay', () => {
    it('should detect if a property is a time delay expressed in milliseconds', () => {
      expect(Property.fromJSON({ id: 'Generator_bitrate', type: 'double' }).isMsDelay()).toEqual(false)
      expect(Property.fromJSON({ id: 'time_delay', type: 'double' }).isMsDelay()).toEqual(false)
      expect(Property.fromJSON({ id: 'shmdelay_time_delay', type: 'double' }).isMsDelay()).toEqual(true)
    })

    it('should set a millisecond delay step to 1', () => {
      expect(Property.fromJSON({ id: 'shmdelay_time_delay', type: 'double' }).step).toEqual(1)
    })
  })
})
