/* global describe it expect beforeEach */

import Quiddity from '@models/quiddity/Quiddity'
import descriptions from '@fixture/description.quiddities.json'

describe('Quiddity', () => {
  const QUID_ID = 'test'
  const QUID_KIND = 'test'
  const QUID_NICKNAME = 'nickname0'

  let defaultQuid

  beforeEach(() => {
    defaultQuid = new Quiddity(QUID_ID, QUID_NICKNAME, QUID_KIND)
  })

  describe('constructor', () => {
    /* eslint-disable no-new */
    it('should fail if required parameters are not well-typed', () => {
      expect(() => { new Quiddity() }).toThrow()
      expect(() => { new Quiddity(0) }).toThrow()

      expect(() => { new Quiddity(QUID_ID) }).toThrow()
      expect(() => { new Quiddity(QUID_ID, 0) }).toThrow()

      expect(() => { new Quiddity(QUID_ID, QUID_NICKNAME, QUID_KIND, 0) }).toThrow()
      expect(() => { new Quiddity(QUID_ID, QUID_NICKNAME, QUID_KIND, undefined, 0) }).toThrow()
      expect(() => { new Quiddity(QUID_ID, QUID_NICKNAME, QUID_KIND, undefined, undefined, 0) }).toThrow()
      expect(() => { new Quiddity(QUID_ID, QUID_NICKNAME, QUID_KIND, undefined, undefined, undefined, 0) }).toThrow()
      expect(() => { new Quiddity(QUID_ID, QUID_NICKNAME, QUID_KIND, undefined, undefined, undefined, undefined, 0) }).toThrow()
    })
    /* eslint-enable no-new */

    it('should set default quiddity name to its nickname', () => {
      expect(defaultQuid.nickname).toEqual('nickname0')
    })

    it('should set a different name than the ID if specified', () => {
      const quidWithName = new Quiddity(QUID_ID, QUID_NICKNAME, QUID_KIND)
      expect(quidWithName.nickname).not.toEqual(quidWithName.id)
      expect(quidWithName.nickname).toEqual(QUID_NICKNAME)
    })
  })

  describe('isPrivate', () => {
    it('should not hide the quiddity if it is not hidden and not protected', () => {
      expect(defaultQuid.isPrivate).toEqual(false)
    })

    it('should hide the quiddity if it is hidden', () => {
      defaultQuid.isHidden = true
      expect(defaultQuid.isPrivate).toEqual(true)
    })

    it('should hide the quiddity if it is protected', () => {
      defaultQuid.isProtected = true
      expect(defaultQuid.isPrivate).toEqual(true)
    })
  })

  describe('fromJSON', () => {
    it('should fail if JSON input is not well-formed', () => {
      expect(() => { Quiddity.fromJSON() }).toThrow()
      expect(() => { Quiddity.fromJSON({}) }).toThrow()
      expect(() => { Quiddity.fromJSON({ 1: 1 }) }).toThrow()

      expect(() => {
        Quiddity.fromJSON({ kind: 'failure' })
      }).toThrow()
    })

    it('should create quiddities from quiddity definitions', () => {
      const quiddities = descriptions.quiddities

      for (const json of quiddities) {
        expect(Quiddity.fromJSON(json)).toBeDefined()
      }
    })
  })
})
