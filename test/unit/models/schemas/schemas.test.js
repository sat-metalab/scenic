/* global describe it expect beforeEach */

import Ajv from 'ajv'

import connectionSchema from '@models/schemas/connection.schema.json'
import contactListSchema from '@models/schemas/contactList.schema.json'
import contactSchema from '@models/schemas/contact.schema.json'
import initQuidditySchema from '@models/schemas/initQuiddity.schema.json'
import menuItemSchema from '@models/schemas/menuItem.schema.json'
import menusSchema from '@models/schemas/menus.schema.json'
import sceneSchema from '@models/schemas/scene.schema.json'
import scenicSchema from '@models/schemas/scenic.schema.json'
import subMenuSchema from '@models/schemas/subMenu.schema.json'
import userTreeSchema from '@models/schemas/userTree.schema.json'

import connection0 from '@fixture/connection0.sample.json'
import contactList0 from '@fixture/contactList0.sample.json'
import contact0 from '@fixture/contact0.sample.json'
import initQuiddity0 from '@fixture/initQuiddity0.sample.json'
import scene0 from '@fixture/scene0.sample.json'
import scenic0 from '@fixture/scenic0.sample.json'
import menuItem0 from '@fixture/menuItem0.sample.json'
import subMenu0 from '@fixture/subMenu0.sample.json'
import menus0 from '@fixture/menus0.sample.json'

const userTree0 = {
  version: 'rev1',
  scenes: { scene0: scene0 },
  connections: { connection0: connection0 }
}

describe('schemas', () => {
  let ajv

  beforeEach(() => {
    ajv = new Ajv()
      .addSchema(sceneSchema, 'scene')
      .addSchema(connectionSchema, 'connection')
      .addSchema(userTreeSchema, 'userTree')
      .addSchema(contactSchema, 'contact')
      .addSchema(contactListSchema, 'contactList')
      .addSchema(initQuidditySchema, 'initQuiddity')
      .addSchema(menuItemSchema, 'menuItem')
      .addSchema(subMenuSchema, 'subMenu')
      .addSchema(menusSchema, 'menus')
      .addSchema(scenicSchema, 'scenic')
  })

  describe('validation', () => {
    it('should fail with empty data', () => {
      expect(ajv.validate('scene', {})).toEqual(false)
    })

    it('should validate a valid Scene', () => {
      expect(ajv.validate('scene', scene0)).toEqual(true)
    })

    it('should validate a valid Connection', () => {
      expect(ajv.validate('connection', connection0)).toEqual(true)
    })

    it('should validate a valid userTree', () => {
      expect(ajv.validate('userTree', userTree0)).toEqual(true)
    })

    it('should validate a valid contact', () => {
      expect(ajv.validate('contact', contact0)).toEqual(true)
    })

    it('should validate a valid contactList', () => {
      expect(ajv.validate('contactList', contactList0)).toEqual(true)
    })

    it('should validate a valid initQuiddity config', () => {
      expect(ajv.validate('initQuiddity', initQuiddity0)).toEqual(true)
    })

    it('should validate a valid scenic config', () => {
      expect(ajv.validate('scenic', scenic0)).toEqual(true)
    })

    it('should validate a valid menu item', () => {
      expect(ajv.validate('menuItem', menuItem0)).toEqual(true)
    })

    it('should validate a valid submenu', () => {
      expect(ajv.validate('subMenu', subMenu0)).toEqual(true)
    })

    it('should validate a valid menu configuration', () => {
      expect(ajv.validate('menus', menus0)).toEqual(true)
    })
  })
})
