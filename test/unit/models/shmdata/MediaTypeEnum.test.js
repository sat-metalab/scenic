/* global describe it expect */

import MediaTypeEnum, { getShmdataIconType } from '@models/shmdata/MediaTypeEnum'

describe('<MediaTypeEnum />', () => {
  describe('getShmdataIconType', () => {
    it('should return a data icon type by default', () => {
      expect(getShmdataIconType()).toEqual('data')
    })

    it('should return a video icon type if the shmdata is a video', () => {
      expect(getShmdataIconType(MediaTypeEnum.VIDEO_RAW)).toEqual('video')
    })

    it('should return a video icon type if the shmdata is encoded video', () => {
      expect(getShmdataIconType(MediaTypeEnum.VIDEO_H264)).toEqual('video')
    })

    it('should return an audio icon type if shmdata is audio', () => {
      expect(getShmdataIconType(MediaTypeEnum.AUDIO_RAW)).toEqual('audio')
    })
  })
})
