/* global describe it expect */

import Stat from '@models/shmdata/Stat'

describe('Stat', () => {
  const STAT_RATE = 1
  const STAT_BYTE_RATE = 1

  describe('constructor', () => {
    it('should define a shmdata Stat', () => {
      const stat = new Stat(STAT_BYTE_RATE, STAT_RATE)
      expect(stat).toBeDefined()
    })

    /* eslint-disable no-new */
    it('should fail if required parameters are not well-typed', () => {
      expect(() => { new Stat() }).toThrow()
      expect(() => { new Stat(STAT_BYTE_RATE) }).toThrow()
      expect(() => { new Stat(undefined, STAT_RATE) }).toThrow()
    })
    /* eslint-enable no-new */
  })

  describe('bitRate', () => {
    const STAT = new Stat(STAT_BYTE_RATE, STAT_RATE)

    it('Should convert 1 Byte to 8 Bit', () => {
      expect(STAT.bitRate).toEqual(8)
    })
  })

  describe('mbpsRate', () => {
    const STAT = new Stat(STAT_BYTE_RATE, STAT_RATE)

    it('Should convert 1 Byte to 8 Bit', () => {
      expect(STAT.mbpsRate).toEqual(0.000008)
    })
  })

  describe('isActive', () => {
    it('should not be active with a negative or null byte rate', () => {
      const negative = new Stat(-1, STAT_RATE)
      const zero = new Stat(0, STAT_RATE)

      expect(negative.isActive).toEqual(false)
      expect(zero.isActive).toEqual(false)
    })

    it('should be active with a positive byte rate', () => {
      const positive = new Stat(1, STAT_RATE)
      expect(positive.isActive).toEqual(true)
    })
  })

  describe('fromJSON', () => {
    it('should fail if JSON input is not well-formed', () => {
      expect(() => { Stat.fromJSON() }).toThrow()
      expect(() => { Stat.fromJSON({}) }).toThrow()
      expect(() => { Stat.fromJSON({ 1: 1 }) }).toThrow()

      expect(() => {
        Stat.fromJSON({ rate: 'failure' })
      }).toThrow()
    })

    it('should create stat from well-defined JSON', () => {
      expect(Stat.fromJSON({ byteRate: STAT_BYTE_RATE, rate: STAT_RATE })).toBeDefined()
      expect(Stat.fromJSON({ byte_rate: STAT_BYTE_RATE, rate: STAT_RATE })).toBeDefined()
    })
  })
})
