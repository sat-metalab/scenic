/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'
import { configure } from 'mobx'

import populateStores from '@stores/populateStores.js'
import InitStateEnum from '@models/common/InitStateEnum'
import { InitializationError, RequirementError } from '@utils/errors'

import { sdiInput, jacksrc } from '@fixture/allQuiddities'

import Shmdata from '@models/shmdata/Shmdata'
import MatrixEntry from '@models/matrix/MatrixEntry'
import Quiddity from '@models/quiddity/Quiddity'

import PreviewStore, { LOG, PREVIEW_NICKNAME, PREVIEW_KIND_ID } from '@stores/timelapse/PreviewStore'

configure({ safeDescriptors: false })

describe('PreviewStore', () => {
  let socket, socketStore, configStore, quiddityStore, shmdataStore, previewStore
  let propertyAPI, infoTreeAPI

  const WRITER_ID = 'test'
  const WRITER_PATH = '/tmp/' + WRITER_ID
  const WRITER_BRANCH = '.shmdata.writer.' + WRITER_PATH
  const WRITER_SHMDATA = new Shmdata(WRITER_PATH, 'caps', 'video')
  const VIDEO_MATRIX_ENTRY = new MatrixEntry(sdiInput, null, [WRITER_SHMDATA])

  const AUDIO_MATRIX_ENTRY = new MatrixEntry(jacksrc, null, [new Shmdata(WRITER_PATH, 'caps', 'audio')])
  const LAST_IMAGE_PROPERTY = 'last_image'

  const PREVIEW_INIT = {
    id: 'preview',
    kindId: 'scenicPreview',
    isHidden: true,
    isProtected: true,
    properties: {
      test: true
    },
    userTree: { hello: true }
  }

  beforeEach(() => {
    ({ socketStore, configStore, quiddityStore, shmdataStore, previewStore } = populateStores())

    LOG.error = jest.fn()
    socket = new Socket()
    socketStore.setActiveSocket(socket)

    jest.spyOn(previewStore, 'previewId', 'get').mockReturnValue(PREVIEW_NICKNAME)
  })

  describe('constructor', () => {
    beforeEach(() => {
      previewStore.handleSocketChange = jest.fn()
    })

    it('should fail if the configStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new PreviewStore(socketStore) }).toThrow(RequirementError)
      /* eslint-enable no-new */
    })

    it('should fail if the quiddityStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new PreviewStore(socketStore, configStore) }).toThrow(RequirementError)
      /* eslint-enable no-new */
    })

    it('should fail if the shmdataStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new PreviewStore(socketStore, configStore, quiddityStore) }).toThrow(RequirementError)
      /* eslint-enable no-new */
    })

    it('should be correctly instantiated', () => {
      previewStore = new PreviewStore(socketStore, configStore, quiddityStore, shmdataStore) // eslint-disable-line no-new
      expect(previewStore).toBeDefined()
    })

    it('should react to the SocketStore\'s active socket change', () => {
      socketStore.activeSocket = new Socket()
      expect(previewStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })
  })

  describe('initialize', () => {
    const QUIDDITY_TEST = new Quiddity(PREVIEW_NICKNAME, PREVIEW_NICKNAME, PREVIEW_KIND_ID)

    beforeEach(() => {
      previewStore.fallbackPreviewQuiddity = jest.fn()
    })

    it('should not fallback if the quiddity store is not initialized', async () => {
      expect(previewStore.initialize()).rejects.toThrow(InitializationError)
      expect(previewStore.fallbackPreviewQuiddity).not.toHaveBeenCalled()
    })

    it('should not fallback if the preview quiddity was already created', async () => {
      quiddityStore.quiddities.set(PREVIEW_NICKNAME, QUIDDITY_TEST)
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      await previewStore.initialize()
      expect(previewStore.fallbackPreviewQuiddity).not.toHaveBeenCalled()
    })

    it('should fallback the SOAP quiddity if the quiddity is initialized and the preview quiddity was not created', async () => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      await previewStore.initialize()
      expect(previewStore.fallbackPreviewQuiddity).toHaveBeenCalled()
    })
  })

  describe('fallbackPreviewQuiddity', () => {
    beforeEach(() => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      configStore.findInitialConfiguration = jest.fn().mockReturnValue(PREVIEW_INIT)
      quiddityStore.applyQuiddityCreation = jest.fn().mockResolvedValue(true)
    })

    it('should request the Preview quiddity creation if it was not created', async () => {
      await previewStore.fallbackPreviewQuiddity()
      expect(configStore.findInitialConfiguration).toHaveBeenCalledWith(PREVIEW_KIND_ID, PREVIEW_NICKNAME)
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith(PREVIEW_KIND_ID, PREVIEW_NICKNAME, PREVIEW_INIT.properties, PREVIEW_INIT.userTree)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should request the Preview quiddity creation with no default properties and userTree if they don\'t exist', async () => {
      configStore.findInitialConfiguration = jest.fn().mockReturnValue(null)
      await previewStore.fallbackPreviewQuiddity()
      expect(configStore.findInitialConfiguration).toHaveBeenCalledWith(PREVIEW_KIND_ID, PREVIEW_NICKNAME)
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith(PREVIEW_KIND_ID, PREVIEW_NICKNAME)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if the request has failed', async () => {
      quiddityStore.applyQuiddityCreation = jest.fn().mockRejectedValue(false)
      await previewStore.fallbackPreviewQuiddity()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('handleSocketChange', () => {
    beforeEach(() => {
      ({ propertyAPI, infoTreeAPI } = socketStore.APIs)

      previewStore.handleShmdataDisconnection = jest.fn()
      previewStore.handleLastImage = jest.fn()
    })

    it('should define the APIs if there is a new socket', () => {
      const onPruneSpy = jest.spyOn(infoTreeAPI, 'onPruned')
      const onUpdateSpy = jest.spyOn(propertyAPI, 'onUpdated')
      const newSocket = new Socket()

      previewStore.handleSocketChange(newSocket)

      expect(propertyAPI).toBeDefined()
      expect(infoTreeAPI).toBeDefined()

      expect(onUpdateSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )

      expect(onPruneSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
    })

    it('should call update when `property_updated` is emitted and (quiddity ID and property) match and store is accepting updates', () => {
      const value = 'test'
      previewStore.acceptingUpdates = true
      previewStore.handleLastImage = jest.fn()
      socket.onEmit('property_updated', PREVIEW_NICKNAME, LAST_IMAGE_PROPERTY, value)
      expect(previewStore.handleLastImage).toHaveBeenCalledWith(value)
    })

    it('should not call update when `property_updated` is emitted but store is not accepting updates', () => {
      const value = 'test'
      previewStore.handleLastImage = jest.fn()
      socket.onEmit('property_updated', PREVIEW_NICKNAME, LAST_IMAGE_PROPERTY, value)
      expect(previewStore.handleLastImage).not.toHaveBeenCalled()
    })

    it('should not call update when `property_updated` is emitted and (quiddity ID and property) don\'t match', () => {
      const value = 'test'
      previewStore.handleLastImage = jest.fn()
      socket.onEmit('property_updated', 'failure', LAST_IMAGE_PROPERTY, value)
      expect(previewStore.handleLastImage).not.toHaveBeenCalled()
      socket.onEmit('property_updated', PREVIEW_NICKNAME, 'i am a failure', value)
      expect(previewStore.handleLastImage).not.toHaveBeenCalled()
    })

    it('should call update when `info_tree_pruned` is emitted and (quiddity ID and path) match', () => {
      previewStore.handleShmdataDisconnection = jest.fn()
      socket.onEmit('info_tree_pruned', PREVIEW_NICKNAME, WRITER_BRANCH)
      expect(previewStore.handleShmdataDisconnection).toHaveBeenCalled()
    })

    it('should not call update when `info_tree_pruned` is emitted and (quiddity ID and path) don\'t match', () => {
      previewStore.handleShmdataDisconnection = jest.fn()
      socket.onEmit('info_tree_pruned', 'failure', WRITER_BRANCH)
      expect(previewStore.handleShmdataDisconnection).not.toHaveBeenCalled()
      socket.onEmit('info_tree_pruned', PREVIEW_NICKNAME, 'failure ')
      expect(previewStore.handleShmdataDisconnection).not.toHaveBeenCalled()
    })
  })

  describe('handleShmdataDisconnection', () => {
    beforeEach(() => {
      previewStore.setPreview = jest.fn()
      URL.revokeObjectURL = jest.fn()
    })

    it('should revoke URL and delete preview if it exists', () => {
      previewStore.url = 'url'
      previewStore.handleShmdataDisconnection()
      expect(URL.revokeObjectURL).toHaveBeenCalledWith('url')
      expect(previewStore.setPreview).toHaveBeenCalledWith(null)
    })

    it('should not revoke URL if it doesn\'t exist', () => {
      previewStore.handleShmdataDisconnection()
      expect(URL.revokeObjectURL).not.toHaveBeenCalled()
      expect(previewStore.setPreview).toHaveBeenCalledWith(null)
    })
  })

  describe('applyPreviewConnection', () => {
    beforeEach(() => {
      socketStore.APIs.quiddityAPI.connectSfid = jest.fn()
      previewStore.applyPreviewDisconnection = jest.fn()
    })

    it('should not request quiddity connection if shmdata is not a video', () => {
      previewStore.applyPreviewConnection(AUDIO_MATRIX_ENTRY)
      expect(previewStore.applyPreviewDisconnection).not.toHaveBeenCalled()
      expect(socketStore.APIs.quiddityAPI.connectSfid).not.toHaveBeenCalled()
      expect(previewStore.acceptingUpdates).toEqual(false)
    })

    it('should request quiddity connections if shmdata is a video', () => {
      previewStore.applyPreviewConnection(VIDEO_MATRIX_ENTRY)
      expect(previewStore.applyPreviewDisconnection).not.toHaveBeenCalled()
      expect(socketStore.APIs.quiddityAPI.connectSfid).toHaveBeenCalled()
      expect(previewStore.acceptingUpdates).toEqual(true)
    })

    it('should request quiddity disconnection of existing shmdatas before connecting a new one', () => {
      previewStore.setPreview('url')
      previewStore.applyPreviewConnection(VIDEO_MATRIX_ENTRY)
      expect(previewStore.applyPreviewDisconnection).toHaveBeenCalled()
      expect(socketStore.APIs.quiddityAPI.connectSfid).toHaveBeenCalled()
      expect(previewStore.acceptingUpdates).toEqual(true)
    })
  })

  describe('applyPreviewDisconnection', () => {
    it('should disconnect all shmdatas and block future updates', () => {
      quiddityStore.applyQuiddityDisconnection = jest.fn()
      previewStore.acceptingUpdates = true

      previewStore.applyPreviewDisconnection()
      expect(previewStore.acceptingUpdates).toEqual(false)
      expect(quiddityStore.applyQuiddityDisconnection).toHaveBeenCalled()
    })
  })

  describe('setPreview', () => {
    it('should set preview url', () => {
      previewStore.setPreview('url')
      expect(previewStore.url).toEqual('url')
      previewStore.setPreview(null)
      expect(previewStore.url).toEqual(null)
    })
  })
})
