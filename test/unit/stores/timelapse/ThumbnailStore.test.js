/* global Blob describe it expect jest beforeEach */
import { Socket } from '@fixture/socket'
import { configure } from 'mobx'

import InitStateEnum from '@models/common/InitStateEnum'
import Shmdata from '@models/shmdata/Shmdata'
import Quiddity from '@models/quiddity/Quiddity'

import ThumbnailStore, { LOG, THUMBNAIL_KIND_ID, THUMBNAIL_NICKNAME } from '@stores/timelapse/ThumbnailStore'
import { DISPLAY_THUMBNAILS_ID } from '@stores/common/SettingStore'

import { videotestsrc, jacksrc } from '@fixture/allQuiddities'

import populateStores from '@stores/populateStores.js'
import { InitializationError, RequirementError } from '@utils/errors'

configure({ safeDescriptors: false })

describe('ThumbnailStore', () => {
  let socket, socketStore, configStore, quiddityStore, shmdataStore, thumbnailStore, settingStore, capsStore
  let imageAPI
  const CAPS = 'caps'
  const CATEGORY_VIDEO = 'video'
  const WRITER_ID = 'test'
  const WRITER_PATH = '/tmp/' + WRITER_ID
  const WRITER_BRANCH = '.shmdata.writer.' + WRITER_PATH
  const WRITER_SHMDATA = new Shmdata(WRITER_PATH, CAPS, CATEGORY_VIDEO)
  const IMAGE_PATH = WRITER_PATH + '_10.jpg'
  const LAST_IMAGE_PROPERTY = 'last_image'

  const THUMBNAIL_INIT = {
    id: 'thumbnails',
    kindId: 'scenicThumbnail',
    isHidden: true,
    isProtected: true,
    properties: {
      test: true
    },
    userTree: { hello: true }
  }

  const THUMBNAIL_ID = 'thumbnails'

  beforeEach(() => {
    ({ socketStore, configStore, quiddityStore, shmdataStore, thumbnailStore, settingStore, capsStore } = populateStores())

    LOG.warn = jest.fn()

    socket = new Socket()
    socketStore.setActiveSocket(socket)

    jest.spyOn(thumbnailStore, 'thumbnailId', 'get').mockReturnValue(THUMBNAIL_ID)
  })

  describe('constructor', () => {
    beforeEach(() => {
      thumbnailStore.handleSocketChange = jest.fn()
    })

    it('should fail if the configStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => {
        new ThumbnailStore(socketStore)
      }).toThrow(RequirementError)
      /* eslint-enable no-new */
    })

    it('should fail if the quiddityStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => {
        new ThumbnailStore(socketStore, configStore)
      }).toThrow(RequirementError)
      /* eslint-enable no-new */
    })

    it('should fail if the shmdataStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => {
        new ThumbnailStore(socketStore, configStore, quiddityStore)
      }).toThrow(RequirementError)
      /* eslint-enable no-new */
    })

    it('should fail if the settingStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => {
        new ThumbnailStore(socketStore, configStore, quiddityStore, shmdataStore)
      }).toThrow(RequirementError)
      /* eslint-enable no-new */
    })

    it('should fail if the capsStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => {
        new ThumbnailStore(socketStore, configStore, quiddityStore, shmdataStore, settingStore)
      }).toThrow(RequirementError)
      /* eslint-enable no-new */
    })

    it('should be correctly instantiated', () => {
      thumbnailStore = new ThumbnailStore(socketStore, configStore, quiddityStore, shmdataStore, settingStore, capsStore) // eslint-disable-line no-new
      expect(thumbnailStore).toBeDefined()
      expect(thumbnailStore.initState).toEqual(InitStateEnum.NOT_INITIALIZED)
    })

    it('should react to the SocketStore\'s active socket change', () => {
      socketStore.activeSocket = new Socket()
      expect(thumbnailStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })

    it('should react to changes to the QuiddityStore\'s initialization state', () => {
      thumbnailStore.handleQuiddityStoreInitialization = jest.fn()
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      expect(thumbnailStore.handleQuiddityStoreInitialization).toHaveBeenCalledWith(InitStateEnum.INITIALIZED)
      quiddityStore.setInitState(InitStateEnum.NOT_INITIALIZED)
      expect(thumbnailStore.handleQuiddityStoreInitialization).toHaveBeenCalledWith(InitStateEnum.NOT_INITIALIZED)
    })

    it('should react to new writing shmdatas', () => {
      thumbnailStore.applyThumbnailConnection = jest.fn()
      shmdataStore.shmdatas.set(WRITER_PATH, WRITER_SHMDATA)
      shmdataStore.writerPaths.set('testId', new Set([WRITER_PATH]))
      expect(thumbnailStore.applyThumbnailConnection).toHaveBeenCalled()
      shmdataStore.writerPaths.delete('testId')
      expect(thumbnailStore.applyThumbnailConnection).toHaveBeenCalled()
    })

    it('should react to displayThumbnails change', () => {
      thumbnailStore.handleSettingChanges = jest.fn()
      settingStore.setSetting(DISPLAY_THUMBNAILS_ID, true)
      expect(thumbnailStore.handleSettingChanges).toHaveBeenCalled()
    })
  })

  describe('initialize', () => {
    const QUIDDITY_TEST = new Quiddity(THUMBNAIL_ID, 'thumbnail0', THUMBNAIL_KIND_ID)

    beforeEach(() => {
      thumbnailStore.fallbackThumbnailQuiddity = jest.fn()
    })

    it('should not initialize if store is already initialized', async () => {
      thumbnailStore.setInitState(InitStateEnum.INITIALIZED)
      thumbnailStore.setInitState = jest.fn()

      expect(await thumbnailStore.initialize()).toEqual(true)
      expect(thumbnailStore.fallbackThumbnailQuiddity).not.toHaveBeenCalled()
      expect(thumbnailStore.setInitState).not.toHaveBeenCalled()
    })

    it('should initialize store', async () => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      expect(await thumbnailStore.initialize()).toEqual(true)
      expect(thumbnailStore.fallbackThumbnailQuiddity).toHaveBeenCalled()
      expect(thumbnailStore.initState).toEqual(InitStateEnum.INITIALIZED)
    })

    it('should not fallback if the quiddity store is not initialized', async () => {
      expect(thumbnailStore.initialize()).rejects.toThrow(InitializationError)
      expect(thumbnailStore.fallbackThumbnailQuiddity).not.toHaveBeenCalled()
    })

    it('should not fallback if the thumnail quiddity was already created', async () => {
      quiddityStore.quiddities.set(THUMBNAIL_NICKNAME, QUIDDITY_TEST)
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      await thumbnailStore.initialize()
      expect(thumbnailStore.fallbackThumbnailQuiddity).not.toHaveBeenCalled()
    })
  })

  describe('fallbackThumbnailQuiddity', () => {
    beforeEach(() => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      configStore.findInitialConfiguration = jest.fn().mockReturnValue(THUMBNAIL_INIT)
      quiddityStore.applyQuiddityCreation = jest.fn().mockResolvedValue(true)
    })

    it('should request the Thumbnail quiddity creation if it was not created', async () => {
      await thumbnailStore.fallbackThumbnailQuiddity()
      expect(configStore.findInitialConfiguration).toHaveBeenCalledWith(THUMBNAIL_KIND_ID, THUMBNAIL_NICKNAME)
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith(THUMBNAIL_KIND_ID, THUMBNAIL_NICKNAME, THUMBNAIL_INIT.properties, THUMBNAIL_INIT.userTree)
    })

    it('should request the Thumbnail quiddity creation with no default properties and userTree if they don\'t exist', async () => {
      configStore.findInitialConfiguration = jest.fn().mockReturnValue(null)
      await thumbnailStore.fallbackThumbnailQuiddity()
      expect(configStore.findInitialConfiguration).toHaveBeenCalledWith(THUMBNAIL_KIND_ID, THUMBNAIL_NICKNAME)
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith(THUMBNAIL_KIND_ID, THUMBNAIL_NICKNAME)
    })

    it('should log an error if the request has failed', async () => {
      LOG.error = jest.fn()
      quiddityStore.applyQuiddityCreation = jest.fn().mockRejectedValue(false)
      await thumbnailStore.fallbackThumbnailQuiddity()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('handleSocketChange', () => {
    let infoTreeAPI, propertyAPI

    beforeEach(() => {
      ({ infoTreeAPI, propertyAPI } = socketStore.APIs)

      thumbnailStore.handleShmdataDisconnection = jest.fn()
      thumbnailStore.handleLastImage = jest.fn()
      thumbnailStore.clear = jest.fn()
    })

    it('should define the APIs if there is a new socket', () => {
      const onPruneSpy = jest.spyOn(infoTreeAPI, 'onPruned')
      const onUpdateSpy = jest.spyOn(propertyAPI, 'onUpdated')
      const newSocket = new Socket()

      thumbnailStore.handleSocketChange(newSocket)
      expect(thumbnailStore.clear).toHaveBeenCalled()

      expect(onUpdateSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
      expect(onPruneSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
    })

    it('should clear the store if the new socket is null', () => {
      thumbnailStore.handleSocketChange(null)
      expect(thumbnailStore.clear).toHaveBeenCalled()
    })

    it('should call update when `property_updated` is emitted and (quiddity ID and property) match', () => {
      const value = 'test'
      thumbnailStore.handleLastImage = jest.fn()
      socket.onEmit('property_updated', THUMBNAIL_ID, LAST_IMAGE_PROPERTY, value)
      expect(thumbnailStore.handleLastImage).toHaveBeenCalledWith(value)
    })

    it('should not call update when `property_updated` is emitted and (quiddity ID and property) don\'t match', () => {
      const value = 'test'
      thumbnailStore.handleLastImage = jest.fn()
      socket.onEmit('property_updated', 'failure', LAST_IMAGE_PROPERTY, value)
      expect(thumbnailStore.handleLastImage).not.toHaveBeenCalled()
      socket.onEmit('property_updated', THUMBNAIL_ID, 'i am a failure', value)
      expect(thumbnailStore.handleLastImage).not.toHaveBeenCalled()
    })

    it('should call update when `info_tree_pruned` is emitted and (quiddity ID and path) match', () => {
      thumbnailStore.handleShmdataDisconnection = jest.fn()
      socket.onEmit('info_tree_pruned', THUMBNAIL_ID, WRITER_BRANCH)
      expect(thumbnailStore.handleShmdataDisconnection).toHaveBeenCalledWith(WRITER_BRANCH)
    })

    it('should not call update when `info_tree_pruned` is emitted and (quiddity ID and path) don\'t match', () => {
      thumbnailStore.handleShmdataDisconnection = jest.fn()
      socket.onEmit('info_tree_pruned', 'failure', WRITER_BRANCH)
      expect(thumbnailStore.handleShmdataDisconnection).not.toHaveBeenCalled()
      socket.onEmit('info_tree_pruned', THUMBNAIL_ID, 'failure ')
      expect(thumbnailStore.handleShmdataDisconnection).not.toHaveBeenCalled()
    })
  })

  describe('handleQuiddityStoreInitialization', () => {
    beforeEach(() => {
      thumbnailStore.clear = jest.fn()
    })

    it('should clear the Store if the QuiddityStore gets uninitialized', () => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      expect(thumbnailStore.clear).not.toHaveBeenCalled()
      quiddityStore.setInitState(InitStateEnum.NOT_INITIALIZED)
      expect(thumbnailStore.clear).toHaveBeenCalled()
    })
  })

  describe('handleShmdataDisconnection', () => {
    beforeEach(() => {
      thumbnailStore.removeThumbnail = jest.fn()
      URL.revokeObjectURL = jest.fn()
    })

    it('should revoke URL and delete thumbnail if it exists in the thumbnailUrls Map', () => {
      thumbnailStore.thumbnailUrls.set(WRITER_ID, 'url')
      thumbnailStore.handleShmdataDisconnection(WRITER_BRANCH)
      expect(URL.revokeObjectURL).toHaveBeenCalledWith('url')
      expect(thumbnailStore.removeThumbnail).toHaveBeenCalledWith(WRITER_ID)
    })

    it('should revoke URL and delete thumbnail if it doesn\'t exist in the thumbnailUrls Map', () => {
      thumbnailStore.handleShmdataDisconnection(WRITER_BRANCH)
      expect(URL.revokeObjectURL).not.toHaveBeenCalled()
      expect(thumbnailStore.removeThumbnail).not.toHaveBeenCalled()
    })
  })

  describe('handleLastImage', () => {
    beforeEach(() => {
      thumbnailStore.addThumbnail = jest.fn()
      settingStore.setSetting(DISPLAY_THUMBNAILS_ID, true)
    })

    it('should not add a thumbnail  when the displayThumbnail property is set to false', async () => {
      settingStore.setSetting(DISPLAY_THUMBNAILS_ID, false)
      await thumbnailStore.handleLastImage(IMAGE_PATH)
      expect(thumbnailStore.addThumbnail).not.toHaveBeenCalled()
    })

    it('should add a thumbnail when the displayThumbnail property is true', async () => {
      const url = 'url'
      settingStore.setSetting(DISPLAY_THUMBNAILS_ID, true)
      thumbnailStore.makeLastImage = jest.fn().mockResolvedValue(url)
      await thumbnailStore.handleLastImage(IMAGE_PATH)
      expect(thumbnailStore.addThumbnail).toHaveBeenCalled()
    })
  })

  describe('makeLastImage', () => {
    const blobUrl1 = 'url1'
    const blobUrl2 = 'url2'
    const testBlob = new Blob(['test'])
    beforeEach(() => {
      ({ imageAPI } = socketStore.APIs)

      imageAPI.readImage = jest.fn().mockResolvedValue(testBlob)
      thumbnailStore.addThumbnail = jest.fn()

      URL.revokeObjectURL = jest.fn()
      URL.createObjectURL = jest.fn().mockReturnValue(blobUrl1)
    })

    it('should read new image to add it to the thumbnailUrls Map', async () => {
      await thumbnailStore.makeLastImage(IMAGE_PATH, WRITER_ID)
      expect(imageAPI.readImage).toHaveBeenCalledWith(IMAGE_PATH)
      expect(URL.revokeObjectURL).not.toHaveBeenCalled()
      expect(URL.createObjectURL).toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should read revoke the old url if it exists in the thumbnailUrls Map', async () => {
      thumbnailStore.thumbnailUrls.set(WRITER_ID, blobUrl2)
      await thumbnailStore.makeLastImage(IMAGE_PATH, WRITER_ID)
      expect(imageAPI.readImage).toHaveBeenCalledWith(IMAGE_PATH)
      expect(URL.revokeObjectURL).toHaveBeenCalledWith(blobUrl2)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if a request fails', async () => {
      imageAPI.readImage = jest.fn().mockRejectedValue(false)
      await thumbnailStore.makeLastImage(IMAGE_PATH)
      expect(imageAPI.readImage).toHaveBeenCalledWith(IMAGE_PATH)
      expect(URL.revokeObjectURL).not.toHaveBeenCalled()
      expect(URL.createObjectURL).not.toHaveBeenCalled()
      expect(LOG.warn).toHaveBeenCalled()
    })
  })

  describe('applyThumbnailConnection', () => {
    beforeEach(() => {
      socketStore.APIs.quiddityAPI.connectSfid = jest.fn()
      thumbnailStore.setInitState(InitStateEnum.INITIALIZED)
    })

    it('should not request quiddity connections if store is not initialized', () => {
      thumbnailStore.setInitState(InitStateEnum.NOT_INITIALIZED)
      quiddityStore.addQuiddity(videotestsrc)
      thumbnailStore.applyThumbnailConnection()
      expect(socketStore.APIs.quiddityAPI.connectSfid).not.toHaveBeenCalled()
    })

    it('should not request quiddity connections if no video shmdata exist', () => {
      thumbnailStore.applyThumbnailConnection()
      expect(socketStore.APIs.quiddityAPI.connectSfid).not.toHaveBeenCalled()
    })

    it('should not request quiddity connections if the thumbnail quiddity is already connected to the right shmdata', () => {
      const thumbnailQuiddity = new Quiddity(THUMBNAIL_ID, 'thumbnail0', THUMBNAIL_KIND_ID)
      thumbnailQuiddity.infoTree.shmdata = {
        reader: videotestsrc.infoTree.shmdata.writer
      }

      quiddityStore.addQuiddity(videotestsrc)
      quiddityStore.addQuiddity(thumbnailQuiddity)

      thumbnailStore.applyThumbnailConnection()
      expect(socketStore.APIs.quiddityAPI.connectSfid).not.toHaveBeenCalled()
    })

    it('should not request quiddity connections if shmdata is not video', () => {
      quiddityStore.addQuiddity(jacksrc)
      thumbnailStore.applyThumbnailConnection()
      expect(socketStore.APIs.quiddityAPI.connectSfid).not.toHaveBeenCalled()
    })

    it('should request quiddity connection if thumbnail doesn\'t already exists and is video shmdata', () => {
      const thumbnailQuiddity = new Quiddity(THUMBNAIL_ID, 'thumbnail0', THUMBNAIL_KIND_ID)
      quiddityStore.addQuiddity(thumbnailQuiddity)
      quiddityStore.addQuiddity(videotestsrc)
      thumbnailStore.applyThumbnailConnection()
      expect(socketStore.APIs.quiddityAPI.connectSfid).toHaveBeenCalledWith(videotestsrc.id, THUMBNAIL_ID, 1)
    })
  })

  describe('addThumbnail', () => {
    it('should add thumbnail to Map', () => {
      thumbnailStore.addThumbnail(WRITER_ID, 'url')
      expect(thumbnailStore.thumbnailUrls.get(WRITER_ID)).toEqual('url')
    })
  })

  describe('removeThumbnail', () => {
    it('should remove thumbnail from Map', () => {
      thumbnailStore.addThumbnail(WRITER_ID, 'url')
      thumbnailStore.removeThumbnail(WRITER_ID)
      expect(thumbnailStore.thumbnailUrls.get(WRITER_ID)).toEqual(undefined)
    })
  })

  describe('clear', () => {
    it('should clear ThumbnailStore', () => {
      thumbnailStore.setInitState(InitStateEnum.INITIALIZED)
      thumbnailStore.addThumbnail(WRITER_ID, 'url')

      thumbnailStore.clear()
      expect(thumbnailStore.thumbnailUrls.size).toEqual(0)
      expect(thumbnailStore.initState).toEqual(InitStateEnum.NOT_INITIALIZED)
    })
  })

  describe('handleSettingChanges', () => {
    beforeEach(() => {
      thumbnailStore.handleSettingChanges = jest.fn()
    })

    it('should create the thumbnails quiddities when displayThumbnails is true', () => {
      settingStore.setSetting(DISPLAY_THUMBNAILS_ID, true)
      expect(thumbnailStore.handleSettingChanges).toHaveBeenCalled()
    })

    it('should remove the thumbnails quiddities when displayThumbnails is false', () => {
      const QUIDDITY_TEST = new Quiddity(THUMBNAIL_ID, 'thumbnail0', THUMBNAIL_KIND_ID)
      quiddityStore.quiddities.set(THUMBNAIL_ID, QUIDDITY_TEST)
      settingStore.setSetting(DISPLAY_THUMBNAILS_ID, false)
      expect(thumbnailStore.handleSettingChanges).toHaveBeenCalled()
    })
  })
})
