/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'
import { configure } from 'mobx'

import populateStores from '@stores/populateStores.js'
import StatStore, { LOG, DEFAULT_STAT } from '@stores/shmdata/StatStore'

import Stat from '@models/shmdata/Stat'
import Quiddity from '@models/quiddity/Quiddity'
import Shmdata from '@models/shmdata/Shmdata'

configure({ computedConfigurable: true, safeDescriptors: false })

describe('StatStore', () => {
  const JSON_STAT = { byte_rate: 1, rate: 1 }
  const QUIDDITY = new Quiddity('test', 'test0', 'test')
  const SHMDATA = new Shmdata('test', 'test', 'test')
  const SHMDATA_ALT = new Shmdata('alt', 'alt', 'alt')
  const ACTIVE_STAT = new Stat(1, 1)

  let socketStore, quiddityStore, shmdataStore, statStore

  function getShmdataTreePath (shmdata, isStat) {
    if (isStat) {
      return `.shmdata.writer.${shmdata.path}.stat`
    } else {
      return `.shmdata.writer.${shmdata.path}`
    }
  }

  beforeEach(() => {
    ({ socketStore, quiddityStore, shmdataStore, statStore } = populateStores())

    LOG.error = jest.fn()
    LOG.warn = jest.fn()

    socketStore.setActiveSocket(new Socket())
  })

  describe('constructor', () => {
    beforeEach(() => {
      statStore.handleSocketChange = jest.fn()
    })

    it('should fail if the quiddityStore is not given', () => {
      expect(() => new StatStore(socketStore)).toThrow()
    })

    it('should fail if the shmdataStore is not given', () => {
      expect(() => new StatStore(socketStore, quiddityStore)).toThrow()
    })
  })

  describe('bitrates', () => {
    beforeEach(() => {
      jest.spyOn(shmdataStore, 'allShmdataPaths', 'get').mockReturnValue([SHMDATA.path])
    })

    it('should get the label of the default stat when the stat is not updated', () => {
      expect(statStore.bitrates.get(SHMDATA.path)).toEqual(DEFAULT_STAT.mbpsLabel)
    })

    it('should get the label of the updated stat', () => {
      statStore.setStat(SHMDATA.path, ACTIVE_STAT)
      expect(statStore.bitrates.get(SHMDATA.path)).toEqual(ACTIVE_STAT.mbpsLabel)
    })
  })

  describe('handleSocketChange', () => {
    beforeEach(() => {
      statStore.clear = jest.fn()
    })

    it('should clear the store when the socket change', () => {
      socketStore.activeSocket = new Socket()
      expect(statStore.clear).toHaveBeenCalled()
    })
  })

  describe('isStatActive', () => {
    it('should return false if the statStore has no shmdata stat', () => {
      expect(statStore.isStatActive(SHMDATA.path)).toEqual(false)
    })

    it('should return false if the requested stat isn\'t active', () => {
      statStore.setStat(SHMDATA.path, DEFAULT_STAT)
      expect(statStore.isStatActive(SHMDATA.path)).toEqual(false)
    })

    it('should return true if the requested stat is active', () => {
      statStore.setStat(SHMDATA.path, ACTIVE_STAT)
      expect(statStore.isStatActive(SHMDATA.path)).toEqual(true)
    })
  })

  describe('getInactiveShmdatas', () => {
    beforeEach(() => {
      shmdataStore.getShmdataSet = jest.fn().mockReturnValue(new Set([
        SHMDATA,
        SHMDATA_ALT
      ]))

      statStore.setStat(SHMDATA.path, DEFAULT_STAT)
      statStore.setStat(SHMDATA_ALT.path, ACTIVE_STAT)
    })

    it('should get all the inactive shmdatas', () => {
      expect(statStore.getInactiveShmdatas(QUIDDITY.id)).toEqual([SHMDATA])
    })
  })

  describe('handleGraftedStat', () => {
    let setSpy

    beforeEach(() => {
      setSpy = jest.spyOn(statStore, 'setStat')
    })

    it('shouldn\'t set the stat if the value is null', () => {
      statStore.handleGraftedStat(getShmdataTreePath(SHMDATA), { stat: null })
      expect(setSpy).not.toHaveBeenCalled()
    })

    it('should set the stat from the value if the path represents a stat update', () => {
      statStore.handleGraftedStat(getShmdataTreePath(SHMDATA, true), JSON_STAT)
      expect(setSpy).toHaveBeenCalled()
      expect(statStore.stats.get(SHMDATA.path)).toEqual(Stat.fromJSON(JSON_STAT))
    })

    it('should set the stat from the stat property of the value if the path represents a shmdata', () => {
      statStore.handleGraftedStat(getShmdataTreePath(SHMDATA), { stat: JSON_STAT })
      expect(setSpy).toHaveBeenCalled()
      expect(statStore.stats.get(SHMDATA.path)).toEqual(Stat.fromJSON(JSON_STAT))
    })

    it('shouldn\'t set the stat if the stat isn\'t updated', () => {
      statStore.isStatUpdated = jest.fn().mockReturnValue(false)
      statStore.handleGraftedStat(getShmdataTreePath(SHMDATA), { stat: JSON_STAT })
      expect(setSpy).not.toHaveBeenCalled()
    })
  })

  describe('handlePrunedStat', () => {
    let removeSpy

    beforeEach(() => {
      removeSpy = jest.spyOn(statStore, 'removeStat')
    })

    it('should delete the stat if it was added', () => {
      statStore.setStat(SHMDATA.path, DEFAULT_STAT)
      statStore.handlePrunedStat(getShmdataTreePath(SHMDATA))
      expect(removeSpy).toHaveBeenCalled()
    })

    it('shouldn\'t delete the stat if it wasn\'t added', () => {
      statStore.handlePrunedStat(getShmdataTreePath(SHMDATA))
      expect(removeSpy).not.toHaveBeenCalled()
    })
  })

  describe('makeStatModel', () => {
    it('should make a Stat model from a json value', () => {
      expect(statStore.makeStatModel(JSON_STAT)).toEqual(Stat.fromJSON(JSON_STAT))
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should return the default stat if the json value isn\'t valid', () => {
      expect(statStore.makeStatModel('I am a failure')).toEqual(DEFAULT_STAT)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('isStatUpdated', () => {
    it('should return true if the statStore has no stat', () => {
      expect(statStore.isStatUpdated(SHMDATA.path, DEFAULT_STAT)).toEqual(true)
    })

    it('should return false if the statStore has the stat with the same value', () => {
      statStore.setStat(SHMDATA.path, DEFAULT_STAT)
      expect(statStore.isStatUpdated(SHMDATA.path, DEFAULT_STAT)).toEqual(false)
    })

    it('should return true if the statStore has the stat with a different value', () => {
      statStore.setStat(SHMDATA.path, DEFAULT_STAT)
      expect(statStore.isStatUpdated(SHMDATA.path, ACTIVE_STAT)).toEqual(true)
    })
  })
})
