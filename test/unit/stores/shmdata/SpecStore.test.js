/* global describe it expect beforeEach jest */

import { Socket } from '@fixture/socket'
import { configure } from 'mobx'

import SpecStore, { LOG } from '@stores/shmdata/SpecStore'
import Spec from '@models/shmdata/Spec'

import populateStores from '@stores/populateStores.js'
import { sdiInput, sipMedia, videoOutput } from '@fixture/allQuiddities'

configure({ safeDescriptors: false })

describe('SpecStore', () => {
  let socket, socketStore, specStore
  let connectionSpecsAPI

  beforeEach(() => {
    ({ socketStore, specStore } = populateStores())

    LOG.info = jest.fn()
    LOG.error = jest.fn()
    LOG.warn = jest.fn()

    socket = new Socket()
    socketStore.setActiveSocket(socket)
  })

  const sdiJsonSpec = sdiInput.connectionSpecs.writer[0]
  const sipMediaJsonSpec = sipMedia.connectionSpecs.writer[0]
  const videoOutputJsonSpec = videoOutput.connectionSpecs.follower[0]

  describe('constructor', () => {
    it('should react to the SocketStore\'s active socket change', () => {
      const { socketStore, specStore } = populateStores()

      specStore.handleSocketChange = jest.fn()
      expect(specStore.handleSocketChange).not.toHaveBeenCalled()

      socketStore.setActiveSocket(socket)
      expect(specStore.handleSocketChange).toHaveBeenCalledTimes(1)
      expect(specStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })

    it('should not fail if all required store are given', () => {
      expect(() => new SpecStore(socketStore)).not.toThrow()
    })
  })

  describe('specs', () => {
    it('should return an empty map if there are no writer specs and no follower specs', () => {
      expect(specStore.specs).toEqual(new Map())
    })

    it('should return a map of all writer specs hashed by quiddity ID if there are no follower specs', () => {
      specStore.addConnectionSpecs(sdiInput.id.id, sdiInput.id.connectionSpecs)
      expect(specStore.specs).toEqual(new Map(specStore.writerSpecs))
    })

    it('should return a map of all follower specs hashed by quiddity ID if there are no writer specs', () => {
      specStore.addConnectionSpecs(videoOutput.id, videoOutput.connectionSpecs)
      expect(specStore.specs).toEqual(new Map().set(videoOutput.id, specStore.followerSpecs.get(videoOutput.id)))
    })

    it('should return a map of all specs (follower and writer) hashed by quiddity ID', () => {
      specStore.addConnectionSpecs(sdiInput.id, sdiInput.connectionSpecs)
      specStore.addConnectionSpecs(videoOutput.id, videoOutput.connectionSpecs)
      expect(specStore.specs).toEqual(new Map(specStore.writerSpecs).set(videoOutput.id, specStore.followerSpecs.get(videoOutput.id)))
    })
  })

  describe('compatibleMediaTypes', () => {
    it('should return a map of all media types hashed by quiddity id', () => {
      expect(specStore.compatibleMediaTypes).toEqual(new Map())

      const videoOutputSpec = specStore.makeSpec(videoOutput.id, 'follower', videoOutputJsonSpec)
      specStore.addFollowerSpec(videoOutput.id, videoOutputSpec)
      expect(specStore.compatibleMediaTypes).toEqual(new Map().set(videoOutput.id, new Set().add(videoOutputJsonSpec.can_do[0])))
    })
  })

  describe('fetchConnectionSpecs', () => {
    beforeEach(() => {
      ({ connectionSpecsAPI } = socketStore.APIs)
    })

    it('should log an error if the API call fails', async () => {
      connectionSpecsAPI.get = jest.fn().mockRejectedValue(false)
      await specStore.fetchConnectionSpecs(sdiInput.id)
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should return the connection spec of a quiddity when the API succeeds', async () => {
      connectionSpecsAPI.get = jest.fn().mockResolvedValue({ test: 'test' })
      expect(await specStore.fetchConnectionSpecs(sdiInput.id)).toEqual({ test: 'test' })
      expect(LOG.error).not.toHaveBeenCalled()
    })
  })

  describe('addWriterSpec', () => {
    it('should add the writer spec of a source quiddity', () => {
      expect(specStore.writerSpecs.size).toEqual(0)

      const sdiSpec = specStore.makeSpec(sdiInput.id, 'writer', sdiJsonSpec)
      specStore.addWriterSpec(sdiInput.id, sdiSpec)
      expect(specStore.writerSpecs.size).toEqual(1)
      expect(LOG.info).toHaveBeenCalled()

      const sipMediaSpec = specStore.makeSpec(sipMedia.id, 'writer', sipMediaJsonSpec)
      specStore.addWriterSpec(sipMedia.id, sipMediaSpec)
      expect(specStore.writerSpecs.size).toEqual(2)
    })
  })

  describe('addFollowerSpec', () => {
    it('should add the follower spec of a destination quiddity', () => {
      expect(specStore.followerSpecs.size).toEqual(0)

      const videoOutputSpec = specStore.makeSpec(videoOutput.id, 'writer', videoOutputJsonSpec)
      specStore.addFollowerSpec(videoOutput.id, videoOutputSpec)
      expect(specStore.followerSpecs.size).toEqual(1)
      expect(LOG.info).toHaveBeenCalled()
    })
  })

  describe('makeSpec', () => {
    it('should log an error if it fails to parse a spec model', () => {
      specStore.makeSpec(sdiInput.id)
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should make a spec model when properly called', () => {
      Spec.fromJSON = jest.fn()
      specStore.makeSpec(sdiInput.id, 'writer', sdiJsonSpec)
      expect(LOG.error).not.toHaveBeenCalled()
      expect(Spec.fromJSON).toHaveBeenCalled()
    })
  })

  describe('addConnectionSpecs', () => {
    it('should add all the connection specs of a writer quiddity', () => {
      expect(specStore.writerSpecs.size).toEqual(0)

      specStore.addConnectionSpecs(sdiInput.id, sdiInput.connectionSpecs)
      expect(specStore.writerSpecs.size).toEqual(1)

      specStore.addConnectionSpecs(sipMedia.id, sipMedia.connectionSpecs)
      expect(specStore.writerSpecs.size).toEqual(2)
      expect(LOG.warn).not.toHaveBeenCalled()
    })

    it('should add all the connection specs of a follower quiddity', () => {
      expect(specStore.followerSpecs.size).toEqual(0)

      specStore.addConnectionSpecs(videoOutput.id, videoOutput.connectionSpecs)
      expect(specStore.followerSpecs.size).toEqual(1)
      expect(LOG.warn).not.toHaveBeenCalled()
    })
  })

  describe('removeSpec', () => {
    it('should delete the spec of a quiddity', () => {
      specStore.addConnectionSpecs(sdiInput.id, sdiInput.connectionSpecs)
      specStore.addConnectionSpecs(sdiInput.id, sipMedia.connectionSpecs)
      specStore.addConnectionSpecs(sipMedia.id, sipMedia.connectionSpecs)

      expect(specStore.specs.size).toEqual(2)
      specStore.removeSpec(sdiInput.id)
      expect(specStore.specs.size).toEqual(1)
      specStore.removeSpec(sipMedia.id)
      expect(specStore.specs.size).toEqual(0)
    })
  })

  describe('clear', () => {
    it('should clear all specs of a quiddity', () => {
      specStore.addConnectionSpecs(sdiInput.id, sdiInput.connectionSpecs)
      specStore.addConnectionSpecs(sipMedia.id, sipMedia.connectionSpecs)
      specStore.addConnectionSpecs(videoOutput.id, videoOutput.connectionSpecs)

      expect(specStore.writerSpecs.size).toEqual(2)
      expect(specStore.followerSpecs.size).toEqual(1)

      specStore.clear()
      expect(specStore.writerSpecs.size).toEqual(0)
    })
  })
})
