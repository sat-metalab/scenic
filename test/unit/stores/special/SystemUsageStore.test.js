/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'
import populateStores from '@stores/populateStores.js'
import { configure } from 'mobx'

import InitStateEnum from '@models/common/InitStateEnum'
import { InitializationError, RequirementError } from '@utils/errors'

import {
  SYSTEM_USAGE_NAME,
  SYSTEM_USAGE_KIND
} from '@models/quiddity/specialQuiddities'

import SystemUsageStore, {
  LOG,
  LOCAL_INTERFACE
} from '@stores/special/SystemUsageStore'

import CpuCore from '@models/systemUsage/CpuCore'
import NetworkInterface from '@models/systemUsage/NetworkInterface'

configure({ safeDescriptors: false })

const SYSTEM_USAGE_INIT = {
  id: 'systemUsage',
  kindId: 'systemUsage',
  isHidden: true,
  isProtected: true,
  properties: {
    period: 1.0
  },
  userTree: {}
}

const { INITIALIZED, INITIALIZING } = InitStateEnum

describe('SystemUsageStore', () => {
  let socket, socketStore, systemUsageStore, configStore, quiddityStore

  beforeEach(() => {
    ({ socketStore, systemUsageStore, configStore, quiddityStore } = populateStores())

    LOG.error = jest.fn()

    socket = new Socket()
    socketStore.setActiveSocket(socket)
  })

  describe('constructor', () => {
    beforeEach(() => {
      systemUsageStore.handleQuiddityStoreInitialization = jest.fn()
      systemUsageStore.handleSocketChange = jest.fn()
    })

    it('should fail if the configStore is not given', () => {
      expect(() => new SystemUsageStore(socketStore)).toThrow(RequirementError)
    })

    it('should fail if the quiddityStore is not given', () => {
      expect(() => new SystemUsageStore(socketStore, configStore)).toThrow(RequirementError)
    })

    it('should react to the QuiddityStore\'s initialization', () => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      expect(systemUsageStore.handleQuiddityStoreInitialization).toHaveBeenCalledWith(InitStateEnum.INITIALIZED)
    })

    it('should react to the SocketStore\'s active socket change', () => {
      socketStore.activeSocket = new Socket()
      expect(systemUsageStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })
  })

  describe('handleQuiddityStoreInitialization', () => {
    beforeEach(() => {
      systemUsageStore.clear = jest.fn()
    })

    it('should clear the Store if the QuiddityStore gets uninitialized', () => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      expect(systemUsageStore.clear).not.toHaveBeenCalled()
      quiddityStore.setInitState(InitStateEnum.NOT_INITIALIZED)
      expect(systemUsageStore.clear).toHaveBeenCalled()
    })
  })

  describe('initialize', () => {
    beforeEach(() => {
      systemUsageStore.fallbackSystemUsageQuiddity = jest.fn().mockResolvedValue(true)
      systemUsageStore.fetchAvailableInterfaces = jest.fn().mockResolvedValue({})
    })

    it('should do nothing if it was already initialized', async () => {
      systemUsageStore.setInitState(INITIALIZED)
      expect(await systemUsageStore.initialize()).toEqual(true)
      expect(systemUsageStore.fallbackSystemUsageQuiddity).not.toHaveBeenCalled()
    })

    it('should fail if the QuiddityStore hasn\'t started its initialization', async () => {
      expect(systemUsageStore.initialize()).rejects.toThrow(InitializationError)
    })

    it('should force systemUsage quiddity creation if it wasn\'t created', async () => {
      quiddityStore.setInitState(INITIALIZING)
      expect(await systemUsageStore.initialize()).toEqual(true)
      expect(systemUsageStore.fallbackSystemUsageQuiddity).toHaveBeenCalled()
    })

    it('should not force systemUsage quiddity creation if it was created', async () => {
      jest.spyOn(quiddityStore, 'usedKindIds', 'get').mockReturnValue(new Set().add(SYSTEM_USAGE_KIND))
      quiddityStore.setInitState(INITIALIZING)
      expect(await systemUsageStore.initialize()).toEqual(true)
      expect(systemUsageStore.fallbackSystemUsageQuiddity).not.toHaveBeenCalled()
    })
  })

  describe('fallbackSystemUsageQuiddity', () => {
    beforeEach(() => {
      quiddityStore.setInitState(InitStateEnum.INITIALIZED)
      configStore.findInitialConfiguration = jest.fn().mockReturnValue(SYSTEM_USAGE_INIT)
      quiddityStore.applyQuiddityCreation = jest.fn().mockResolvedValue(true)
    })

    it('should request the SystemUsage quiddity creation with no default properties and userTree if they don\'t exist', async () => {
      configStore.findInitialConfiguration = jest.fn().mockReturnValue(null)
      await systemUsageStore.fallbackSystemUsageQuiddity()
      expect(configStore.findInitialConfiguration).toHaveBeenCalledWith(SYSTEM_USAGE_KIND, SYSTEM_USAGE_NAME)
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith(SYSTEM_USAGE_KIND, SYSTEM_USAGE_NAME)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if the request has failed', async () => {
      quiddityStore.applyQuiddityCreation = jest.fn().mockRejectedValue(false)
      await systemUsageStore.fallbackSystemUsageQuiddity()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('handleSocketChange', () => {
    let infoTreeAPI

    beforeEach(() => {
      ({ infoTreeAPI } = socketStore.APIs)

      systemUsageStore.handleGraftedTop = jest.fn()
      systemUsageStore.clear = jest.fn()
    })

    it('should define the APIs if there is a new socket', () => {
      const onGraftSpy = jest.spyOn(infoTreeAPI, 'onGrafted')

      systemUsageStore.handleSocketChange(socket)

      expect(systemUsageStore.clear).toHaveBeenCalled()

      expect(onGraftSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
    })

    it('should clear the store if the new socket is null', () => {
      systemUsageStore.handleSocketChange(null)
      expect(systemUsageStore.clear).toHaveBeenCalled()
    })

    it('should call update when `info_tree_grafted` is emitted and (quiddity ID and path) match', () => {
      jest.spyOn(quiddityStore, 'usedKinds', 'get').mockReturnValue(new Map().set(SYSTEM_USAGE_NAME, { id: SYSTEM_USAGE_KIND }))
      socket.onEmit('info_tree_grafted', SYSTEM_USAGE_NAME, '.top.test', '"test"')
      expect(systemUsageStore.handleGraftedTop).toHaveBeenCalledWith('test')
    })

    it('should not call update when the path or ID doesn\'t match', () => {
      socket.onEmit('info_tree_grafted', SYSTEM_USAGE_NAME, 'i.am.a.failure', '"test"')

      expect(systemUsageStore.handleGraftedTop).not.toHaveBeenCalled()
      socket.onEmit('info_tree_grafted', 'fail', '.top.test', '"test"')
      expect(systemUsageStore.handleGraftedTop).not.toHaveBeenCalled()
    })
  })

  describe('handleGraftedTop', () => {
    beforeEach(() => {
      systemUsageStore.updateMemoryUsage = jest.fn()
      systemUsageStore.updateNetworkInterfacesUsage = jest.fn()
      systemUsageStore.updateCpuUsage = jest.fn()
    })

    it('should update the memory usage only if it is available', () => {
      const TOP_JSON = { mem: {} }
      systemUsageStore.handleGraftedTop(TOP_JSON)
      expect(systemUsageStore.updateMemoryUsage).toHaveBeenCalled()
      expect(systemUsageStore.updateNetworkInterfacesUsage).not.toHaveBeenCalled()
      expect(systemUsageStore.updateCpuUsage).not.toHaveBeenCalled()
    })

    it('should update the cpu cores usage only if it is available', () => {
      const TOP_JSON = { cpu: {} }
      systemUsageStore.handleGraftedTop(TOP_JSON)
      expect(systemUsageStore.updateMemoryUsage).not.toHaveBeenCalled()
      expect(systemUsageStore.updateNetworkInterfacesUsage).not.toHaveBeenCalled()
      expect(systemUsageStore.updateCpuUsage).toHaveBeenCalled()
    })

    it('should update the network interfaces usage only if it is available', () => {
      const TOP_JSON = { net: {} }
      systemUsageStore.handleGraftedTop(TOP_JSON)
      expect(systemUsageStore.updateMemoryUsage).not.toHaveBeenCalled()
      expect(systemUsageStore.updateNetworkInterfacesUsage).toHaveBeenCalled()
      expect(systemUsageStore.updateCpuUsage).not.toHaveBeenCalled()
    })
  })

  describe('updateCpuUsage', () => {
    const CPU_CORES = {
      cpu: new CpuCore('cpu').toJSON(),
      cpu0: new CpuCore('cpu0').toJSON(),
      cpu1: new CpuCore('cpu1').toJSON()
    }

    beforeEach(() => {
      LOG.error = jest.fn()
      systemUsageStore.addCpuCore = jest.fn()
    })

    it('should update the cpu usage for all cpu cores if the cpu core is not `cpu`', () => {
      systemUsageStore.updateCpuUsage(CPU_CORES)
      expect(systemUsageStore.addCpuCore).toHaveBeenCalledTimes(2)
    })

    it('should log an error if the cpu usage update failed', () => {
      systemUsageStore.updateCpuUsage({ '': {} })
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('updateNetworkInterfacesUsage', () => {
    const LOCAL_IP = '0.0.0.0'
    const ETHERNET_INTERFACE = 'eno1'
    const ETHERNET_IP = '0.0.0.0'

    const NETWORK_INTERFACES = {
      lo: new NetworkInterface(LOCAL_INTERFACE, '0.0.0.0', '0', '0').toJSON(),
      eno1: new NetworkInterface(ETHERNET_INTERFACE, '0.0.0.0', '0', '0').toJSON()
    }

    beforeEach(() => {
      systemUsageStore.availableInterfaces = {
        [LOCAL_INTERFACE]: LOCAL_IP,
        [ETHERNET_INTERFACE]: ETHERNET_IP
      }

      systemUsageStore.addNetworkInterface = jest.fn()
      systemUsageStore.updateInterfaceSelection = jest.fn()
    })

    it('should update network interface if the interfaces are available', () => {
      systemUsageStore.updateNetworkInterfacesUsage(NETWORK_INTERFACES)
      expect(systemUsageStore.addNetworkInterface).toHaveBeenCalled()
    })

    it('should update interface selection with ethernet interface', () => {
      systemUsageStore.updateNetworkInterfacesUsage(NETWORK_INTERFACES)
      expect(systemUsageStore.updateInterfaceSelection).toHaveBeenCalledWith(ETHERNET_INTERFACE)
    })

    it('should not update interface selection if it is already updated', () => {
      systemUsageStore.networkInterfaces.set(ETHERNET_INTERFACE, {})
      systemUsageStore.updateNetworkInterfacesUsage(NETWORK_INTERFACES)
      expect(systemUsageStore.updateInterfaceSelection).not.toHaveBeenCalled()
    })

    it('should log an error if the network interfaces usage update failed', () => {
      systemUsageStore.updateNetworkInterfacesUsage({ [LOCAL_INTERFACE]: {} })
      expect(LOG.error).toHaveBeenCalled()
    })
  })
})
