/* global describe it expect jest beforeEach */

import { RTMP_MODAL_ID } from '@components/modals/RtmpModal'

import RtmpStore, { LOG } from '@stores/special/RtmpStore'

import { RTMP_KIND_ID } from '@models/quiddity/specialQuiddities'

import populateStores from '@stores/populateStores.js'

import InitStateEnum from '@models/common/InitStateEnum'
import Property from '@models/quiddity/Property'
import MatrixEntry from '@models/matrix/MatrixEntry'

import { rtmpOutput } from '@fixture/allQuiddities'
import { getKind } from '@fixture/allQuiddityKinds'

describe('RtmpStore', () => {
  let socketStore, quiddityStore, maxLimitStore, modalStore, rtmpStore, lockStore, propertyStore, kindStore

  beforeEach(() => {
    ({
      socketStore,
      quiddityStore,
      maxLimitStore,
      modalStore,
      rtmpStore,
      lockStore,
      propertyStore,
      kindStore
    } = populateStores())

    LOG.error = jest.fn()
  })

  describe('constructor', () => {
    beforeEach(() => {
      rtmpStore.handleRtmpRequest = jest.fn()
    })

    it('should RtmpStore to be defined', () => {
      expect(rtmpStore).toBeDefined()
      expect(rtmpStore.isRtmpRequested).toEqual(false)
    })

    it('should fail if the quiddityStore is not given', () => {
      expect(() => new RtmpStore(socketStore)).toThrow()
    })

    it('should fail if the maxLimitStore is not given', () => {
      expect(() => new RtmpStore(socketStore, quiddityStore)).toThrow()
    })

    it('should fail if the modalStore is not given', () => {
      expect(() => new RtmpStore(socketStore, quiddityStore, maxLimitStore)).toThrow()
    })

    it('should handle RTMP request when it is requested', () => {
      rtmpStore.setRtmpRequestFlag(true)
      expect(rtmpStore.handleRtmpRequest).toHaveBeenCalledWith(true)
    })

    it('should register the lockable RTMP kind', () => {
      expect(lockStore.lockableKindIds.has(RTMP_KIND_ID)).toEqual(true)
    })
  })

  describe('handleStartedPropertyChange', () => {
    const startProperty = new Property('RTMP/started', 'boolean')
    const rtmpEntry = new MatrixEntry(rtmpOutput)

    let toggleSpy

    beforeEach(() => {
      toggleSpy = jest.spyOn(lockStore, 'setLock')

      kindStore.addKind(getKind(rtmpOutput))
      quiddityStore.addQuiddity(rtmpOutput)
    })

    it('should change the lock of a RTMP quiddity when its started property change', () => {
      propertyStore.addProperty(rtmpOutput.id, startProperty)
      expect(toggleSpy).toHaveBeenCalledWith(rtmpEntry, false)
      propertyStore.setPropertyValue(rtmpOutput.id, startProperty.id, true)
      expect(toggleSpy).toHaveBeenCalledWith(rtmpEntry, true)
      propertyStore.setPropertyValue(rtmpOutput.id, startProperty.id, false)
      expect(toggleSpy).toHaveBeenCalledWith(rtmpEntry, false)
    })
  })

  describe('handleRtmpRequest', () => {
    beforeEach(() => {
      modalStore.setActiveModal = jest.fn()
      modalStore.cleanActiveModal = jest.fn()
    })

    it('should make RtmpModal active or inactive depending on RTMP request status', () => {
      rtmpStore.setRtmpRequestFlag(true)
      expect(modalStore.setActiveModal).toHaveBeenCalledWith(RTMP_MODAL_ID)
      rtmpStore.setRtmpRequestFlag(false)
      expect(modalStore.cleanActiveModal).toHaveBeenCalledWith(RTMP_MODAL_ID)
    })
  })

  describe('applyRtmpQuiddityCreation', () => {
    const { INITIALIZED, NOT_INITIALIZED } = InitStateEnum
    const RTMP_URL = 'url'
    const RTMP_KEY = 'key'
    const RTMP_PROPERTIES = {
      'RTMP/stream_app_url': RTMP_URL,
      'RTMP/stream_key': RTMP_KEY
    }

    beforeEach(() => {
      quiddityStore.setInitState(INITIALIZED)
      quiddityStore.applyQuiddityCreation = jest.fn().mockResolvedValue(true)
      kindStore.addKind(getKind(rtmpOutput))
    })

    it('should create an RTMP quiddity with the input arguments', async () => {
      await rtmpStore.applyRtmpQuiddityCreation(RTMP_URL, RTMP_KEY)
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith(RTMP_KIND_ID, null, RTMP_PROPERTIES)
    })

    it('should not create RTMP quiddity if the QuiddityStore is not initialized', async () => {
      quiddityStore.setInitState(NOT_INITIALIZED)
      await rtmpStore.applyRtmpQuiddityCreation(RTMP_URL, RTMP_KEY)
      expect(quiddityStore.applyQuiddityCreation).not.toHaveBeenCalled()
    })

    it('should log an error if a request fails', async () => {
      quiddityStore.applyQuiddityCreation = jest.fn().mockRejectedValue(false)
      await rtmpStore.applyRtmpQuiddityCreation(RTMP_URL, RTMP_KEY)
      expect(quiddityStore.applyQuiddityCreation).toHaveBeenCalledWith(RTMP_KIND_ID, null, RTMP_PROPERTIES)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('setRtmpRequestFlag', () => {
    it('should set isRtmpRequested flag with provided status', () => {
      rtmpStore.setRtmpRequestFlag(true)
      expect(rtmpStore.isRtmpRequested).toEqual(true)
      rtmpStore.setRtmpRequestFlag(false)
      expect(rtmpStore.isRtmpRequested).toEqual(false)
    })
  })
})
