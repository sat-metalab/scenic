/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'

import populateStores from '@stores/populateStores.js'
import { LOG } from '@stores/quiddity/KindStore'

import Kind from '@models/quiddity/Kind'
import InitStateEnum from '@models/common/InitStateEnum'

import quiddityKinds, { jsonKinds } from '@fixture/allQuiddityKinds'

const { INITIALIZED, NOT_INITIALIZED } = InitStateEnum

describe('KindStore', () => {
  let socket, socketStore, kindStore

  function setupKinds () {
    for (const quidKind of quiddityKinds) {
      kindStore.addKind(quidKind)
    }
  }

  beforeEach(() => {
    ({ socketStore, kindStore } = populateStores())

    LOG.error = jest.fn()
    socket = new Socket()
    socketStore.setActiveSocket(socket)
  })

  describe('initialize', () => {
    beforeEach(() => {
      kindStore.setInitState(NOT_INITIALIZED)
      kindStore.initializeKinds = jest.fn()
    })

    it('should not initialize kinds if it is already initialized', () => {
      kindStore.setInitState(INITIALIZED)
      kindStore.initialize()
      expect(kindStore.initializeKinds).not.toHaveBeenCalled()
    })

    it('should not initialize kinds if some kinds are already stored', () => {
      setupKinds()
      kindStore.initialize()
      expect(kindStore.initializeKinds).not.toHaveBeenCalled()
    })

    it('should force kinds initialization if reloadClasses is true', () => {
      setupKinds()
      kindStore.initialize(true)
      expect(kindStore.initializeKinds).toHaveBeenCalled()
    })

    it('should initialize kinds if the kinds map is empty', () => {
      kindStore.initialize()
      expect(kindStore.initializeKinds).toHaveBeenCalled()
    })
  })

  describe('initializeKinds', () => {
    beforeEach(() => {
      kindStore.fetchKinds = jest.fn().mockResolvedValue(jsonKinds)
      kindStore.makeKindModel = jest.fn()
      LOG.warn = jest.fn()
    })

    it('should initialize all kinds provided by the back-end', async () => {
      await kindStore.initializeKinds()
      expect(kindStore.fetchKinds).toHaveBeenCalled()
      expect(kindStore.makeKindModel).toHaveBeenCalledTimes(jsonKinds.length)
    })

    it('should skip all malformed kinds', async () => {
      kindStore.fetchKinds = jest.fn().mockResolvedValue([{ id: 'test' }])
      await kindStore.initializeKinds()
      expect(LOG.warn).toHaveBeenCalledWith(expect.objectContaining({ kindId: 'test' }))
    })
  })

  describe('fetchKinds', () => {
    let quiddityAPI

    const availableQuids = [
      new Kind('quid1', 'test', 'category'),
      new Kind('quid2', 'test', 'category')
    ]

    beforeEach(() => {
      ({ quiddityAPI } = socketStore.APIs)
    })

    it('should return all quiddities kinds as an array', async () => {
      const kinds = availableQuids.map(c => c.toJSON())
      quiddityAPI.listKinds = jest.fn().mockResolvedValue({ kinds: kinds })
      expect(await kindStore.fetchKinds()).toEqual(kinds)
    })

    it('should fail if the fetch is rejected', async () => {
      quiddityAPI.listKinds = jest.fn().mockRejectedValue(false)
      await kindStore.fetchKinds()

      expect(LOG.error).toHaveBeenCalledWith(
        expect.objectContaining({ msg: expect.stringMatching('resource kinds') })
      )
    })
  })

  describe('handleSocketChange', () => {
    beforeEach(() => {
      kindStore.clear = jest.fn()
    })

    it('should clear the kinds each time the socket is handled', () => {
      kindStore.handleSocketChange()
      expect(kindStore.clear).toHaveBeenCalled()
    })
  })

  describe('makeKindModel', () => {
    beforeEach(() => {
      LOG.error = jest.fn()
    })

    it('should makes a Kind from its JSON description', () => {
      expect(kindStore.makeKindModel(jsonKinds[0])).toEqual(Kind.fromJSON(jsonKinds[0]))
    })

    it('should log an error if the JSON parsing failed', () => {
      kindStore.makeKindModel({ test: 'test' })
      expect(LOG.error).toHaveBeenCalledWith(expect.objectContaining({ msg: expect.stringMatching(/bad format/) }))
    })
  })

  describe('isFollower', () => {
    beforeEach(() => {
      setupKinds()
    })

    it('should returns true if the kind represents a reading quiddity', () => {
      expect(kindStore.isFollower('OSCsink')).toEqual(true)
    })

    it('should returns false if the kind doesn\'t represent a reading quiddity', () => {
      expect(kindStore.isFollower('OSCsrc')).toEqual(false)
    })
  })

  describe('isWriter', () => {
    beforeEach(() => {
      setupKinds()
    })

    it('should returns true if the kind represents a writing quiddity', () => {
      expect(kindStore.isWriter('OSCsrc')).toEqual(true)
    })

    it('should returns false if the kind doesn\'t represent a writing quiddity', () => {
      expect(kindStore.isWriter('OSCsink')).toEqual(false)
    })
  })
})
