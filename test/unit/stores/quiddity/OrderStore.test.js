/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'
import { configure } from 'mobx'

import OrderStore, { LOG, DEFAULT_ORDER, ORDER_PROPERTY_PATHS } from '@stores/quiddity/OrderStore'
import populateStores from '@stores/populateStores.js'

import Kind from '@models/quiddity/Kind'
import Quiddity from '@models/quiddity/Quiddity'
import ShmdataRoleEnum from '@models/shmdata/ShmdataRoleEnum'
import QuiddityTagEnum from '@models/quiddity/QuiddityTagEnum'
import OrderEnum from '@models/common/OrderEnum'

configure({ safeDescriptors: false })

describe('OrderStore', () => {
  const NEW_ORDER = DEFAULT_ORDER + 1

  const SOURCE_ID = 'source'
  const SOURCE_QUIDDITY = new Quiddity(SOURCE_ID, 'source0', SOURCE_ID)
  const SOURCE_KIND = new Kind(
    SOURCE_ID,
    SOURCE_ID,
    SOURCE_ID,
    [ShmdataRoleEnum.WRITER]
  )

  const DESTINATION_ID = 'destination'

  const UNKNOWN_ID = 'unknown'
  const UNKNOWN_KIND = new Kind(UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID)
  const UNKNOWN_QUIDDITY = new Quiddity(UNKNOWN_ID, 'unknown0', UNKNOWN_ID)

  const POSSIBLE_PATHS = [
    'order.source',
    'order.destination',
    'order.destination:ndi',
    'order.destination:rtmp'
  ]

  let socket, socketStore, quiddityStore, orderStore, kindStore
  let userTreeAPI

  beforeEach(() => {
    ({ socketStore, quiddityStore, orderStore, kindStore } = populateStores())

    LOG.error = jest.fn()
    LOG.warn = jest.fn()

    socket = new Socket()
    socketStore.setActiveSocket(socket)
  })

  describe('constructor', () => {
    beforeEach(() => {
      orderStore.handleSocketChange = jest.fn()
      orderStore.handleQuiddityCreation = jest.fn()
    })

    it('should fail if the QuiddityStore is not given', () => {
      expect(() => new OrderStore(socketStore)).toThrow()
    })

    it('should be correctly instantiated', () => {
      expect(orderStore).toBeDefined()
    })

    it('should react to the SocketStore\'s active socket change', () => {
      socketStore.activeSocket = new Socket()
      expect(orderStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })

    it('should react to a new Matrix Id', () => {
      kindStore.addKind(SOURCE_KIND)
      quiddityStore.addQuiddity(SOURCE_QUIDDITY)
      expect(orderStore.handleQuiddityCreation).toHaveBeenCalled()
    })
  })

  describe('handleSocketChange', () => {
    beforeEach(() => {
      ({ userTreeAPI } = socketStore.APIs)
      orderStore.handleGraftedOrder = jest.fn()
      orderStore.clear = jest.fn()
    })

    it('should define the APIs if there is a new socket', () => {
      const onGraftSpy = jest.spyOn(userTreeAPI, 'onGrafted')
      orderStore.handleSocketChange(socket)
      expect(orderStore.clear).toHaveBeenCalled()

      expect(onGraftSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
    })

    it('should clear the store if the new socket is null', () => {
      orderStore.handleSocketChange(null)
      expect(orderStore.clear).toHaveBeenCalled()
    })

    it('should handle updated order', () => {
      jest.spyOn(quiddityStore, 'userQuiddityIds', 'get').mockReturnValue([SOURCE_ID])
      socket.onEmit('user_tree_grafted', SOURCE_ID, POSSIBLE_PATHS[0], 'test')
      expect(orderStore.handleGraftedOrder).toHaveBeenCalledWith(SOURCE_ID, POSSIBLE_PATHS[0], 'test')
    })

    it('should not handle userTree that is not an order', () => {
      kindStore.addKind(SOURCE_KIND)
      quiddityStore.addQuiddity(SOURCE_QUIDDITY)
      socket.onEmit('user_tree_grafted', SOURCE_ID, 'property.unkwown')
      expect(orderStore.handleGraftedOrder).not.toHaveBeenCalled()
    })

    it('should not handle quiddity that is not in the matrix', () => {
      kindStore.addKind(UNKNOWN_KIND)
      quiddityStore.addQuiddity(UNKNOWN_QUIDDITY)
      socket.onEmit('user_tree_grafted', UNKNOWN_ID, ORDER_PROPERTY_PATHS[0])
      expect(orderStore.handleGraftedOrder).not.toHaveBeenCalled()
    })
  })

  describe('handleGraftedOrder', () => {
    beforeEach(() => {
      orderStore.setOrder = jest.fn()
    })

    it('should set the updated order', () => {
      orderStore.handleGraftedOrder(SOURCE_ID, POSSIBLE_PATHS[0], DEFAULT_ORDER)
      expect(orderStore.setOrder).toHaveBeenCalledWith(QuiddityTagEnum.SOURCE, SOURCE_ID, DEFAULT_ORDER)
    })

    it('shouldn\'t set an order without a valid tag', () => {
      orderStore.handleGraftedOrder(SOURCE_ID, 'test', DEFAULT_ORDER)
      expect(orderStore.setOrder).not.toHaveBeenCalledWith(DEFAULT_ORDER)
    })

    it('shouldn\'t set an invalid order', () => {
      orderStore.handleGraftedOrder(SOURCE_ID, POSSIBLE_PATHS[0], 'test')
      expect(orderStore.setOrder).not.toHaveBeenCalledWith(DEFAULT_ORDER)
    })

    it('shouldn\'t set an already stored order', () => {
      orderStore.isUpdatedOrder = jest.fn().mockReturnValue(false)
      orderStore.handleGraftedOrder(SOURCE_ID, POSSIBLE_PATHS[0], DEFAULT_ORDER)
      expect(orderStore.setOrder).not.toHaveBeenCalledWith(DEFAULT_ORDER)
    })
  })

  describe('handleQuiddityCreation', () => {
    beforeEach(() => {
      orderStore.populateOrders = jest.fn()
      orderStore.cleanOrders = jest.fn()
    })

    it('should populate all orders from the source quiddities', () => {
      orderStore.handleQuiddityCreation()
      expect(orderStore.populateOrders).not.toHaveBeenCalled()
      jest.spyOn(quiddityStore, 'sourceIds', 'get').mockReturnValue([SOURCE_ID])
      jest.spyOn(quiddityStore, 'sources', 'get').mockReturnValue([SOURCE_QUIDDITY])
      orderStore.handleQuiddityCreation()
      expect(orderStore.populateOrders).toHaveBeenCalledWith(QuiddityTagEnum.SOURCE, [SOURCE_ID])
    })

    it('should populate all orders from the destination quiddities', () => {
      orderStore.handleQuiddityCreation()
      expect(orderStore.populateOrders).not.toHaveBeenCalled()
      jest.spyOn(quiddityStore, 'destinationIds', 'get').mockReturnValue([DESTINATION_ID])
      orderStore.handleQuiddityCreation()
      expect(orderStore.populateOrders).toHaveBeenCalledWith(QuiddityTagEnum.DESTINATION, [DESTINATION_ID])
    })

    it('should clean all orders linked with removed quiddities', () => {
      orderStore.handleQuiddityCreation()
      expect(orderStore.cleanOrders).not.toHaveBeenCalled()
      orderStore.setOrder(QuiddityTagEnum.SOURCE, SOURCE_ID, DEFAULT_ORDER)
      orderStore.handleQuiddityCreation()
      expect(orderStore.cleanOrders).toHaveBeenCalledWith(SOURCE_ID)
    })
  })

  describe('populateOrders', () => {
    const QUIDDITY_IDS = [SOURCE_ID, DESTINATION_ID]

    beforeEach(() => {
      orderStore.updateOrder = jest.fn()
    })

    it('should create order from the quiddity order', async () => {
      await orderStore.populateOrders('test', QUIDDITY_IDS)
      expect(orderStore.updateOrder).toHaveBeenNthCalledWith(1, 'test', SOURCE_ID, QUIDDITY_IDS.indexOf(SOURCE_ID))
      expect(orderStore.updateOrder).toHaveBeenNthCalledWith(2, 'test', DESTINATION_ID, QUIDDITY_IDS.indexOf(DESTINATION_ID))
    })
  })

  describe('updateOrder', () => {
    async function updateSourceOrder (quidId, order) {
      await orderStore.updateOrder(QuiddityTagEnum.SOURCE, quidId, order)
    }

    beforeEach(() => {
      orderStore.applyOrderChange = jest.fn()
      orderStore.setOrder = jest.fn()
      orderStore.isUpdatedOrder = jest.fn().mockReturnValue(true)
    })

    it('should fetch the current order of the quiddity', async () => {
      orderStore.fetchOrder = jest.fn().mockResolvedValue(null)
      await updateSourceOrder(SOURCE_ID, DEFAULT_ORDER)
      expect(orderStore.fetchOrder).toHaveBeenCalledWith(QuiddityTagEnum.SOURCE, SOURCE_ID)
    })

    it('should set the order fetched on the server', async () => {
      orderStore.fetchOrder = jest.fn().mockResolvedValue(NEW_ORDER)
      await updateSourceOrder(SOURCE_ID, DEFAULT_ORDER)
      expect(orderStore.setOrder).toHaveBeenCalledWith(QuiddityTagEnum.SOURCE, SOURCE_ID, NEW_ORDER)
    })

    it('should apply the new order if it is not saved on the server', async () => {
      orderStore.fetchOrder = jest.fn().mockResolvedValue(null)
      await updateSourceOrder(SOURCE_ID, NEW_ORDER)
      expect(orderStore.applyOrderChange).toHaveBeenCalledWith(QuiddityTagEnum.SOURCE, SOURCE_ID, NEW_ORDER)
    })

    it('should do nothing if the fetched order and the stored order are the same', async () => {
      orderStore.fetchOrder = jest.fn().mockResolvedValue(NEW_ORDER)
      orderStore.isUpdatedOrder = jest.fn().mockReturnValue(false)
      await updateSourceOrder(SOURCE_ID, NEW_ORDER)
      expect(orderStore.setOrder).not.toHaveBeenCalled()
      expect(orderStore.applyOrderChange).not.toHaveBeenCalled()
    })
  })
  describe('updateDestinationOrder', () => {
    beforeEach(() => {
      orderStore.applyOrderChange = jest.fn()
    })

    it('should request an order change for a destination with changed order', async () => {
      orderStore.destinationOrders.set(DESTINATION_ID, 0)
      orderStore.destinationOrders.set(UNKNOWN_ID, 3)
      await orderStore.updateDestinationOrder(DESTINATION_ID, 1, OrderEnum.RIGHT)
      expect(orderStore.applyOrderChange).toHaveBeenCalledWith(QuiddityTagEnum.DESTINATION, DESTINATION_ID, 1)
    })

    it('should request an order change for two destinations with the same order', async () => {
      orderStore.destinationOrders.set(DESTINATION_ID, 0)
      orderStore.destinationOrders.set(UNKNOWN_ID, 1)
      await orderStore.updateDestinationOrder(DESTINATION_ID, 1, OrderEnum.RIGHT)
      expect(orderStore.applyOrderChange).toHaveBeenCalledWith(QuiddityTagEnum.DESTINATION, DESTINATION_ID, 1)
      expect(orderStore.applyOrderChange).toHaveBeenCalledWith(QuiddityTagEnum.DESTINATION, UNKNOWN_ID, 0)
    })
  })

  describe('updateSourceOrder', () => {
    beforeEach(() => {
      orderStore.applyOrderChange = jest.fn()
    })

    it('should request an order change for a source with changed order', async () => {
      orderStore.sourceOrders.set(SOURCE_ID, 0)
      orderStore.sourceOrders.set(UNKNOWN_ID, 3)
      await orderStore.updateSourceOrder(SOURCE_ID, 1, OrderEnum.DOWN)
      expect(orderStore.applyOrderChange).toHaveBeenCalledWith(QuiddityTagEnum.SOURCE, SOURCE_ID, 1)
    })

    it('should request an order change for two sources with the same order', async () => {
      orderStore.sourceOrders.set(SOURCE_ID, 0)
      orderStore.sourceOrders.set(UNKNOWN_ID, 1)
      await orderStore.updateSourceOrder(SOURCE_ID, 1, OrderEnum.DOWN)
      expect(orderStore.applyOrderChange).toHaveBeenCalledWith(QuiddityTagEnum.SOURCE, SOURCE_ID, 1)
      expect(orderStore.applyOrderChange).toHaveBeenCalledWith(QuiddityTagEnum.SOURCE, UNKNOWN_ID, 0)
    })
  })

  describe('cleanOrders', () => {
    beforeEach(() => {
      orderStore.setOrder(QuiddityTagEnum.SOURCE, SOURCE_ID, NEW_ORDER)
      orderStore.setOrder(QuiddityTagEnum.DESTINATION, DESTINATION_ID, NEW_ORDER)
    })

    it('should delete the orders that are not used by the source of the matrix', () => {
      expect(orderStore.sourceOrders.has(SOURCE_ID)).toEqual(true)
      orderStore.cleanOrders(SOURCE_ID)
      expect(orderStore.sourceOrders.has(SOURCE_ID)).toEqual(false)
    })

    it('should not delete the orders that are used by a source in the matrix', () => {
      expect(orderStore.sourceOrders.has(SOURCE_ID)).toEqual(true)
      jest.spyOn(quiddityStore, 'userQuiddityIds', 'get').mockReturnValue([SOURCE_ID])
      orderStore.cleanOrders(SOURCE_ID)
      expect(orderStore.sourceOrders.has(SOURCE_ID)).toEqual(true)
    })

    it('should delete the orders that are not used by the destination of the matrix', () => {
      expect(orderStore.destinationOrders.has(DESTINATION_ID)).toEqual(true)
      orderStore.cleanOrders(DESTINATION_ID)
      expect(orderStore.destinationOrders.has(DESTINATION_ID)).toEqual(false)
    })

    it('should not delete the orders that are used by a destination in the matrix', () => {
      expect(orderStore.destinationOrders.has(DESTINATION_ID)).toEqual(true)
      jest.spyOn(quiddityStore, 'userQuiddityIds', 'get').mockReturnValue([DESTINATION_ID])
      orderStore.cleanOrders(DESTINATION_ID)
      expect(orderStore.destinationOrders.has(DESTINATION_ID)).toEqual(true)
    })
  })

  describe('fetchOrder', () => {
    async function fetchSourceOrder (quidId) {
      return await orderStore.fetchOrder(QuiddityTagEnum.SOURCE, quidId)
    }

    beforeEach(() => {
      ({ userTreeAPI } = socketStore.APIs)
      userTreeAPI.get = jest.fn().mockResolvedValue(NEW_ORDER)
    })

    it('should fetch and return the order', async () => {
      expect(await fetchSourceOrder(SOURCE_ID)).toEqual(NEW_ORDER)
      expect(userTreeAPI.get).toHaveBeenCalledWith(SOURCE_ID, 'order.source')
      expect(LOG.warn).not.toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should return null if no order is saved on the server', async () => {
      userTreeAPI.get = jest.fn().mockResolvedValue(null)
      expect(await fetchSourceOrder(SOURCE_ID)).toEqual(null)
      expect(LOG.warn).toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should return null and log an error if the fetched order is not an integer', async () => {
      userTreeAPI.get = jest.fn().mockResolvedValue('string')
      expect(await fetchSourceOrder(SOURCE_ID)).toEqual(null)
      expect(LOG.warn).toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if the request fails', async () => {
      userTreeAPI.get = jest.fn().mockRejectedValue(false)
      expect(await fetchSourceOrder(SOURCE_ID)).toEqual(null)
      expect(LOG.warn).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyOrderChange', () => {
    beforeEach(() => {
      ({ userTreeAPI } = socketStore.APIs)
      orderStore.handleQuiddityCreation = jest.fn()
      userTreeAPI.graft = jest.fn().mockResolvedValue(true)
      quiddityStore.findQuiddityType = jest.fn().mockReturnValue(QuiddityTagEnum.SOURCE)
    })

    it('should apply an order', async () => {
      await orderStore.applyOrderChange(QuiddityTagEnum.SOURCE, SOURCE_ID, NEW_ORDER)
      expect(userTreeAPI.graft).toHaveBeenCalledWith(SOURCE_ID, 'order.source', NEW_ORDER)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if the quiddity tag is not compatible with the matrix', async () => {
      quiddityStore.findQuiddityType = jest.fn().mockReturnValue(null)
      await orderStore.applyOrderChange('failure', UNKNOWN_ID, NEW_ORDER)
      expect(userTreeAPI.graft).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if the request fails', async () => {
      userTreeAPI.graft = jest.fn().mockRejectedValue(false)
      await orderStore.applyOrderChange(QuiddityTagEnum.SOURCE, SOURCE_ID, NEW_ORDER)
      expect(userTreeAPI.graft).toHaveBeenCalledWith(SOURCE_ID, 'order.source', NEW_ORDER)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('isValidOrder', () => {
    it('should validate a number', () => {
      expect(orderStore.isValidOrder(1)).toEqual(true)
    })

    it('should validate a string number', () => {
      expect(orderStore.isValidOrder('1')).toEqual(true)
    })

    it('shouldn\'t validate a string', () => {
      expect(orderStore.isValidOrder('test')).toEqual(false)
    })

    it('shouldn\'t validate a falsy value', () => {
      expect(orderStore.isValidOrder(null)).toEqual(false)
    })
  })

  describe('isUpdatedOrder', () => {
    it('should return true if the source order is not stored', () => {
      expect(orderStore.isUpdatedOrder(QuiddityTagEnum.SOURCE, SOURCE_ID, DEFAULT_ORDER)).toEqual(true)
    })

    it('should return true if the source order is updated', () => {
      orderStore.setOrder(QuiddityTagEnum.SOURCE, SOURCE_ID, DEFAULT_ORDER)
      expect(orderStore.isUpdatedOrder(QuiddityTagEnum.SOURCE, SOURCE_ID, NEW_ORDER)).toEqual(true)
    })

    it('should return false if the source order isn\'t updated', () => {
      orderStore.setOrder(QuiddityTagEnum.SOURCE, SOURCE_ID, DEFAULT_ORDER)
      expect(orderStore.isUpdatedOrder(QuiddityTagEnum.SOURCE, SOURCE_ID, DEFAULT_ORDER)).toEqual(false)
    })

    it('should return true if the destination order is not stored', () => {
      expect(orderStore.isUpdatedOrder(QuiddityTagEnum.DESTINATION, DESTINATION_ID, DEFAULT_ORDER)).toEqual(true)
    })

    it('should return true if the destination order is updated', () => {
      orderStore.setOrder(QuiddityTagEnum.DESTINATION, DESTINATION_ID, DEFAULT_ORDER)
      expect(orderStore.isUpdatedOrder(QuiddityTagEnum.DESTINATION, DESTINATION_ID, NEW_ORDER)).toEqual(true)
    })

    it('should return false if the source order isn\'t updated', () => {
      orderStore.setOrder(QuiddityTagEnum.DESTINATION, DESTINATION_ID, DEFAULT_ORDER)
      expect(orderStore.isUpdatedOrder(QuiddityTagEnum.DESTINATION, DESTINATION_ID, DEFAULT_ORDER)).toEqual(false)
    })
  })

  describe('hasOrder', () => {
    it('should return true if the source order is stored', () => {
      orderStore.setOrder(QuiddityTagEnum.SOURCE, SOURCE_ID, DEFAULT_ORDER)
      expect(orderStore.hasOrder(SOURCE_ID, QuiddityTagEnum.SOURCE)).toEqual(true)
    })

    it('should return true if the destination order is stored', () => {
      orderStore.setOrder(QuiddityTagEnum.DESTINATION, DESTINATION_ID, DEFAULT_ORDER)
      expect(orderStore.hasOrder(DESTINATION_ID, QuiddityTagEnum.DESTINATION)).toEqual(true)
    })

    it('should return true if the quiddity has a stored order', () => {
      orderStore.setOrder(QuiddityTagEnum.DESTINATION, DESTINATION_ID, DEFAULT_ORDER)
      expect(orderStore.hasOrder(DESTINATION_ID)).toEqual(true)
    })
  })

  describe('resetDestinationOrder', () => {
    beforeEach(() => {
      orderStore.applyOrderChange = jest.fn()

      orderStore.destinationOrders.set(DESTINATION_ID, DEFAULT_ORDER)
      orderStore.destinationOrders.set(UNKNOWN_ID, 1)
    })

    it('should be called when we remove the order of a destination quiddity', () => {
      orderStore.resetDestinationOrders = jest.fn()
      orderStore.resetSourceOrders = jest.fn()
      orderStore.removeOrder(DESTINATION_ID)
      expect(orderStore.resetDestinationOrders).toHaveBeenCalled()
      expect(orderStore.resetSourceOrders).not.toHaveBeenCalled()
    })

    it('should reset the destination order of all destinations when a destination is removed', () => {
      orderStore.removeOrder(UNKNOWN_ID)
      orderStore.resetDestinationOrders()
      expect(orderStore.applyOrderChange).toHaveBeenCalledWith(QuiddityTagEnum.DESTINATION, DESTINATION_ID, DEFAULT_ORDER)
      expect(orderStore.applyOrderChange).not.toHaveBeenCalledWith(QuiddityTagEnum.DESTINATION, UNKNOWN_ID, 1)
    })
  })

  describe('resetSourceOrder', () => {
    beforeEach(() => {
      orderStore.applyOrderChange = jest.fn()

      orderStore.sourceOrders.set(SOURCE_ID, DEFAULT_ORDER)
      orderStore.sourceOrders.set(UNKNOWN_ID, 1)
    })

    it('should be called when we remove the order of a source quiddity', () => {
      orderStore.resetSourceOrders = jest.fn()
      orderStore.resetDestinationOrders = jest.fn()
      orderStore.removeOrder(SOURCE_ID)
      expect(orderStore.resetSourceOrders).toHaveBeenCalled()
      expect(orderStore.resetDestinationOrders).not.toHaveBeenCalled()
    })

    it('should reset the source order of all sources when a source is removed', () => {
      orderStore.removeOrder(UNKNOWN_ID)
      orderStore.resetSourceOrders()
      expect(orderStore.applyOrderChange).toHaveBeenCalledWith(QuiddityTagEnum.SOURCE, SOURCE_ID, DEFAULT_ORDER)
      expect(orderStore.applyOrderChange).not.toHaveBeenCalledWith(QuiddityTagEnum.SOURCE, UNKNOWN_ID, 1)
    })
  })
})
