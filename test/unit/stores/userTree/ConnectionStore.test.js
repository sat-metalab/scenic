/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'
import { configure } from 'mobx'

import Scene from '@models/userTree/Scene'
import Connection from '@models/userTree/Connection'
import InitStateEnum from '@models/common/InitStateEnum'
import SDI_INPUT_SHMDATA from '@fixture/models/shmdatas.sdiInput1'
import VIDEO_OUTPUT_SHMDATA from '@fixture/models/shmdatas.videoOutput'

import { USER_TREE_ID } from '@stores/userTree/UserTreeStore'

import populateStores from '@stores/populateStores.js'
import ConnectionStore, { LOG } from '@stores/userTree/ConnectionStore'
// import MatrixEntry from '@models/matrix/MatrixEntry'

import quiddityKinds, { getKind } from '@fixture/allQuiddityKinds'
import { sdiInput, videoOutput, rtmpOutput, encoderH264 } from '@fixture/allQuiddities'
import { video, addShmdataToInfoTree } from '@fixture/allShmdatas'

import USER_TREE_REV1 from '@fixture/models/userTree.revision1.json'

configure({ safeDescriptors: false })

// @todo: Update todo tests when `applyShmdataConnection` function is replaced
describe('ConnectionStore', () => {
  const AVAILABLE_CONNECTIONS = Connection.fromUserTree(USER_TREE_REV1)
  const AVAILABLE_SCENES = Scene.fromUserTree(USER_TREE_REV1)

  let socket, socketStore, configStore
  let shmdataStore, maxLimitStore
  let quiddityStore, lockStore, kindStore, userTreeStore
  let sceneStore, connectionStore

  const SCENE = AVAILABLE_SCENES[0]
  const CONNECTION = AVAILABLE_CONNECTIONS[0]
  const connection = new Connection(sdiInput.id, videoOutput.id, video.mediaType)

  const CONNECTION_DATA = AVAILABLE_CONNECTIONS[0].toJSON()

  const CONNECTED_SDI_INPUT = addShmdataToInfoTree(sdiInput, SDI_INPUT_SHMDATA)
  const CONNECTED_VIDEO_OUTPUT = addShmdataToInfoTree(videoOutput, VIDEO_OUTPUT_SHMDATA)

  beforeEach(() => {
    ({ socketStore, configStore, quiddityStore, shmdataStore, sceneStore, maxLimitStore, lockStore, kindStore, userTreeStore, connectionStore } = populateStores())

    LOG.error = jest.fn()
    LOG.warn = jest.fn()
    LOG.info = jest.fn()
    LOG.debug = jest.fn()

    socket = new Socket()
    socketStore.setActiveSocket(socket)

    jest.spyOn(userTreeStore, 'userTreeId', 'get').mockReturnValue(USER_TREE_ID)
  })

  function activateConnection (connection, scene) {
    scene.connections = [connection.id]
    scene.active = true
    sceneStore.addUserScene(scene)
  }

  describe('constructor', () => {
    beforeEach(() => {
      connectionStore.handleSocketChange = jest.fn()
      connectionStore.handleSceneStoreInitialization = jest.fn()
    })

    it('should fail if the socketStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new ConnectionStore() }).toThrow()
      /* eslint-enable no-new */
    })

    it('should fail if the configStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new ConnectionStore(socketStore) }).toThrow()
      /* eslint-enable no-new */
    })

    it('should fail if the quiddityStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new ConnectionStore(socketStore, configStore) }).toThrow()
      /* eslint-enable no-new */
    })

    it('should fail if the shmdataStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new ConnectionStore(socketStore, configStore, quiddityStore) }).toThrow()
      /* eslint-enable no-new */
    })

    it('should fail if the sceneStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new ConnectionStore(socketStore, configStore, quiddityStore, shmdataStore) }).toThrow()
      /* eslint-enable no-new */
    })

    it('should fail if the maxLimitStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new ConnectionStore(socketStore, configStore, quiddityStore, shmdataStore, sceneStore) }).toThrow()
      /* eslint-enable no-new */
    })

    it('should fail if the lockStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new ConnectionStore(socketStore, configStore, quiddityStore, shmdataStore, sceneStore, maxLimitStore) }).toThrow()
      /* eslint-enable no-new */
    })

    it('should not fail if all stores are provided', () => {
      /* eslint-disable no-new */
      expect(() => { new ConnectionStore(socketStore, configStore, quiddityStore, shmdataStore, sceneStore, maxLimitStore, lockStore) }).not.toThrow()
      /* eslint-enable no-new */
    })

    it('should be correctly instantiated', () => {
      expect(connectionStore).toBeDefined()
      expect(connectionStore.initState).toEqual(InitStateEnum.NOT_INITIALIZED)
    })

    it('should react to the SocketStore\'s active socket change', () => {
      socketStore.activeSocket = new Socket()
      expect(connectionStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })

    it('should react to changes to the SceneStore\'s initialization state', () => {
      sceneStore.setInitState(InitStateEnum.INITIALIZED)
      expect(connectionStore.handleSceneStoreInitialization).toHaveBeenCalledWith(InitStateEnum.INITIALIZED)
      sceneStore.setInitState(InitStateEnum.NOT_INITIALIZED)
      expect(connectionStore.handleSceneStoreInitialization).toHaveBeenCalledWith(InitStateEnum.NOT_INITIALIZED)
    })
  })

  describe('initialize', () => {
    beforeEach(() => {
      connectionStore.fetchUserTree = jest.fn().mockResolvedValue(USER_TREE_REV1)
      connectionStore.populateConnectionsFromUserTree = jest.fn().mockReturnValue(true)
      connectionStore.fallbackUserConnectionsInitialization = jest.fn().mockResolvedValue(true)
    })

    it('should initialize all connections if they exist', async () => {
      expect(await connectionStore.initialize()).toEqual(true)
      expect(connectionStore.fetchUserTree).toHaveBeenCalled()
      expect(connectionStore.populateConnectionsFromUserTree).toHaveBeenCalledWith(USER_TREE_REV1)
      expect(connectionStore.fallbackUserConnectionsInitialization).not.toHaveBeenCalled()
      expect(connectionStore.initState).toEqual(InitStateEnum.INITIALIZED)
    })

    it('should not initialize if store is already initialized', async () => {
      connectionStore.setInitState(InitStateEnum.INITIALIZED)
      connectionStore.setInitState = jest.fn()

      expect(await connectionStore.initialize()).toEqual(false)
      expect(connectionStore.fetchUserTree).not.toHaveBeenCalled()
      expect(connectionStore.populateConnectionsFromUserTree).not.toHaveBeenCalled()
      expect(connectionStore.fallbackUserConnectionsInitialization).not.toHaveBeenCalled()
      expect(connectionStore.setInitState).not.toHaveBeenCalled()
    })

    it('should initialize with default scenes if fetched json is invalid', async () => {
      connectionStore.fetchUserTree = jest.fn().mockResolvedValue({})
      connectionStore.populateConnectionsFromUserTree = jest.fn().mockReturnValue(false)

      expect(await connectionStore.initialize()).toEqual(true)
      expect(connectionStore.populateConnectionsFromUserTree).toHaveBeenCalledWith({})
      expect(connectionStore.fallbackUserConnectionsInitialization).toHaveBeenCalled()
      expect(connectionStore.initState).toEqual(InitStateEnum.INITIALIZED)
    })
  })

  describe('handleSocketChange', () => {
    let userTreeAPI

    beforeEach(() => {
      ({ userTreeAPI } = socketStore.APIs)

      connectionStore.handleGraftedUserTreeConnections = jest.fn()
      connectionStore.handlePrunedUserTreeConnections = jest.fn()
      connectionStore.handleRemovedQuiddity = jest.fn()
      connectionStore.handleLoadedFile = jest.fn()
      connectionStore.clear = jest.fn()
    })

    it('should define the APIs if there is a new socket', () => {
      const onGraftSpy = jest.spyOn(userTreeAPI, 'onGrafted')
      const onPruneSpy = jest.spyOn(userTreeAPI, 'onPruned')
      const newSocket = new Socket()

      connectionStore.userTreeAPI = null

      connectionStore.handleSocketChange(newSocket)

      expect(connectionStore.clear).toHaveBeenCalled()

      expect(onGraftSpy).toHaveBeenNthCalledWith(
        1,
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )

      expect(onPruneSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
    })

    it('should clear the store if the new socket is null', () => {
      connectionStore.handleSocketChange(null)
      expect(connectionStore.clear).toHaveBeenCalled()
    })

    it('should handle grafted connection tree', () => {
      socket.onEmit('user_tree_grafted', USER_TREE_ID, '.connections.test', CONNECTION_DATA)
      expect(connectionStore.handleGraftedUserTreeConnections).toHaveBeenCalledWith(USER_TREE_ID, '.connections.test', CONNECTION_DATA)
    })

    it('should handle pruned connection tree', () => {
      socket.onEmit('user_tree_pruned', USER_TREE_ID, '.connections.test')
      expect(connectionStore.handlePrunedUserTreeConnections).toHaveBeenCalledWith(USER_TREE_ID, '.connections.test')
    })
  })

  describe('connectedShmdatas', () => {
    const SHMDATA_PATH = Object.keys(CONNECTED_SDI_INPUT.infoTree.shmdata.writer)[0]

    beforeEach(() => {
      quiddityStore.addQuiddity(CONNECTED_SDI_INPUT)
      quiddityStore.addQuiddity(CONNECTED_VIDEO_OUTPUT)
    })

    it('should return an empty map if there are no user connections', () => {
      expect(connectionStore.connectedShmdatas).toEqual(new Map())
    })

    it('should return a populated map of connected shmdatas when they exist', () => {
      connectionStore.addUserConnection(connection)
      expect(connectionStore.connectedShmdatas).toEqual(new Map().set(
        `${CONNECTED_SDI_INPUT.id}_${CONNECTED_VIDEO_OUTPUT.id}`, SHMDATA_PATH))
    })
  })

  describe('handleSceneStoreInitialization', () => {
    beforeEach(() => {
      connectionStore.initialize = jest.fn()
      connectionStore.clear = jest.fn()
    })

    it('should initialize the Store if the SceneStore gets initialized', () => {
      sceneStore.setInitState(InitStateEnum.INITIALIZED)
      expect(connectionStore.initialize).toHaveBeenCalled()
    })

    it('should clear the Store if the SceneStore gets uninitialized', () => {
      sceneStore.setInitState(InitStateEnum.INITIALIZED)
      expect(connectionStore.clear).not.toHaveBeenCalled()
      sceneStore.setInitState(InitStateEnum.NOT_INITIALIZED)
      expect(connectionStore.clear).toHaveBeenCalled()
    })
  })

  describe('handleLoadedFile', () => {
    beforeEach(() => {
      connectionStore.clear = jest.fn()
      connectionStore.initialize = jest.fn()
      connectionStore.fallbackSessionLoading = jest.fn()
    })

    it('should handle loaded file', async () => {
      connectionStore.handleLoadedFile('test')
      expect(await connectionStore.clear).toHaveBeenCalled()
      expect(await connectionStore.initialize).toHaveBeenCalled()
      expect(await connectionStore.fallbackSessionLoading).toHaveBeenCalled()
      expect(LOG.info).toHaveBeenCalled()
    })
  })

  describe('handleGraftedUserTreeConnections', () => {
    beforeEach(() => {
      connectionStore.addUserConnection = jest.fn()
    })

    it('should add user connection if the path does not include the property', () => {
      connectionStore.handleGraftedUserTreeConnections(sdiInput.id, '.connections.test', CONNECTION_DATA)
      expect(connectionStore.addUserConnection).toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should not add user connection if the path includes a property', () => {
      connectionStore.handleGraftedUserTreeConnections(sdiInput.id, '.connections.test.test', CONNECTION_DATA)
      expect(connectionStore.addUserConnection).not.toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })
  })

  describe('handlePrunedUserTreeConnections', () => {
    beforeEach(() => {
      connectionStore.removeUserConnection = jest.fn()
    })

    it('should remove user connection if the path does not include a property', () => {
      connectionStore.handlePrunedUserTreeConnections(sdiInput.id, '.connections.test')
      expect(connectionStore.removeUserConnection).toHaveBeenCalled()
      expect(LOG.debug).toHaveBeenCalled()
    })

    it('should not remove user connection if the path includes a property', () => {
      connectionStore.handlePrunedUserTreeConnections(sdiInput.id, '.connections.test.test')
      expect(connectionStore.removeUserConnection).not.toHaveBeenCalled()
      expect(LOG.debug).toHaveBeenCalled()
    })
  })

  describe('populateConnectionsFromUserTree', () => {
    beforeEach(() => {
      connectionStore.addUserConnection = jest.fn()
    })

    it('should not populate connections if user data is empty', () => {
      expect(connectionStore.populateConnectionsFromUserTree({})).toEqual(false)
      expect(connectionStore.addUserConnection).not.toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should not populate connections if "connections" key doesn\'t exists', () => {
      expect(connectionStore.populateConnectionsFromUserTree({ test: 'test' })).toEqual(false)
      expect(connectionStore.addUserConnection).not.toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should not populate connections if "connections" key is empty', () => {
      expect(connectionStore.populateConnectionsFromUserTree({ connections: {} })).toEqual(false)
      expect(connectionStore.addUserConnection).not.toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should populate connections with user data', () => {
      expect(connectionStore.populateConnectionsFromUserTree(USER_TREE_REV1)).toEqual(true)
      expect(connectionStore.addUserConnection).toHaveBeenCalledTimes(13)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if connection is malformed', () => {
      expect(connectionStore.populateConnectionsFromUserTree({ connections: { failure: 'failure' } })).toEqual(false)
      expect(connectionStore.addUserConnection).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyUserConnection', () => {
    beforeEach(() => {
      quiddityStore.applyQuiddityConnection = jest.fn()
    })

    it('should not apply quiddity connection if the connection is locked', () => {
      lockStore.isLockedConnection = jest.fn().mockReturnValue(true)
      connectionStore.applyUserConnection(connection)
      expect(quiddityStore.applyQuiddityConnection).not.toHaveBeenCalled()
      expect(LOG.warn).toHaveBeenCalled()
    })

    it('should not apply quiddity connection if the selected scene is not active', () => {
      jest.spyOn(sceneStore, 'isSelectedSceneActive', 'get').mockReturnValue(false)
      connectionStore.applyUserConnection(connection)
      expect(quiddityStore.applyQuiddityConnection).not.toHaveBeenCalled()
      expect(LOG.warn).not.toHaveBeenCalled()
    })

    it('should apply quiddity connection if the selected scene is active', () => {
      activateConnection(connection, SCENE)
      connectionStore.applyUserConnection(connection)
      expect(quiddityStore.applyQuiddityConnection).toHaveBeenCalled()
      expect(LOG.warn).not.toHaveBeenCalled()
    })
  })

  describe('applyUserDisconnection', () => {
    beforeEach(() => {
      quiddityStore.applyQuiddityDisconnection = jest.fn()
    })

    it('should not apply quiddity disconnection if the connection is locked', () => {
      lockStore.isLockedConnection = jest.fn().mockReturnValue(true)
      connectionStore.applyUserDisconnection(connection)
      expect(quiddityStore.applyQuiddityDisconnection).not.toHaveBeenCalled()
      expect(LOG.warn).toHaveBeenCalled()
    })

    it('should not apply quiddity disconnection if the selected scene is not active', () => {
      jest.spyOn(sceneStore, 'isSelectedSceneActive', 'get').mockReturnValue(false)
      connectionStore.applyUserDisconnection(connection)
      expect(quiddityStore.applyQuiddityDisconnection).not.toHaveBeenCalled()
    })
  })

  describe('clear', () => {
    beforeEach(() => {
      AVAILABLE_CONNECTIONS.forEach(c => connectionStore.addUserConnection(c))
    })

    it('should clear all user connections', () => {
      expect(connectionStore.userConnections.size).toEqual(13)
      connectionStore.clear()
      expect(connectionStore.userConnections.size).toEqual(0)
    })
  })

  describe('applyConnectionArmament', () => {
    const rtmpConnection = new Connection(sdiInput.id, rtmpOutput.id, video.mediaType)

    beforeEach(() => {
      connectionStore.applySceneConnectionArmament = jest.fn()
      connectionStore.applyUserConnectionArmament = jest.fn()

      AVAILABLE_SCENES.forEach(s => sceneStore.addUserScene(s))
      quiddityKinds.forEach(c => kindStore.addKind(c))

      kindStore.addKind(getKind(rtmpOutput))
      quiddityStore.addQuiddity(rtmpOutput)
    })

    it('should request the armament for all scenes if the destination is lockable', async () => {
      lockStore.addLockableKind(rtmpOutput.kindId)
      await connectionStore.applyConnectionArmament(rtmpConnection)
      expect(connectionStore.applySceneConnectionArmament).toHaveBeenCalledTimes(AVAILABLE_SCENES.length)
    })

    it('should request the armament for the selected scenes if the destination isn\'t lockable', async () => {
      await connectionStore.applyConnectionArmament(connection)
      expect(connectionStore.applySceneConnectionArmament).toHaveBeenCalledWith(AVAILABLE_SCENES[0], connection)
    })
  })

  describe('applySceneConnectionArmament', () => {
    const h264Connection = new Connection(sdiInput.id, encoderH264.id, video.mediaType)

    beforeEach(() => {
      quiddityStore.applyQuiddityDisconnection = jest.fn()
      connectionStore.findConcurrentSceneConnections = jest.fn().mockReturnValue(['test'])
    })

    it('should disconnect the shmdata of the last connection when the max reader limit is exceeded', async () => {
      connectionStore.isSceneMaxReaderLimitExceed = jest.fn().mockReturnValue(true)
      await connectionStore.applySceneConnectionArmament({ connections: ['test'] }, h264Connection)
      expect(quiddityStore.applyQuiddityDisconnection).toHaveBeenCalled()
    })

    it('should not disconnect the shmdata of the last connection when the max reader limit is not exceeded', async () => {
      connectionStore.isSceneMaxReaderLimitExceed = jest.fn().mockReturnValue(false)
      await connectionStore.applySceneConnectionArmament({ connections: ['test'] }, h264Connection)
      expect(quiddityStore.applyQuiddityDisconnection).not.toHaveBeenCalled()
    })
  })

  describe('handleActiveScene', () => {
    beforeEach(() => {
      SCENE.connections = [CONNECTION.id]

      sceneStore.addUserScene(SCENE)
      connectionStore.addUserConnection(CONNECTION)

      connectionStore.applyActiveSceneConnections = jest.fn()
    })

    it('should apply all the connections of the active scene', () => {
      connectionStore.handleActiveScene(SCENE.id)
      expect(connectionStore.applyActiveSceneConnections).toHaveBeenCalled()
    })

    it('should apply all the disconnections when the active scene is undefined', () => {
      connectionStore.handleActiveScene(undefined)
      expect(connectionStore.applyActiveSceneConnections).toHaveBeenCalled()
    })

    it('should not apply all the the active scene connections when the futureScene is set', () => {
      connectionStore.handleActiveScene(undefined, SCENE.id)
      expect(connectionStore.applyActiveSceneConnections).not.toHaveBeenCalled()
    })
  })

  describe('applyActiveSceneConnections', () => {
    beforeEach(() => {
      connectionStore.addUserConnection(connection)
      connectionStore.handleActiveScene = jest.fn()
      quiddityStore.applyQuiddityConnection = jest.fn()
      quiddityStore.applyQuiddityDisconnection = jest.fn()
      lockStore.isLockableConnection = jest.fn().mockReturnValue(true)
    })

    it('should not disconnect a connection with disconnected shmdatas', () => {
      jest.spyOn(connectionStore, 'connectedShmdatas', 'get').mockReturnValue(new Map().set())

      connectionStore.applyActiveSceneConnections()
      expect(quiddityStore.applyQuiddityDisconnection).not.toHaveBeenCalled()
      expect(quiddityStore.applyQuiddityConnection).not.toHaveBeenCalled()
    })

    it('should not connect a non-active connection', () => {
      connectionStore.applyActiveSceneConnections()
      expect(quiddityStore.applyQuiddityConnection).not.toHaveBeenCalled()
      expect(quiddityStore.applyQuiddityDisconnection).not.toHaveBeenCalled()
    })

    it('should disconnect a connection with connected shmdatas if it is not active and not lockable', () => {
      jest.spyOn(connectionStore, 'connectedShmdatas', 'get').mockReturnValue(new Map().set(connection.id))
      jest.spyOn(connectionStore, 'activeConnections', 'get').mockReturnValue(new Set())
      lockStore.isLockableConnection = jest.fn().mockReturnValue(false)

      connectionStore.applyActiveSceneConnections()
      expect(quiddityStore.applyQuiddityDisconnection).toHaveBeenCalled()
      expect(quiddityStore.applyQuiddityConnection).not.toHaveBeenCalled()
    })

    it('shouldn\'t connect an active connection with disconnected shmdata in a lockable connection', () => {
      jest.spyOn(connectionStore, 'connectedShmdatas', 'get').mockReturnValue(new Map().set())
      jest.spyOn(connectionStore, 'activeConnections', 'get').mockReturnValue(new Set(connection.id))

      connectionStore.applyActiveSceneConnections()
      expect(quiddityStore.applyQuiddityDisconnection).not.toHaveBeenCalled()
      expect(quiddityStore.applyQuiddityConnection).not.toHaveBeenCalled()
    })
  })

  describe('handleQuiddityChanges', () => {
    beforeEach(() => {
      connectionStore.addUserConnection(connection)

      kindStore.addKind(getKind(sdiInput))
      quiddityStore.addQuiddity(sdiInput)

      kindStore.addKind(getKind(videoOutput))
      quiddityStore.addQuiddity(videoOutput)

      quiddityStore.applyQuiddityDisconnection = jest.fn()
      connectionStore.applyConnectionDisarmament = jest.fn()
    })

    it('should be called when quiddities are changed', () => {
      connectionStore.handleQuiddityChanges = jest.fn()
      quiddityStore.removeQuiddity(sdiInput)
      expect(connectionStore.handleQuiddityChanges).toHaveBeenCalledTimes(1)
      quiddityStore.addQuiddity(sdiInput)
      expect(connectionStore.handleQuiddityChanges).toHaveBeenCalledTimes(2)
    })

    it('should remove the connection when the destination quiddity doesn\'t exist', async () => {
      quiddityStore.removeQuiddity(sdiInput)
      expect(connectionStore.applyConnectionDisarmament).toHaveBeenCalledWith(connection)
    })

    it('should remove the connection when the source quiddity doesn\'t exist', async () => {
      quiddityStore.removeQuiddity(videoOutput)
      expect(connectionStore.applyConnectionDisarmament).toHaveBeenCalledWith(connection)
    })
  })
})
