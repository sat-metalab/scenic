/* global describe it expect jest beforeEach */
import { Socket } from '@fixture/socket'

// @todo [swIO] Switch menu for debugging purposes
import DEFAULT_MENUS from '@assets/json/menus.default.json'
import DEFAULT_BUNDLES from '@assets/json/bundles.default.json'
import DEFAULT_SCENIC from '@assets/json/scenic.default.json'

import InitStateEnum from '@models/common/InitStateEnum'
import ConfigEnum from '@models/common/ConfigEnum'
import populateStores from '@stores/populateStores.js'
import ConfigStore, { LOG, SCENIC_CONFIG_PATH } from '@stores/common/ConfigStore'

import SIP_INIT from '@fixture/sipInit.sample.json'
import CONTACT_LIST from '@fixture/contactList0.sample.json'
import SCENIC_CONFIG from '@fixture/scenic0.sample.json'
// import MERGED_SCENIC_CONFIG from '@fixture/merged0.sample.json'
import MENUS_CONFIG from '@fixture/menus0.sample.json'
import MERGED_BUNDLES_CONFIG from '@fixture/bundlesMerged0.sample.json'

describe('ConfigStore', () => {
  let socketStore, configStore
  let switcherAPI

  beforeEach(() => {
    ({ socketStore, configStore } = populateStores())

    LOG.error = jest.fn()
    LOG.warn = jest.fn()

    socketStore.setActiveSocket(new Socket())
  })

  describe('constructor', () => {
    beforeEach(() => {
      configStore.handleSocketChange = jest.fn()
    })

    it('should be correctly instantiated', () => {
      configStore = new ConfigStore(socketStore) // eslint-disable-line no-new
      expect(configStore).toBeDefined()
      expect(configStore.initState).toEqual(InitStateEnum.NOT_INITIALIZED)
    })

    it('should react to the SocketStore\'s active socket change', () => {
      socketStore.activeSocket = new Socket()
      expect(configStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })
  })

  describe('initialize', () => {
    beforeEach(() => {
      configStore.fetchConfiguration = jest.fn().mockResolvedValue(null)
      configStore.isScenicSchemaValid = jest.fn().mockReturnValue(true)
      configStore.setUserScenic = jest.fn()
      configStore.isMenuSchemaValid = jest.fn().mockReturnValue(true)
      configStore.setUserMenus = jest.fn()
      configStore.isContactSchemaValid = jest.fn().mockReturnValue(true)
      configStore.setUserContacts = jest.fn()
      configStore.setUserBundles = jest.fn()
      configStore.applySendBundlesToServer = jest.fn().mockResolvedValue(true)
    })

    it('should not initialize if store is already initialized', async () => {
      configStore.setInitState(InitStateEnum.INITIALIZED)
      configStore.setInitState = jest.fn()

      expect(await configStore.initialize()).toEqual(false)
      expect(configStore.fetchConfiguration).not.toHaveBeenCalled()
      expect(configStore.isScenicSchemaValid).not.toHaveBeenCalled()
      expect(configStore.setUserScenic).not.toHaveBeenCalled()
      expect(configStore.isMenuSchemaValid).not.toHaveBeenCalled()
      expect(configStore.setUserMenus).not.toHaveBeenCalled()
      expect(configStore.isContactSchemaValid).not.toHaveBeenCalled()
      expect(configStore.setUserContacts).not.toHaveBeenCalled()
      expect(configStore.setUserBundles).not.toHaveBeenCalled()
      expect(configStore.applySendBundlesToServer).not.toHaveBeenCalled()
      expect(configStore.setInitState).not.toHaveBeenCalled()
    })

    it('should initialize the scenic configuration if the path is right', async () => {
      configStore.setInitState(InitStateEnum.NOT_INITIALIZED)
      configStore.fetchConfigurationPaths = jest.fn().mockResolvedValue([SCENIC_CONFIG_PATH])
      configStore.initializeScenicConfig = jest.fn()
      await configStore.initialize()
      expect(configStore.initializeScenicConfig).toHaveBeenCalled()
    })

    it('should initialize the menus configuration if the path is right', async () => {
      configStore.setInitState(InitStateEnum.NOT_INITIALIZED)
      configStore.fetchConfigurationPaths = jest.fn().mockResolvedValue(['~/test/menus.json'])
      configStore.initializeMenusConfig = jest.fn()
      await configStore.initialize()
      expect(configStore.initializeMenusConfig).toHaveBeenCalled()
    })

    it('should initialize the contacts configuration if the path is right', async () => {
      configStore.setInitState(InitStateEnum.NOT_INITIALIZED)
      configStore.fetchConfigurationPaths = jest.fn().mockResolvedValue(['~/test/contacts.json'])
      configStore.initializeContactsConfig = jest.fn()
      await configStore.initialize()
      expect(configStore.initializeContactsConfig).toHaveBeenCalled()
    })

    it('should initialize the bundles configuration if the path is right', async () => {
      configStore.setInitState(InitStateEnum.NOT_INITIALIZED)
      configStore.fetchConfigurationPaths = jest.fn().mockResolvedValue(['~/test/bundles.json'])
      configStore.initializeBundlesConfig = jest.fn()
      await configStore.initialize()
      expect(configStore.initializeBundlesConfig).toHaveBeenCalled()
    })

    it('should initiate a warning when configuration path is not valid', async () => {
      configStore.setInitState(InitStateEnum.NOT_INITIALIZED)
      configStore.fetchConfigurationPaths = jest.fn().mockResolvedValue(['~/test/test.json'])
      LOG.warn = jest.fn()
      await configStore.initialize()
      expect(LOG.warn).toHaveBeenCalled()
    })

    it('should log an error if a request fails while initializing', async () => {
      configStore.fetchConfiguration = jest.fn().mockRejectedValue(false)
      expect(await configStore.initialize()).toEqual(false)
      expect(configStore.isScenicSchemaValid).not.toHaveBeenCalled()
      expect(configStore.setUserScenic).not.toHaveBeenCalled()
      expect(configStore.isMenuSchemaValid).not.toHaveBeenCalled()
      expect(configStore.setUserMenus).not.toHaveBeenCalled()
      expect(configStore.isContactSchemaValid).not.toHaveBeenCalled()
      expect(configStore.setUserContacts).not.toHaveBeenCalled()
      expect(configStore.setUserBundles).not.toHaveBeenCalled()
      expect(configStore.applySendBundlesToServer).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
      expect(configStore.initState).toEqual(InitStateEnum.NOT_INITIALIZED)
    })
  })

  describe('initializeScenicConfig', () => {
    beforeEach(() => {
      configStore.setUserScenic = jest.fn()
      configStore.fetchConfiguration = jest.fn().mockResolvedValue(JSON.stringify({ TEST: true }))
      LOG.warn = jest.fn()
    })

    it('should not call setUserScenic if the scenic schema is not valid', async () => {
      configStore.isScenicSchemaValid = jest.fn().mockReturnValue(false)
      await configStore.initializeScenicConfig(ConfigEnum.SCENIC)
      expect(LOG.warn).toHaveBeenCalled()
      expect(configStore.setUserScenic).not.toHaveBeenCalled()
    })

    it('should call setUserScenic if the scenic schema is valid', async () => {
      configStore.isScenicSchemaValid = jest.fn().mockReturnValue(true)
      await configStore.initializeScenicConfig(ConfigEnum.SCENIC)
      expect(LOG.warn).not.toHaveBeenCalled()
      expect(configStore.setUserScenic).toHaveBeenCalled()
    })
  })

  describe('initializeMenusConfig', () => {
    beforeEach(() => {
      configStore.setUserMenus = jest.fn()
      configStore.fetchConfiguration = jest.fn().mockResolvedValue(JSON.stringify({ TEST: true }))
      LOG.warn = jest.fn()
    })
    it('should not call setUserMenus if the scenic schema is not valid', async () => {
      configStore.isMenuSchemaValid = jest.fn().mockReturnValue(false)
      await configStore.initializeMenusConfig(ConfigEnum.MENUS, 'path')
      expect(LOG.warn).toHaveBeenCalled()
      expect(configStore.setUserMenus).not.toHaveBeenCalled()
    })

    it('should call setUserMenus if the scenic schema is valid', async () => {
      configStore.isMenuSchemaValid = jest.fn().mockReturnValue(true)
      await configStore.initializeMenusConfig(ConfigEnum.MENUS, 'path')
      expect(LOG.warn).not.toHaveBeenCalled()
      expect(configStore.setUserMenus).toHaveBeenCalled()
    })
  })

  describe('initializeContactsConfig', () => {
    beforeEach(() => {
      configStore.setUserContacts = jest.fn()
      configStore.fetchConfiguration = jest.fn().mockResolvedValue(JSON.stringify({ TEST: true }))
      LOG.warn = jest.fn()
    })
    it('should not call setUserContacts if the scenic schema is not valid', async () => {
      configStore.isContactSchemaValid = jest.fn().mockReturnValue(false)
      await configStore.initializeContactsConfig(ConfigEnum.MENUS, 'path')
      expect(LOG.warn).toHaveBeenCalled()
      expect(configStore.setUserContacts).not.toHaveBeenCalled()
    })

    it('should call setUserContacts if the scenic schema is valid', async () => {
      configStore.isContactSchemaValid = jest.fn().mockReturnValue(true)
      await configStore.initializeContactsConfig(ConfigEnum.MENUS, 'path')
      expect(LOG.warn).not.toHaveBeenCalled()
      expect(configStore.setUserContacts).toHaveBeenCalled()
    })
  })

  describe('initializeBundlesConfig', () => {
    beforeEach(() => {
      configStore.setUserBundles = jest.fn()
    })
    it('should not call setUserBundles if the scenic schema is not valid', async () => {
      configStore.fetchConfiguration = jest.fn().mockResolvedValue(false)
      await configStore.initializeBundlesConfig(ConfigEnum.MENUS, 'path')
      expect(configStore.setUserBundles).not.toHaveBeenCalled()
    })

    it('should call setUserBundles if the scenic schema is valid', async () => {
      configStore.fetchConfiguration = jest.fn().mockResolvedValue(true)
      await configStore.initializeBundlesConfig(ConfigEnum.MENUS, 'path')
      expect(configStore.setUserBundles).toHaveBeenCalled()
    })
  })

  describe('handleSocketChange', () => {
    beforeEach(() => {
      configStore.clear = jest.fn()
    })

    it('should define the APIs if there is a new socket', () => {
      configStore.handleSocketChange(new Socket())
      expect(configStore.clear).toHaveBeenCalled()
    })

    it('should clear the store if the new socket is null', () => {
      configStore.handleSocketChange(null)
      expect(configStore.clear).toHaveBeenCalled()
    })
  })

  describe('isScenicSchemaValid', () => {
    it('should return true if configuration is valid', () => {
      expect(configStore.isScenicSchemaValid(SCENIC_CONFIG)).toEqual(true)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should return false if configuration is invalid', () => {
      expect(configStore.isScenicSchemaValid(MENUS_CONFIG)).toEqual(false)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('isMenuSchemaValid', () => {
    it('should return true if configuration is valid', () => {
      expect(configStore.isMenuSchemaValid(MENUS_CONFIG)).toEqual(true)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should return false if configuration is invalid', () => {
      expect(configStore.isMenuSchemaValid(SCENIC_CONFIG)).toEqual(false)
      expect(LOG.error).toHaveBeenCalled()
      expect(configStore.isMenuSchemaValid({})).toEqual(false)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('isContactSchemaValid', () => {
    it('should return true if contact list is valid', () => {
      expect(configStore.isContactSchemaValid(CONTACT_LIST)).toEqual(true)
      expect(LOG.error).not.toHaveBeenCalled()
      expect(configStore.isContactSchemaValid({})).toEqual(true)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should return false if contact list is invalid', () => {
      expect(configStore.isContactSchemaValid(SCENIC_CONFIG)).toEqual(false)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('fetchConfiguration', () => {
    const PATH = 'PATH'

    beforeEach(() => {
      ({ switcherAPI } = socketStore.APIs)
    })

    it('should return configuration if it exists', async () => {
      const VALUE = { TEST: true }
      switcherAPI.readConfig = jest.fn().mockResolvedValue(JSON.stringify(VALUE))

      expect(await configStore.fetchConfiguration(PATH)).toEqual(VALUE)
      expect(switcherAPI.readConfig).toHaveBeenCalledWith(PATH)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should return null if configuration exists but is empty', async () => {
      switcherAPI.readConfig = jest.fn().mockResolvedValue('{}')
      expect(await configStore.fetchConfiguration(PATH)).toEqual({})
      expect(LOG.error).not.toHaveBeenCalled()

      switcherAPI.readConfig = jest.fn().mockResolvedValue(null)
      expect(await configStore.fetchConfiguration(PATH)).toEqual(null)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should return null and log an error if a request fails', async () => {
      switcherAPI.readConfig = jest.fn().mockRejectedValue(false)

      expect(await configStore.fetchConfiguration(PATH)).toEqual(null)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('findInitialConfiguration', () => {
    it('should return configuration if it exists for the specified class and ID', () => {
      expect(configStore.findInitialConfiguration('sip', 'sip')).toEqual(SIP_INIT)
    })

    it('should return null if no initial configuration exists for specified class and ID', () => {
      expect(configStore.findInitialConfiguration('test', 'test')).toEqual(null)
    })
  })

  describe('applySendBundlesToServer', () => {
    beforeEach(() => {
      ({ switcherAPI } = socketStore.APIs)
    })

    it('should send bundles to server', async () => {
      switcherAPI.sendBundles = jest.fn().mockResolvedValue(true)
      expect(await configStore.applySendBundlesToServer(DEFAULT_BUNDLES)).toEqual(true)
      expect(switcherAPI.sendBundles).toHaveBeenCalledWith(DEFAULT_BUNDLES)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if bundles could not be sent', async () => {
      switcherAPI.sendBundles = jest.fn().mockResolvedValue(false)

      expect(await configStore.applySendBundlesToServer(DEFAULT_BUNDLES)).toEqual(false)
      expect(switcherAPI.sendBundles).toHaveBeenCalledWith(DEFAULT_BUNDLES)
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should log an error if request fails', async () => {
      switcherAPI.sendBundles = jest.fn().mockRejectedValue(false)

      expect(await configStore.applySendBundlesToServer(DEFAULT_BUNDLES)).toEqual(false)
      expect(switcherAPI.sendBundles).toHaveBeenCalledWith(DEFAULT_BUNDLES)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('mergeScenicConfigArrays', () => {
    const OBJECT_ARRAY_1 = [
      {
        id: 'SIP',
        class: 'sip',
        isHidden: true,
        isProtected: true,
        properties: {
          port: 5060
        },
        userTree: {}
      },
      {
        id: 'videotestsrc',
        class: 'videotestsrc'
      }
    ]
    const OBJECT_ARRAY_2 = [
      {
        id: 'SIP',
        class: 'sip',
        isHidden: false,
        properties: {
          port: 8080,
          test: false
        }
      },
      {
        id: 'systemusage',
        class: 'systemusage'
      }
    ]
    const OBJECT_ARRAY_MERGED = [
      {
        id: 'SIP',
        class: 'sip',
        isHidden: false,
        isProtected: true,
        properties: {
          port: 8080,
          test: false
        },
        userTree: {}
      },
      {
        id: 'videotestsrc',
        class: 'videotestsrc'
      },
      {
        id: 'systemusage',
        class: 'systemusage'
      }
    ]

    it('should return the userArray if at least one array is not an Object-based array', () => {
      const ARRAY_1 = ['test1', 'test2']
      const ARRAY_2 = ['test3', 'test4']

      expect(configStore.mergeScenicConfigArrays(ARRAY_1, ARRAY_2)).toEqual(ARRAY_2)
      expect(configStore.mergeScenicConfigArrays(OBJECT_ARRAY_1, ARRAY_2)).toEqual(ARRAY_2)
      expect(configStore.mergeScenicConfigArrays(ARRAY_1, OBJECT_ARRAY_1)).toEqual(OBJECT_ARRAY_1)
      expect(configStore.mergeScenicConfigArrays([...ARRAY_1, ...OBJECT_ARRAY_1], ARRAY_2)).toEqual(ARRAY_2)
    })

    it('should merge the userArray and the defaultArray if both are entirely Object-based and return the result', () => {
      expect(configStore.mergeScenicConfigArrays(OBJECT_ARRAY_1, OBJECT_ARRAY_2)).toEqual(OBJECT_ARRAY_MERGED)
    })
  })

  describe('setUserBundles', () => {
    it('should set user bundles with provided json', () => {
      const BUNDLES = {
        bundle: {
          testBundle: {
            pipeline: 'testing'
          },
          videoInput: {
            doc: {
              description: 'Testing'
            }
          }
        }
      }
      expect(configStore.userBundles).toEqual(null)
      expect(configStore.bundleConfiguration).toEqual(DEFAULT_BUNDLES)

      configStore.setUserBundles(BUNDLES)
      expect(configStore.userBundles).toEqual(BUNDLES)
      expect(configStore.bundleConfiguration).toEqual(MERGED_BUNDLES_CONFIG)
    })
  })

  describe('setUserMenus', () => {
    it('should set user menus with provided json', () => {
      expect(configStore.userMenus).toEqual(null)
      expect(configStore.menuConfiguration).toEqual(DEFAULT_MENUS)

      configStore.setUserMenus(MENUS_CONFIG)
      expect(configStore.userMenus).toEqual(MENUS_CONFIG)
      expect(configStore.menuConfiguration).toEqual(MENUS_CONFIG)
    })
  })

  describe('setUserContacts', () => {
    it('should set user contact list with provided json', () => {
      configStore.setUserContacts(CONTACT_LIST)
      expect(configStore.userContacts).toEqual(CONTACT_LIST)
    })
  })

  describe('setUserScenic', () => {
    it('should set user Scenic configuration with provided json', () => {
      expect(configStore.userScenic).toEqual(null)
      expect(configStore.scenicConfiguration).toEqual(DEFAULT_SCENIC)

      configStore.setUserScenic(SCENIC_CONFIG)
      expect(configStore.userScenic).toEqual(SCENIC_CONFIG)
      // expect(configStore.scenicConfiguration).toEqual(MERGED_SCENIC_CONFIG)
    })
  })

  describe('clear', () => {
    it('should clear ConfigStore', () => {
      const BUNDLES = { test: true }
      configStore.setInitState(InitStateEnum.INITIALIZED)
      configStore.setUserScenic(SCENIC_CONFIG)
      configStore.setUserMenus(MENUS_CONFIG)
      configStore.setUserBundles(BUNDLES)
      configStore.setUserContacts(CONTACT_LIST)
      expect(configStore.userScenic).toEqual(SCENIC_CONFIG)
      expect(configStore.userMenus).toEqual(MENUS_CONFIG)
      expect(configStore.userBundles).toEqual(BUNDLES)
      expect(configStore.userContacts).toEqual(CONTACT_LIST)
      expect(configStore.initState).toEqual(InitStateEnum.INITIALIZED)

      configStore.clear()
      expect(configStore.userScenic).toEqual(null)
      expect(configStore.userMenus).toEqual(null)
      expect(configStore.userBundles).toEqual(null)
      expect(configStore.userContacts).toEqual(null)
      expect(configStore.initState).toEqual(InitStateEnum.NOT_INITIALIZED)
    })
  })
})
